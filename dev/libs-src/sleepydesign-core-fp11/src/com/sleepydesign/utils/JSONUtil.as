package com.sleepydesign.utils
{

	public class JSONUtil
	{
		public static function decode(value:String):*
		{
			try
			{
				return JSON.parse(value);
			}
			catch (e:Error)
			{
				return null;
			}
			//	return null
		}

		public static function encode(value:*):String
		{
			return JSON.stringify(value);
		}
	}
}
