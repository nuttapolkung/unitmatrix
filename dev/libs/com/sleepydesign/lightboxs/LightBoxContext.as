package com.sleepydesign.lightboxs
{
	import com.sleepydesign.lightboxs.view.LightBoxMediator;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.system.DebugUtil;
	
	import flash.display.DisplayObjectContainer;
	
	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;
	

	public class LightBoxContext extends ModuleContext
	{
		public function LightBoxContext(contextView:DisplayObjectContainer = null, parentInjector:IInjector = null)
		{
			super(contextView, true, parentInjector);
		}

		override public function startup():void
		{
			DebugUtil.trace("! LightBoxContext");

			mediatorMap.mapView(LightBoxModule, LightBoxMediator);

			super.startup();
		}

		override public function dispose():void
		{
			DebugUtil.trace(" ! " + this + ".dispose");

			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
