package com.sleepydesign.lightboxs.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.system.DebugUtil;
	
	import flash.display.DisplayObject;
	
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;
	

	public class LightBoxMediator extends ModuleMediator
	{
		[Inject]
		public var view:LightBoxModule;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		private static var _this:LightBoxMediator;

		override public function onRegister():void
		{
			DebugUtil.trace("! LightBoxMediator.onRegister");
			_this = this;
			// modal
			lightBoxSignal.add(view.showBox);
		}

		public function showBox(content:DisplayObject, layerType:String = "contentLayer"):DisplayObject
		{
			return view.showBox(content, layerType);
		}
	}
}
