package
{

	import com.unitmatrix.core.controller.RequestSaveDataCommand;
	import com.unitmatrix.core.controller.RequestSaveImageCommand;
	import com.unitmatrix.core.controller.RequestSaveLocalDataCommand;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.remote.service.CoreService;
	import com.unitmatrix.core.signals.ReceiveLoadLocalDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveImageSignal;
	import com.unitmatrix.core.signals.ReceiveSaveLocalDataSignal;
	import com.unitmatrix.core.signals.RequestSaveDataSignal;
	import com.unitmatrix.core.signals.RequestSaveImageSignal;
	import com.unitmatrix.core.signals.RequestSaveLocalDataSignal;
	import com.unitmatrix.module.roomdetail.view.RoomDetailModule;
	import com.unitmatrix.module.home.view.HomeModule;
	import com.unitmatrix.module.init.view.InitModule;
	import com.unitmatrix.module.reserveroom.view.ReserveRoomModule;
	import com.unitmatrix.module.survey.view.SurveyModule;
	import com.unitmatrix.module.surveyinfo.view.SurveyInfoModule;
	import com.unitmatrix.module.questionnaire.view.QuestionnaireModule;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.AppConfig;
	import com.sleepydesign.robotlegs.modules.ModuleTransition;
	import com.sleepydesign.robotlegs.shells.ShellContext;

	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	import flash.utils.getQualifiedClassName;

	import org.robotlegs.base.SignalCommandMap;
	import org.robotlegs.core.IInjector;
	import org.robotlegs.core.IViewMap;

	public class Config
	{

		// App --------------------------------------------------------------
		public static const IS_ANDROID:Boolean = true;
		public static const VERSION:String = "0.0.7"; //DataProxy.getData("VERSION");

		public static const SERVER_DATA:String = "http://192.168.0.1:8090/";
		//public static const SERVER_DATA:String = "http://aisystem.dyndns.biz:8090/";

		// Setting -----------------------------------------------------------
		//for android
//		public static const GUI_SIZE:Rectangle = new Rectangle(0, 0, Capabilities.screenResolutionX, Capabilities.screenResolutionY);
		//for ios
		//public static const GUI_SIZE:Rectangle = new Rectangle(0, 0, 1024, 768);
		//for test
		public static const GUI_SIZE:Rectangle = new Rectangle(0, 0, 1920, 1080);
		public static var ratio:Number = GUI_SIZE.height / 1080;
		private static var _isIOS:Boolean = true;

		// Styles --------------------------------------------------------------
		public static const TRANSITION_DELAY:Number = .5;
		public static const SOCIAL_NETWORK:Boolean = false;
		private static var _shellContext:ShellContext;

		public static var panel:MovieClip;

		public static function get shellContext():ShellContext
		{
			return _shellContext;
		}

		public static function get M00_INIT_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[0]);
		}

		public static function get M10_QUESTIONNAIRE_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[1]);
		}

		public static function get M20_HOME_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[2]);
		}

		public static function get M30_RESERVEROOM_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[3]);
		}

		public static function get M40_SURVEY_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[4]);
		}

		public static function get M50_SURVEY_INFO_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[5]);
		}

		public static function get M60_ROOM_DETAIL_MODULE():String
		{
			return getQualifiedClassName(AppConfig.modules[6]);
		}


		// init --------------------------------------------------------------

		public static function init():void
		{
			// predefine layers
			AppConfig.layers = Vector.<String>([AppConfig.SYSTEM_LAYER, AppConfig.BG_LAYER, AppConfig.APP_LAYER, AppConfig.TOP_LAYER, AppConfig.LIGHTBOX_LAYER]);

			// predefine modules
			AppConfig.modules = Vector.<Class>([InitModule, QuestionnaireModule, HomeModule, ReserveRoomModule, SurveyModule, SurveyInfoModule, RoomDetailModule]);

			// custom transitions
			AppConfig.transition = new ModuleTransition;

			// do before startup thingy here
			AppConfig.beforeStartup = beforeStartup;

			// do startup thingy here
			AppConfig.startup = startup;

			// do startup thingy here
			AppConfig.onStartup = onStartup;

			// set default modul
			AppConfig.DEFAULT_MODULE_NAME = M00_INIT_MODULE;
		}


		private static function startup(viewMap:IViewMap, injector:IInjector, signalCommandMap:SignalCommandMap):void
		{
			injector.mapSingleton(LightBoxSignal);
			injector.mapSingleton(CoreModel);
			injector.mapSingleton(CoreService);
			injector.mapSingleton(ReceiveSaveLocalDataSignal);
			injector.mapSingleton(ReceiveLoadLocalDataSignal);
			injector.mapSingleton(ReceiveSaveDataSignal);
			injector.mapSingleton(ReceiveSaveImageSignal);

			viewMap.mapType(LightBoxModule);

			signalCommandMap.mapSignalClass(RequestSaveDataSignal, RequestSaveDataCommand, false, ['raw', 'file_name']);
			signalCommandMap.mapSignalClass(RequestSaveLocalDataSignal, RequestSaveLocalDataCommand, false, ['raw', 'file_name']);
			signalCommandMap.mapSignalClass(RequestSaveImageSignal, RequestSaveImageCommand, false, ['ba']);
		}

		private static function beforeStartup(shellContext:ShellContext):void
		{
			shellContext.layers[AppConfig.LIGHTBOX_LAYER].addChild(new LightBoxModule);
		}

		private static function onStartup(shellContext:ShellContext):void
		{
			_shellContext = shellContext;
		}
	}
}


