package
{
	import com.sleepydesign.display.SDStageSprite;
	import com.sleepydesign.robotlegs.shells.view.ShellView;
	import com.sleepydesign.system.DebugUtil;

	import flash.display.Bitmap;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.utils.getQualifiedClassName;

	[SWF(backgroundColor = "#FFFFFF", frameRate = "30", width = "1920", height = "1080", embedAsCFF = "false")]
	//[SWF(backgroundColor = "#FFFFFF", frameRate = "30", width = "1200", height = "800", embedAsCFF = "false")]

	public class main extends SDStageSprite
	{
		[Embed(source = '/bg.jpg')]
		private var BG_CLASS:Class;
		private var bg:Bitmap = new BG_CLASS();

		protected const _ID:String = getQualifiedClassName(this);
		private var _appScale:Number;
		private static var _shellView:ShellView;

		public static function get shellView():ShellView
		{
			return _shellView;
		}

		public function get ID():String
		{
			return _ID;
		}

		public function main()
		{
			initStage();
			initModule();

			super();
		}

		private function initModule():void
		{
			Config.init();
		}

		override protected function onStage():void
		{
			bg.width = Config.GUI_SIZE.width;
			bg.height = Config.GUI_SIZE.height;
			bg.smoothing = true;
			addChild(bg);

			_shellView = new ShellView();
			addChild(_shellView);

			_shellView.scrollRect = Config.GUI_SIZE;

			Jojo.init(this.stage);
			Jojo.startBreeze();
		}

		private function initStage():void
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
		}

		private function initDebug():void
		{
			DebugUtil.init(this);
		}
	}
}


