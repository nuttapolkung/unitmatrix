package com.unitmatrix.core.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	import Dialog.alert_sp;

	import SurveyInfo.block_image;

	import org.osflash.signals.Signal;

	public class DialogComponent extends SDSprite
	{
		public static const DIALOGCOMPONENT_CLICK_YES:String = "yes";
		public static const DIALOGCOMPONENT_CLICK_NO:String = "no";
		public static const DIALOGCOMPONENT_CLICK_CLOSE:String = "close";

		public static const CLOSE:String = "close";
		public static const YES_NO:String = "yse_no";
		public static const NONE:String = "none";

		private var _skin:Sprite;
		private var _title:String;
		private var _desc:String;
		private var _type:String;
		private var _onClickSignal:Signal;

		/**
		 *
		 * @param title
		 * @param desc
		 * @param type
		 * @param onClickSignal (String)
		 *
		 */
		public function DialogComponent(title:String, desc:String, type:String = NONE, onClickSignal:Signal = null)
		{
			addEventListener(Event.ADDED_TO_STAGE, onStage);
			_title = title;
			_desc = desc;
			_type = type;

			_onClickSignal = onClickSignal;
		}

		protected function onStage(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, onStage);

			create();

			if (_onClickSignal)
				addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		protected function onMouseUp(event:MouseEvent):void
		{
			switch (event.target)
			{
				case close_btn:
				{
					_onClickSignal.dispatch(DIALOGCOMPONENT_CLICK_CLOSE);
					break;
				}
				case yes_btn:
				{
					_onClickSignal.dispatch(DIALOGCOMPONENT_CLICK_YES);
					break;
				}
				case no_btn:
				{
					_onClickSignal.dispatch(DIALOGCOMPONENT_CLICK_NO);
					break;
				}

				default:
				{
					break;
				}
			}

		}

		private function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new Dialog.alert_sp;
			addChild(_skin);
			resize();
			switch (_type)
			{
				case NONE:
					close_btn.visible = false;
					yes_btn.visible = false;
					no_btn.visible = false;
					break;
				case CLOSE:
					close_btn.visible = true;
					yes_btn.visible = false;
					no_btn.visible = false;
					break;
				case YES_NO:
					close_btn.visible = false;
					yes_btn.visible = true;
					no_btn.visible = true;
					break;
			}

			title_tf.text = _title;
			desc_tf.htmlText = _desc;

		}

		private function resize():void
		{
			trace(_skin.width)
			_skin.scaleX = _skin.scaleY = Config.ratio;
			trace(_skin.width)
			_skin.x = Config.GUI_SIZE.width / 2 //- _skin.width / 2;
			_skin.y = Config.GUI_SIZE.height / 2 //- _skin.height / 2;
		}

		private function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}

		private function get desc_tf():TextField
		{
			return _skin.getChildByName("desc_tf") as TextField;
		}

		private function get close_btn():SimpleButton
		{
			return _skin.getChildByName("close_btn") as SimpleButton;
		}

		private function get yes_btn():SimpleButton
		{
			return _skin.getChildByName("yes_btn") as SimpleButton;
		}

		private function get no_btn():SimpleButton
		{
			return _skin.getChildByName("no_btn") as SimpleButton;
		}

		//destroy ---------------------------------------------

		public function dispose():void
		{
			/*if (_onClickSignal)
				removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);*/
			_onClickSignal = null;
			destroy();
		}
	}
}
