package com.unitmatrix.core.signals
{
	import org.osflash.signals.Signal;

	public class RequestSaveDataSignal extends Signal
	{
		public function RequestSaveDataSignal()
		{
			super(String, String);
		}
	}
}
