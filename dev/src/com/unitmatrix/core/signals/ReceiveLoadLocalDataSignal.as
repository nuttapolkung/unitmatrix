package com.unitmatrix.core.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveLoadLocalDataSignal extends Signal
	{
		public function ReceiveLoadLocalDataSignal()
		{
			super();
		}
	}
}
