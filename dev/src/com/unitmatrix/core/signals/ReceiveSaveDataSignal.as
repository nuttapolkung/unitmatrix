package com.unitmatrix.core.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveSaveDataSignal extends Signal
	{
		public function ReceiveSaveDataSignal()
		{
			super();
		}
	}
}
