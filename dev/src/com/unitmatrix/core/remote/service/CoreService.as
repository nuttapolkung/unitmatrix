package com.unitmatrix.core.remote.service
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.signals.ReceiveSaveDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveImageSignal;
	import com.unitmatrix.core.signals.ReceiveSaveLocalDataSignal;
	import com.sleepydesign.net.LoaderUtil;
	import com.sleepydesign.system.DebugUtil;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class CoreService extends Actor
	{

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var receiveSaveDataOfflineSignal:ReceiveSaveLocalDataSignal;

		[Inject]
		public var receiveSaveDataSignal:ReceiveSaveDataSignal;

		[Inject]
		public var receiveSaveImageSignal:ReceiveSaveImageSignal;

		private var _raw:String;

		private var _file_name:String;

		public function saveFile(raw:String, file_name:String):void
		{
			var file:File = getFile(file_name);

			var fileStream:FileStream = new FileStream();

			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeUTFBytes(raw);
			fileStream.addEventListener(Event.CLOSE, fileClosed);
			fileStream.close();

		}

		public function getFile(file_name:String):File
		{
			return File.documentsDirectory.resolvePath("bmmt/" + file_name + ".dat");
		}

		public function saveToServer(raw:String):void
		{
			var completeSignal:Signal = new Signal(String);

			var date:Date = new Date();
			var dateString:String = date.fullYear.toString() + "-" + ((date.month <= 9) ? ('0' + date.month.toString()) : date.month.toString()) + '-' + ((date.date <= 9) ? ('0' + date.date.toString()) : date.date.toString()) + ' ' + ((date.hours <= 9) ? ('0' + date.hours.toString()) : date.hours.toString()) + ':' + ((date.minutes <= 9) ? ('0' + date.minutes.toString()) : date.minutes.toString()) + ':' + ((date.seconds <= 9) ? ('0' + date.seconds.toString()) : date.seconds.toString());

			var urlVariables:URLVariables = new URLVariables
			urlVariables.data = raw;
			urlVariables.client_time = dateString;
			// set URI
			var uri:String = Config.SERVER_DATA + "bmmt_ai/JsonData5Save.php";

			// load!
			LoaderUtil.request(uri, urlVariables, function(event:Event):void
			{
				if (event.type == IOErrorEvent.IO_ERROR)
				{
					DebugUtil.trace("Error : " + event);
				}

				if (event.type == Event.COMPLETE)
				{
					//
					var raw:String = event.target.data as String;
					_raw = raw;

					//-tell any one listener
					completeSignal.dispatch(_raw);
					//-clear signal
					completeSignal.removeAll();
					completeSignal = null;
					receiveSaveDataSignal.dispatch();
				}
			}, URLLoaderDataFormat.TEXT, URLRequestMethod.POST);
		}

		public function saveImage(ba:ByteArray):void
		{
			var sendHeader:URLRequestHeader = new URLRequestHeader("Content-type", "application/octet-stream");

			var sendReq:URLRequest = new URLRequest(Config.SERVER_DATA + "bmmt_ai/savepicture.php");
			sendReq.requestHeaders.push(sendHeader);
			sendReq.method = URLRequestMethod.POST;
			sendReq.data = ba;

			var sendLoader:URLLoader;
			sendLoader = new URLLoader();
			sendLoader.addEventListener(Event.COMPLETE, imageSentHandler);
			sendLoader.load(sendReq);
		}

		protected function imageSentHandler(event:Event):void
		{
			receiveSaveImageSignal.dispatch();
		}

		private function fileClosed(event:Event):void
		{

			receiveSaveDataOfflineSignal.dispatch();
		}

	}
}
