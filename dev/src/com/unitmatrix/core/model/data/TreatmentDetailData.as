package com.unitmatrix.core.model.data
{


	public class TreatmentDetailData extends Object
	{

		private var _id:String;
		private var _quantity:String;
		private var _path:String;

		public function TreatmentDetailData(id:String, quantity:String, path:String = "")
		{
			_id = id;
			_quantity = quantity;
			if (!path || path == "")
				_path = "gallery_" + String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get id():String
		{
			return _id;
		}

		public function get quantity():String
		{
			return _quantity;
		}

		public function get path():String
		{
			return _path;
		}

		public function toObject():Object
		{
			return {treatmentid: _id, treatmentqty: _quantity, path: _path};
		}


	}
}
