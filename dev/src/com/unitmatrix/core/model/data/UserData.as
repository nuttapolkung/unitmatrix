package com.unitmatrix.core.model.data
{

	public class UserData extends Object
	{
		private var _usercd:String;
		private var _firstname:String;
		private var _lastname:String;
		private var _groupid:String;
		private var _username:String;
		private var _password:String;

		public function UserData(usercd:String, firstname:String, lastname:String, groupid:String, username:String, password:String)
		{
			_usercd = usercd;
			_firstname = firstname;
			_lastname = lastname;
			_groupid = groupid;
			_username = username;
			_password = password;
		}

		public function get usercd():String
		{
			return _usercd;
		}

		public function get firstname():String
		{
			return _firstname;
		}

		public function get lastname():String
		{
			return _lastname;
		}

		public function get groupid():String
		{
			return _groupid;
		}

		public function get username():String
		{
			return _username;
		}

		public function get password():String
		{
			return _password;
		}


	}
}
