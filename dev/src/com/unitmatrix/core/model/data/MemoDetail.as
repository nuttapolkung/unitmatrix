package com.unitmatrix.core.model.data
{

	public class MemoDetail extends Object
	{
		private var _id:String;
		private var _memo:String;
		private var _path:String;

		public function MemoDetail(id:String, memo:String, path:String = "")
		{
			_id = id;
			_memo = memo;
			if (path == "")
				_path = "gallery_" + String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get path():String
		{
			return _path;
		}

		public function get memo():String
		{
			return _memo;
		}

		public function get id():String
		{
			return _id;
		}

		public function toObject():Object
		{
			return {detailid: _id, memo: _memo, path: _path};
		}


	}
}
