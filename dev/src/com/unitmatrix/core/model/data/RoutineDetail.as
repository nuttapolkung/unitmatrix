package com.unitmatrix.core.model.data
{

	public class RoutineDetail extends Object
	{
		private var _id:String;
		private var _routine_dt:String;
		private var _routine_last_dt:String;
		private var _routine_next_dt:String;
		private var _routine_special_flag:String;
		private var _memo_detail:Vector.<MemoDetail>;
		private var _treatment_detail:Vector.<TreatmentDetailData>;
		private var _routine_date:Date;
		private var _path:String;

		public function RoutineDetail(id:String, routine_dt:String, routine_last_dt:String, routine_next_dt:String, routine_special_flag:String, memo_detail:Vector.<MemoDetail>, treatment_detail:Vector.<TreatmentDetailData>, path:String = "")
		{
			_id = id;
			if (_id == null)
				_id = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);

			_routine_dt = routine_dt;
			_routine_last_dt = routine_last_dt;
			_routine_next_dt = routine_next_dt;
			_routine_special_flag = routine_special_flag;
			_memo_detail = memo_detail;
			_treatment_detail = treatment_detail;

			var ds:String = routine_dt.split("-").join("/");
			_routine_date = new Date(Date.parse(ds));

			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}



		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function get routine_date():Date
		{
			return _routine_date;
		}

		public function get memo_detail():Vector.<MemoDetail>
		{
			return _memo_detail;
		}

		public function get routine_special_flag():String
		{
			return _routine_special_flag;
		}

		public function get routine_next_dt():String
		{
			return _routine_next_dt;
		}

		public function get routine_last_dt():String
		{
			return _routine_last_dt;
		}

		public function get routine_dt():String
		{
			return _routine_dt;
		}

		public function get id():String
		{
			return _id;
		}

		public function get treatment_detail():Vector.<TreatmentDetailData>
		{
			return _treatment_detail;
		}


		public function set routine_dt(value:String):void
		{
			_routine_dt = value;
		}

		public function set routine_last_dt(value:String):void
		{
			_routine_last_dt = value;
		}

		public function set routine_next_dt(value:String):void
		{
			_routine_next_dt = value;
		}

		public function set routine_special_flag(value:String):void
		{
			_routine_special_flag = value;
		}

		public function set memo_detail(value:Vector.<MemoDetail>):void
		{
			_memo_detail = value;
		}

		public function set treatment_detail(value:Vector.<TreatmentDetailData>):void
		{
			_treatment_detail = value;
		}

		public function toObject():Object
		{
			var _memos:Array = [];
			for each (var md:MemoDetail in _memo_detail)
			{
				_memos.push(md.toObject());
			}
			var _treatments:Array = [];
			for each (var td:TreatmentDetailData in _treatment_detail)
			{
				_treatments.push(td.toObject());
			}
			return {routineid: _id, routinedt: _routine_dt, routinelastdt: _routine_last_dt, routinenextdt: _routine_next_dt, routinespecialflag: _routine_special_flag, memodetail: _memos, treatmentdetail: _treatments};
		}

		public function clone():RoutineDetail
		{
			return new RoutineDetail(id, routine_dt, routine_last_dt, routine_next_dt, routine_special_flag, memo_detail.concat(), treatment_detail.concat());
		}

	}
}
