package com.unitmatrix.core.model.data
{

	public class SurfaceData extends Object
	{
		private var _surfaceid:String;
		private var _path:String;
		private var _surfacename:String;
		private var _surfaceabbrev:String;

		public function SurfaceData(id:String, name:String, abbrev:String, path:String = "")
		{
			_surfaceid = id;
			_surfacename = name;
			_surfaceabbrev = abbrev;
			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function get surfaceid():String
		{
			return _surfaceid;
		}

		public function get surfacename():String
		{
			return _surfacename;
		}

		public function get surfaceabbrev():String
		{
			return _surfaceabbrev;
		}
	}
}
