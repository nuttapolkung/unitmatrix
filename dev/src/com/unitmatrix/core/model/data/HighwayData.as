package com.unitmatrix.core.model.data
{

	public class HighwayData extends Object
	{
		private var _highwayid:String;
		private var _path:String;
		private var _highwayname:String;
		private var _highwaycode:String;

		public function HighwayData(id:String, name:String, code:String, path:String = "")
		{
			_highwayid = id;
			_highwayname = name;
			_highwaycode = code;
			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
		}

		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function get highwayid():String
		{
			return _highwayid;
		}

		public function get highwayname():String
		{
			return _highwayname;
		}

		public function get highwaycode():String
		{
			return _highwaycode;
		}
	}
}


