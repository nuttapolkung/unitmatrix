package com.unitmatrix.core.model.data
{
	import __AS3__.vec.Vector;

	public class SearchData extends Object
	{
		private var _group:Vector.<GroupData>;
		private var _surface:Vector.<SurfaceData>;
		private var _highway:Vector.<HighwayData>;
		private var _category:Vector.<CategoryData>;
		private var _bpiscore:Vector.<String>;

		public function SearchData(group:Vector.<GroupData>, surface:Vector.<SurfaceData>, highway:Vector.<HighwayData>, category:Vector.<CategoryData>, bpiscore:Vector.<String>)
		{
			_group = group;
			_surface = surface;
			_highway = highway;
			_category = category;
			_bpiscore = bpiscore;
		}

		public function get group():Vector.<GroupData>
		{
			return _group;
		}

		public function get surface():Vector.<SurfaceData>
		{
			return _surface;
		}

		public function get highway():Vector.<HighwayData>
		{
			return _highway;
		}

		public function get category():Vector.<CategoryData>
		{
			return _category;
		}

		public function get bpiscore():Vector.<String>
		{
			return _bpiscore;
		}

		public function bpiscoreToObject():Object
		{
			return {0: _bpiscore[0], 1: _bpiscore[1], 2: _bpiscore[2], 3: _bpiscore[3], 4: _bpiscore[4], 5: _bpiscore[5], N: _bpiscore[6]};
		}

		public function toObject():Object
		{
			return {group: _group, surface: _surface, highway: _highway, category: _category, bpiscore: bpiscoreToObject()}
		}


	}


}
