package com.unitmatrix.core.model.data
{

	public class TreatmentData extends Object
	{
		private var _id:String;
		private var _name:String;
		private var _code:String;
		private var _routine_flag:String;
		private var _principle_flag:String;
		private var _unit:String;

		public function TreatmentData(id:String, name:String, code:String, routine_flag:String, principle_flag:String, unit:String)
		{
			_id = id;
			_name = name;
			_code = code;
			_routine_flag = routine_flag;
			_principle_flag = principle_flag;
			_unit = unit;

		}

		public function get id():String
		{
			return _id;
		}

		public function get name():String
		{
			return _name;
		}

		public function get code():String
		{
			return _code;
		}

		public function get routine_flag():String
		{
			return _routine_flag;
		}

		public function get principle_flag():String
		{
			return _principle_flag;
		}

		public function get unit():String
		{
			return _unit;
		}

		public function toObject():Object
		{
			return {treatmentid: _id, treatment_name: _name, treatmentcode: _code, routineflag: _routine_flag, principleflag: _principle_flag, treatment_unit: _unit};
		}


	}
}
