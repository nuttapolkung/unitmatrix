package com.unitmatrix.core.model.data
{

	public class GroupData extends Object
	{
		private var _groupid:String;
		private var _grouppath:String;
		private var _groupname:String;
		private var _groupabbrev:String;

		public function GroupData(id:String, name:String, abbrev:String, path:String = "")
		{
			_groupid = id;
			_groupname = name;
			_groupabbrev = abbrev;
			if (path == "")
				_grouppath = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_grouppath = path;
		}

		public function get path():String
		{
			return _grouppath;
		}

		public function set path(value:String):void
		{
			_grouppath = value;
		}

		public function get groupid():String
		{
			return _groupid;
		}

		public function get groupname():String
		{
			return _groupname;
		}

		public function get groupabbrev():String
		{
			return _groupabbrev;
		}


	}
}
