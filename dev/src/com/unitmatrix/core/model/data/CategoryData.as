package com.unitmatrix.core.model.data
{

	public class CategoryData extends Object
	{
		private var _categoryid:String;
		private var _path:String;
		private var _categoryname:String;
		private var _categoryabbrev:String;

		public function CategoryData(id:String, name:String, abbrev:String, path:String = "")
		{
			_categoryid = id;
			if (path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;
			_categoryname = name;
			_categoryabbrev = abbrev;
		}

		public function get path():String
		{
			return _path;
		}

		public function set path(value:String):void
		{
			_path = value;
		}

		public function get categoryid():String
		{
			return _categoryid;
		}

		public function get categoryname():String
		{
			return _categoryname;
		}

		public function get categoryabbrev():String
		{
			return _categoryabbrev;
		}
	}
}
