package com.unitmatrix.core.model
{
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.core.model.data.SearchData;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.unitmatrix.core.model.data.UserData;
	import com.unitmatrix.core.paser.Parser;
	import com.unitmatrix.core.signals.ReceiveLoadLocalDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveLocalDataSignal;
	import com.unitmatrix.core.signals.RequestSaveDataSignal;
	import com.unitmatrix.core.signals.RequestSaveImageSignal;
	import com.unitmatrix.core.signals.RequestSaveLocalDataSignal;
	import com.unitmatrix.module.survey.model.data.SurveyData;
	import com.unitmatrix.module.survey.view.component.SurveyItem;
	import com.sleepydesign.utils.JSONUtil;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.utils.ByteArray;

	import __AS3__.vec.Vector;

	import by.blooddy.crypto.image.JPEGEncoder;

	import net.pirsquare.m.MobileFileUtil;

	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Actor;

	public class CoreModel extends Actor
	{
		[Inject]
		public var requestSaveLocalDataSignal:RequestSaveLocalDataSignal;

		[Inject]
		public var receiveSaveLocalDataSignal:ReceiveSaveLocalDataSignal;

		[Inject]
		public var requestSaveDataSignal:RequestSaveDataSignal;

		[Inject]
		public var receiveSaveDataSignal:ReceiveSaveDataSignal;

		[Inject]
		public var receiveLoadLocalDataSignal:ReceiveLoadLocalDataSignal;

		[Inject]
		public var requestSaveImageSignal:RequestSaveImageSignal;


		public var RoomName:String = "";
		// external ------------------------------------------------------------------

		public var completeLocalRead:Signal = new Signal(String, String);
		public var changeViewSignal:Signal = new Signal(String);
		public var initAppDataCompleteSignal:Signal = new Signal();

		public var active_page:int = 0;

		public var bridgeDatas:Vector.<BridgeData>;
		public var treatmentDatas:Vector.<TreatmentData>;
		public var isOnline:Boolean;
		public var bridgeDetailDatas:Vector.<BridgeDetailData>;
		public var searchData:SearchData;

		public var isLogined:Boolean = false;
		public var select_survey_id:String;
		public var currentTreatment:TreatmentDetailData;
		public var currentRoutineDetail:RoutineDetail;
		public var currentPrintciple:PrintcipleDetail;

		private var _file_names:Array;
		private var _counter:int;



//		Gallery Section
		public var currentBridgeData:BridgeData;
		public var currentItem:SurveyItem;
		public var currentSurveyData:SurveyData;
		public var systemErrorSignal:Signal = new Signal;
		public var dataLocal:Boolean;
		public var userDatas:Vector.<UserData>;
		public var currentUserData:UserData;

		public function set itemToSave(value:Boolean):void
		{
			currentBridgeData.isSave = value;
			currentSurveyData.want_to_save = value;

			saveLocal();
		}

		public function get itemToSave():Boolean
		{
			if (!currentBridgeData)
				return false;
			return currentBridgeData.isSave;
		}

		public static function checkHaveImageAtID(id:String):Boolean
		{
			if (id == null)
				return false;

			var file:File = File.applicationStorageDirectory.resolvePath(id);
			if (file.exists)
			{
				if (file.getDirectoryListing().length > 0)
					return true;
				else
					return false;
			}
			else
				return false;

		}

		public static function getImageByteAtID(id:String):Vector.<ByteArray>
		{
			var result:Vector.<ByteArray> = new Vector.<ByteArray>
			var file:File = File.applicationStorageDirectory.resolvePath(id);
			if (file.exists)
				if (file.getDirectoryListing().length > 0)
				{
					var i:int = 0;
					var files:Array = file.getDirectoryListing();
					var total:int = files.length;
					while (i < total)
					{
						var image_file:File = files[i];

						var fs:FileStream = new FileStream();
						fs.open(image_file, FileMode.READ);
						var byte:ByteArray = new ByteArray();
						fs.readBytes(byte, 0, fs.bytesAvailable);
						fs.close();

						result.push(byte);
						i++;
					}

				}

			return result;
		}

//		END Gallery Section



		public function getBridgeDataByID(id:String):BridgeData
		{
			return VectorUtil.getItemByID(bridgeDatas, id) as BridgeData;
		}

		public function getBridgeDetailDatasByID(id:String):BridgeDetailData
		{
			return VectorUtil.getItemByID(bridgeDetailDatas, id) as BridgeDetailData;
		}

		public function getBridgeDetailDataByID(id:String):BridgeDetailData
		{
			return VectorUtil.getItemByID(bridgeDetailDatas, id) as BridgeDetailData;
		}

		public function getTreatmentDataByID(id:String):TreatmentData
		{
			return VectorUtil.getItemByID(treatmentDatas, id) as TreatmentData;
		}

		public function saveData(raw:String, file_name:String):void
		{
			requestSaveLocalDataSignal.dispatch(raw, file_name);
		}

		public function readData(file_names:Array):void
		{
			_file_names = file_names;
			_counter = 0;
			MobileFileUtil.open("bmmt/" + _file_names[_counter] + ".dat").addOnce(function(ba:ByteArray):void
			{
				readComplete(_file_names[_counter], ba.toString());
			});

		}

		public function deleteData(file_names:Array):void
		{
			_file_names = file_names;
			_counter = 0;
			do
			{
				MobileFileUtil.deleteFileAsync("bmmt/" + _file_names[_counter] + ".dat").addOnce(function():void
				{
					_counter++;
				});
			} while (_counter >= _file_names.length);

		}

		private function readComplete(filename:String, raw:String):void
		{

			_counter++;
			if (_counter < _file_names.length)
			{
				MobileFileUtil.open("bmmt/" + _file_names[_counter] + ".dat").addOnce(function(ba:ByteArray):void
				{
					readComplete(_file_names[_counter], ba.toString());

				});
			}

			if (filename && raw)
			{
				switch (filename)
				{
					case "JsonData1":
						//	bridgeDatas = Parser.parseBridgeData(raw);

						var rawDatas:Vector.<BridgeData> = Parser.parseBridgeData(raw);
						var newDatas:Vector.<BridgeData> = new Vector.<BridgeData>();

						if (!currentUserData || !currentUserData.groupid || currentUserData.groupid == "")
						{
							bridgeDatas = rawDatas;
						}
						else
						{
							for each (var bridgeData:BridgeData in rawDatas)
							{
								if (currentUserData.groupid == bridgeData.group_id)
									newDatas.push(bridgeData);
							}

							bridgeDatas = newDatas;
						}

						break;
					case "JsonData2":
						treatmentDatas = Parser.parseTreatmentData(raw);
						break;
					case "JsonData3":
						bridgeDetailDatas = Parser.parseBridgeDetailData(raw);
						break;
					case "jsondata4Search":
						searchData = Parser.parseSearchData(raw);
						receiveLoadLocalDataSignal.dispatch();
						break;
				}
			}
		}

		public function saveToServer():void
		{
			var bds:Array = [];
			for each (var bd:BridgeData in bridgeDatas)
			{
				bd.isSave = false;
				bds.push(bd.toObject());
			}

			var raw:String = JSONUtil.encode(bds);

			requestSaveDataSignal.dispatch(raw, "JsonData1");

		}

		public function saveLocal():void
		{
			var bds:Array = [];
			for each (var bd:BridgeData in bridgeDatas)
			{
				bds.push(bd.toObject());
			}

			var raw:String = JSONUtil.encode(bds);

			receiveSaveLocalDataSignal.addOnce(onSaveLocalStep1);
			saveData(raw, "JsonData1");

		}

		public function saveImage(image:ByteArray):void
		{
			var loader:Loader = new Loader();
			loader.loadBytes(image);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, loaderComplete);
		}

		private function loaderComplete(event:Event):void
		{
			var loaderInfo:LoaderInfo = LoaderInfo(event.target);
			var bitmapData:BitmapData;
			if (loaderInfo.width > loaderInfo.height)
				bitmapData = new BitmapData(800, 600, false, 0x00FF00);
			else
				bitmapData = new BitmapData(600, 800, false, 0x00FF00);

			var ratioH:Number = bitmapData.height / loaderInfo.height;
			var ratioV:Number = bitmapData.width / loaderInfo.width;

			if (ratioV > ratioH)
				ratioH = ratioV;
			else
				ratioV = ratioH;

			var offsetX:Number = 0;
			var offsetY:Number = 0;

			bitmapData.draw(loaderInfo.loader, new Matrix(ratioH, 0, 0, ratioV, offsetX, offsetY), null, null, null, true);

			var imageBytes:ByteArray = JPEGEncoder.encode(bitmapData, 60);
			requestSaveImageSignal.dispatch(imageBytes);
		}



		private function onSaveLocalStep1():void
		{
			var tds:Array = [];
			for each (var data:TreatmentData in treatmentDatas)
			{
				tds.push(data.toObject());
			}

			var raw:String = JSONUtil.encode(tds);

			receiveSaveLocalDataSignal.addOnce(onSaveLocalStep2);
			saveData(raw, "JsonData2");
		}

		private function onSaveLocalStep2():void
		{
			var bdds:Array = [];
			for each (var data:BridgeDetailData in bridgeDetailDatas)
			{
				bdds.push(data.toObject());
			}

			var raw:String = JSONUtil.encode(bdds);
			receiveSaveLocalDataSignal.addOnce(onSaveLocalStep3);
			saveData(raw, "JsonData3");
		}

		private function onSaveLocalStep3():void
		{
			var raw:String = JSONUtil.encode(searchData.toObject());
			saveData(raw, "jsondata4Search");
		}

	}
}
