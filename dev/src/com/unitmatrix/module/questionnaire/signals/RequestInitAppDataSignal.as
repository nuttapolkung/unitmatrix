package com.unitmatrix.module.questionnaire.signals
{
	import org.osflash.signals.Signal;

	public class RequestInitAppDataSignal extends Signal
	{
		public function RequestInitAppDataSignal()
		{

			super();
		}
	}
}
