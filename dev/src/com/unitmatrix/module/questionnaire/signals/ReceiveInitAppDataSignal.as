package com.unitmatrix.module.questionnaire.signals
{
	import org.osflash.signals.Signal;
	
	public class ReceiveInitAppDataSignal extends Signal
	{
		public function ReceiveInitAppDataSignal(...parameters)
		{
			super(parameters);
		}
	}
}