package com.unitmatrix.module.questionnaire.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.signals.ReceiveLoadLocalDataSignal;
	import com.unitmatrix.module.questionnaire.model.QuestionnaireModel;
	import com.unitmatrix.module.questionnaire.signals.ReceiveInitAppDataSignal;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class QuestionnaireMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;
		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:QuestionnaireModule;

		[Inject]
		public var model:QuestionnaireModel;

		[Inject]
		public var receiveInitAppDataSignal:ReceiveInitAppDataSignal;

		[Inject]
		public var receiveLoadLocalDataSignal:ReceiveLoadLocalDataSignal;

		// create ------------------------------------------------------------------

		private var _preload:MovieClip;
		private var _monitor:Object;
		private var _dialogComponent:DialogComponent;


		override public function onRegister():void
		{
			trace("! " + this + ".start");
			module.create();

			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		/*coreModel.systemErrorSignal.addOnce(systemError);

		_dialogComponent = new DialogComponent("แจ้งเตือน", "กำลังเชื่อมต่อกับ server เพื่อดึงข้อมูล\nกรุณารอสักครู่");
		lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);

		if (!coreModel.dataLocal)
		{
			coreModel.dataLocal = true;
			receiveInitAppDataSignal.addOnce(onComplete);
			model.getData();
		}
		else
		{
			receiveLoadLocalDataSignal.addOnce(onComplete);
			coreModel.readData(["JsonData1", "JsonData2", "JsonData3", "jsondata4Search"]);
		}*/
		}

		private function onMouseUp(event:MouseEvent):void
		{
			if (event.target.name == "back_btn")
			{
				appModel.viewManager.viewID = Config.M20_HOME_MODULE;
			}
		}

		private function systemError():void
		{
			if (_dialogComponent)
				_dialogComponent.dispose();

			var dialogSignal:Signal = new Signal(String);
			dialogSignal.addOnce(onAlertError);
			_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ข้อมูลมีปัญหากรุณาเข้าสู่ระบบอีกครับ</font>", DialogComponent.CLOSE, dialogSignal);
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
		}

		private function onAlertError(value:String):void
		{
			_dialogComponent.dispose();
			_dialogComponent = null;

			appModel.viewManager.viewID = Config.M00_INIT_MODULE;
		}

		private function onComplete():void
		{
			_dialogComponent.dispose();
			appModel.viewManager.viewID = Config.M40_SURVEY_MODULE;
		}


		// event ------------------------------------------------------------------

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
