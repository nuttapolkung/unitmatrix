package com.unitmatrix.module.questionnaire.view
{

	import com.bit101.components.PushButton;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.questionnaire.QuestionnaireContext;

	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.media.StageWebView;

	import org.robotlegs.core.IInjector;

	public class QuestionnaireModule extends ModuleBase
	{
		private var _from_login:Sprite;
		private var _block:Sprite;

		public var webView:StageWebView;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new QuestionnaireContext(this, value);
		}

		// -----------------------------------------------------------------------------------------------------------------------


		public function QuestionnaireModule()
		{

		}

		[Embed(source = '/bg.jpg')]
		private var BG_CLASS:Class;
		private var bg:Bitmap = new BG_CLASS();

		override public function create():void
		{
			this.graphics.beginFill(0xffffff, 1.0);
			this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();

			bg.width = Config.GUI_SIZE.width;
			bg.height = Config.GUI_SIZE.height;
			bg.smoothing = true;
			addChild(bg);

			webView = new StageWebView(true);
			webView.stage = this.stage;
			webView.viewPort = new Rectangle(0, 50, stage.stageWidth, stage.stageHeight - 50);

			var testFile:File = File.applicationDirectory.resolvePath("questionnaire.html");
			var testFileCopy:File = File.createTempFile();
			testFile.copyTo(testFileCopy, true);

			var fs:FileStream = new FileStream();
			fs.open(testFile, FileMode.READ);
			var w:String = fs.readUTFBytes(fs.bytesAvailable);
			fs.close();

			webView.loadURL("http://sena.crmthai.net/QS.php");

//			webView.loadString(w);

			var back_btn:PushButton = new PushButton(this, 0, 0, "Back");
			back_btn.name = "back_btn";


		}


		override public function activate(onActivated:Function, prevView:IView = null):void
		{

			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			dispose();

			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			webView.stage = null;

			destroy();
			super.dispose();
		}

	}
}
