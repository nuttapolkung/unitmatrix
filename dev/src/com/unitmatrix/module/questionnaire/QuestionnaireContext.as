package com.unitmatrix.module.questionnaire
{

	import com.unitmatrix.module.questionnaire.controller.RequestInitAppDataCommand;
	import com.unitmatrix.module.questionnaire.model.QuestionnaireModel;
	import com.unitmatrix.module.questionnaire.remote.service.QuestionnaireService;
	import com.unitmatrix.module.questionnaire.signals.ReceiveInitAppDataSignal;
	import com.unitmatrix.module.questionnaire.signals.RequestInitAppDataSignal;
	import com.unitmatrix.module.questionnaire.view.QuestionnaireMediator;
	import com.unitmatrix.module.questionnaire.view.QuestionnaireModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class QuestionnaireContext extends ModuleContext
	{
		public function QuestionnaireContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(QuestionnaireModel);
			injector.mapSingleton(QuestionnaireService);
			injector.mapSingleton(ReceiveInitAppDataSignal);

			signalCommandMap.mapSignalClass(RequestInitAppDataSignal, RequestInitAppDataCommand);

			mediatorMap.mapView(QuestionnaireModule, QuestionnaireMediator);

			super.startup();
		}

		override public function dispose():void
		{

			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
