package com.unitmatrix.module.init
{

	import com.unitmatrix.module.init.controller.RequestUserDataCommand;
	import com.unitmatrix.module.init.model.InitModel;
	import com.unitmatrix.module.init.remote.services.InitService;
	import com.unitmatrix.module.init.signals.ReceiveUserDataSignal;
	import com.unitmatrix.module.init.signals.RequestUserDataSignal;
	import com.unitmatrix.module.init.view.InitMediator;
	import com.unitmatrix.module.init.view.InitModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class InitContext extends ModuleContext
	{
		public function InitContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(InitModel);
			injector.mapSingleton(InitService);
			injector.mapSingleton(ReceiveUserDataSignal);

			signalCommandMap.mapSignalClass(RequestUserDataSignal, RequestUserDataCommand);
			mediatorMap.mapView(InitModule, InitMediator);

			super.startup();
		}

		override public function dispose():void
		{

			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);



			super.dispose();
		}
	}
}
