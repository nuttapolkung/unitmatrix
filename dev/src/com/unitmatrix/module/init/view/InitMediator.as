package com.unitmatrix.module.init.view
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.module.init.model.InitModel;
	import com.unitmatrix.module.init.signals.ReceiveUserDataSignal;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.StatusEvent;
	import flash.net.URLRequest;

	import Main.offline_sp;

	import air.net.URLMonitor;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class InitMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:InitModule;

		[Inject]
		public var model:InitModel;

		[Inject]
		public var receiveUserDataSignal:ReceiveUserDataSignal;


		// create ------------------------------------------------------------------

		private var _preload:MovieClip;
		private var _monitor:Object;
		private var _dialogComponent:DialogComponent;

		private var _icon:Sprite;

		override public function onRegister():void
		{
			coreModel.systemErrorSignal.add(systemError);

			module.create();

//			redirect to home view.
			appModel.viewManager.viewID = Config.M20_HOME_MODULE;
		}


		private function systemError():void
		{
			if (_dialogComponent)
				_dialogComponent.dispose();

			var dialogSignal:Signal = new Signal(String);
			dialogSignal.addOnce(onAlertError);
			_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ข้อมูลมีปัญหากรุณาเข้าสู่ระบบอีกครับ</font>", DialogComponent.CLOSE, dialogSignal);
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
		}

		private function onAlertError(value:String):void
		{
			_dialogComponent.dispose();
			_dialogComponent = null;
			checkInternet();


			_dialogComponent = new DialogComponent("แจ้งเตือน", "กำลังตรวจสอบสถานะการเชื่อมต่อ internet\nกรุณารอสักครู่");
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
		}

		private function checkInternet():void
		{
			_monitor = new URLMonitor(new URLRequest('http://www.adobe.com'));
			_monitor.addEventListener(StatusEvent.STATUS, netConnectivity);
			_monitor.start();
		}

		protected function netConnectivity(e:StatusEvent):void
		{
			if (_icon)
				module.removeChild(_icon);

			if (_monitor.available)
			{
				_icon = new Main.online_sp;
				coreModel.isOnline = true;
			}
			else
			{
				_icon = new Main.offline_sp;
				coreModel.isOnline = false;
			}

			_icon.x = module.stage.stageWidth - (_icon.width + 10);
			_icon.y = module.stage.stageHeight - (_icon.height + 10);

			module.addChild(_icon);

			if (_dialogComponent)
			{
				_dialogComponent.dispose();
				_dialogComponent = null;
			}


			_dialogComponent = new DialogComponent("แจ้งเตือน", "กำลังตรวจสอบ version ของ application\nกรุณารอสักครู่");
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
			// load Config data
			receiveUserDataSignal.add(onComplete)
			model.getUserData();

		}

		private function onComplete():void
		{
			if (_dialogComponent)
			{
				_dialogComponent.dispose();
				_dialogComponent = null;
			}

			if (appModel.viewManager.viewID == Config.M00_INIT_MODULE)
			{
				if (!coreModel.isLogined)
					appModel.viewManager.viewID = Config.M30_RESERVEROOM_MODULE;
				else
					appModel.viewManager.viewID = Config.M20_HOME_MODULE;
				coreModel.systemErrorSignal.remove(systemError);
			}
		}

		// event ------------------------------------------------------------------

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
