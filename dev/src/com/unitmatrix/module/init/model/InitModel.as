package com.unitmatrix.module.init.model
{
	import com.unitmatrix.module.init.signals.RequestUserDataSignal;

	import org.robotlegs.mvcs.Actor;

	public class InitModel extends Actor
	{
		// internal ------------------------------------------------------------------

		[Inject]
		public var requestUserDataSignal:RequestUserDataSignal;


		public function getUserData():void
		{
			requestUserDataSignal.dispatch();
		}


	}
}
