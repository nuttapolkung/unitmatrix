package com.unitmatrix.module.init.remote.services
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.UserData;
	import com.unitmatrix.module.init.model.InitModel;
	import com.unitmatrix.module.init.signals.ReceiveUserDataSignal;
	import com.idealizm.manager.LoadManager;
	import com.sleepydesign.utils.JSONUtil;

	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLVariables;
	import flash.utils.ByteArray;

	import net.pirsquare.m.MobileFileUtil;


	public class InitService
	{

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var model:InitModel;

		[Inject]
		public var receiveUserDataSignal:ReceiveUserDataSignal;

		private var _version:String;

		public function loadConfig():void
		{
			if (coreModel.isOnline)
				LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/chkversion.php", new URLVariables).addOnce(onLoadConfig);
			else
				onLoadDataLocal();

		}

		private function onLoadConfig(raw:String):void
		{
			var data:Array = JSONUtil.decode(raw);
			if (data)
				_version = data[0].version;
			else
			{
				coreModel.systemErrorSignal.dispatch();
				return;
			}

			var file:File = MobileFileUtil.getFile("bmmt/config.dat");
			if (file.exists)
			{
				MobileFileUtil.open("bmmt/config.dat").addOnce(function(ba:ByteArray):void
				{

					var version:String = (ba) ? ba.toString() : "";
					trace("version : " + version);
					if (_version != version)
						onLoadData();
					else
						onLoadDataLocal();

				});

			}
			else
			{
				onLoadData();
			}

		}

		private function onLoadDataLocal():void
		{
			MobileFileUtil.open("bmmt/user.dat").addOnce(function(ba:ByteArray):void
			{
				var raw:String = ba.toString();
				onLoadComplete(raw);
			});
		}

		private function onLoadData():void
		{
			// save new version
			save();
			// load new data
			LoadManager.loadJson(Config.SERVER_DATA + "bmmt_ai/chkuser.php", new URLVariables).addOnce(onLoadComplete);

		}

		private function onLoadComplete(raw:String):void
		{
			var data:Array = JSONUtil.decode(raw);
			var userDatas:Vector.<UserData> = new Vector.<UserData>();
			for each (var obj:Object in data)
			{
				var userData:UserData = new UserData(obj.usercd, obj.firstname, obj.lastname, obj.groupid, obj.username, obj.password);
				userDatas.push(userData);
			}
			coreModel.userDatas = userDatas;

			saveUser();

			receiveUserDataSignal.dispatch();
		}

		private function saveUser():void
		{
			var raw:String = JSONUtil.encode(coreModel.userDatas);
			var file:File = MobileFileUtil.getFile("bmmt/user.dat");
			var fileStream:FileStream = new FileStream();

			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeUTFBytes(raw);
			fileStream.close();
		}

		private function save():void
		{
			var file:File = MobileFileUtil.getFile("bmmt/config.dat");
			var fileStream:FileStream = new FileStream();

			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeUTFBytes(_version);
			fileStream.close();
		}
	}
}
