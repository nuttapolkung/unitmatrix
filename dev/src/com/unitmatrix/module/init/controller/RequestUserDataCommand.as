package com.unitmatrix.module.init.controller
{
	import com.unitmatrix.module.init.remote.services.InitService;

	import org.robotlegs.mvcs.SignalCommand;

	public class RequestUserDataCommand extends SignalCommand
	{
		[Inject]
		public var service:InitService;


		//-get carees data from server.
		override public function execute():void
		{
			service.loadConfig();
			//-
			super.execute();
		}
	}
}
