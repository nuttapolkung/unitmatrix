package com.unitmatrix.module.init.signals
{
	import org.osflash.signals.Signal;

	public class ReceiveUserDataSignal extends Signal
	{
		public function ReceiveUserDataSignal(... parameters)
		{
			super(parameters);
		}
	}
}
