package com.unitmatrix.module.roomdetail
{
	import com.unitmatrix.module.roomdetail.view.RoomDetailMediator;
	import com.unitmatrix.module.roomdetail.view.RoomDetailModule;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class RoomDetailContext extends ModuleContext
	{
		public function RoomDetailContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{

			mediatorMap.mapView(RoomDetailModule, RoomDetailMediator);

			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
