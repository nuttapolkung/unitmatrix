package com.unitmatrix.module.roomdetail.view
{
	import com.bit101.components.Label;
	import com.bit101.components.PushButton;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.sleepydesign.utils.AlignUtil;
	import com.unitmatrix.module.roomdetail.RoomDetailContext;

	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import BridgeInfo.bridge_info_sp;

	import org.robotlegs.core.IInjector;

	public class RoomDetailModule extends ModuleBase
	{

		[Embed(source = '/room.png')]
		private var BG_CLASS:Class;
		private var bg:Bitmap = new BG_CLASS();

		private var _bridge_sp:Sprite;
		private var _selectMenu:int;


		private var contentScroll:ScrollController;
		private var _container:Sprite;
		private var _content:Sprite;

		public function get selectMenu():int
		{
			return _selectMenu;
		}

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new RoomDetailContext(this, value);
		}

		public function createRoom(RoomName:String):void
		{
			var label:Label = new Label(this, 200, 200, RoomName);
		}

		override public function create():void
		{
			this.graphics.beginFill(0x000000, 0.5);
			this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();

			var back_btn:PushButton = new PushButton(this, 0, 0, "close");
			back_btn.name = "back_btn";

			AlignUtil.align(AlignUtil.MIDDLE_CENTER, bg, new Rectangle(0, 0, stage.stageWidth, stage.stageHeight));
			addChild(bg);
		}

		private function resize():void
		{
			_content.scaleX = _content.scaleY = Config.ratio;
			_content.x = Config.GUI_SIZE.width * 0.5 - _content.width * 0.5;
			_content.y = Config.GUI_SIZE.height * 0.5 - _content.height * 0.5;
		}

		public function set selectMenu(index:int):void
		{
			var header_btn:MovieClip;
			for (var i:int = 1; i < 6; i++)
			{
				header_btn = _bridge_sp.getChildByName("bridge_info_" + i + "_btn") as MovieClip;
				header_btn.gotoAndStop(2);
			}

			header_btn = _bridge_sp.getChildByName("bridge_info_" + index + "_btn") as MovieClip;
			header_btn.gotoAndStop(1);

		}


		public function addContent(content:DisplayObjectContainer):void
		{
			if (!_container)
			{
				_container = new Sprite;
				_content.addChild(_container);
			}
			else
			{
				DisplayObjectUtil.removeChildren(_container);
			}

			content.x = 55;
			content.y = 175;
			_container.addChild(content);

		}

		public function setViewContent(content:DisplayObjectContainer):void
		{
			if (contentScroll)
				contentScroll.removeScrollControll();

			var container:Sprite = new Sprite();
			container.addChild(content);
			addContent(container);

			var containerNormalViewport:Rectangle = new Rectangle(0, 0, 915, 512);

			contentScroll.horizontalScrollingEnabled = false;
			contentScroll.addScrollControll(content, container, containerNormalViewport);

		}

		public function getBTN(value:String):MovieClip
		{
			return _bridge_sp.getChildByName("bridge_info_" + value + "_btn") as MovieClip;
		}

		public function get container_mc():MovieClip
		{
			return _bridge_sp.getChildByName("container_mc") as MovieClip;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			destroy();
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}


	}
}
