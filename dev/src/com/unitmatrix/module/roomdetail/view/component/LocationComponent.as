package com.unitmatrix.module.roomdetail.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import Tab.location_detail_sp;

	public class LocationComponent extends SDSprite
	{
		private var _skin:Sprite;

		public function LocationComponent(positionlat:String, positionlng:String, province_name:String, amphur_name:String, tambol_name:String, position:String, directionname:String, environment:String)
		{
			_skin = new Tab.location_detail_sp;
			addChild(_skin);

			positionlat_tf.text = positionlat || "ไม่มีข้อมูล";
			positionlng_tf.text = positionlng || "ไม่มีข้อมูล";
			province_name_tf.text = province_name || "ไม่มีข้อมูล";
			amphur_name_tf.text = amphur_name || "ไม่มีข้อมูล";
			tambol_name_tf.text = tambol_name || "ไม่มีข้อมูล";
			position_tf.text = position || "ไม่มีข้อมูล";
			directionname_tf.text = directionname || "ไม่มีข้อมูล";
			environment_tf.text = environment || "ไม่มีข้อมูล";
		}

		public function get positionlat_tf():TextField
		{
			return _skin.getChildByName("positionlat_tf") as TextField;
		}

		public function get positionlng_tf():TextField
		{
			return _skin.getChildByName("positionlng_tf") as TextField;
		}

		public function get province_name_tf():TextField
		{
			return _skin.getChildByName("province_name_tf") as TextField;
		}

		public function get amphur_name_tf():TextField
		{
			return _skin.getChildByName("amphur_name_tf") as TextField;
		}

		public function get tambol_name_tf():TextField
		{
			return _skin.getChildByName("tambol_name_tf") as TextField;
		}

		public function get position_tf():TextField
		{
			return _skin.getChildByName("position_tf") as TextField;
		}

		public function get directionname_tf():TextField
		{
			return _skin.getChildByName("directionname_tf") as TextField;
		}

		public function get environment_tf():TextField
		{
			return _skin.getChildByName("environment_tf") as TextField;
		}
	}
}
