package com.unitmatrix.module.roomdetail.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import Tab.geometry_sp;

	public class GeometryComponent extends SDSprite
	{
		private var _skin:Sprite;

		public function GeometryComponent(skew:String, lane:String, length_data:String, surfacewidth:String, size:String)
		{
			_skin = new Tab.geometry_sp;
			addChild(_skin);

			skew_tf.text = skew || "ไม่มีข้อมูล";
			lane_tf.text = lane || "ไม่มีข้อมูล";
			length_tf.text = length_data + " เมตร" || "ไม่มีข้อมูล";
			surfacewidth_tf.text = surfacewidth + " เมตร" || "ไม่มีข้อมูล";
			size_tf.text = size || "ไม่มีข้อมูล";
		}

		public function get skew_tf():TextField
		{
			return _skin.getChildByName("skew_tf") as TextField;
		}

		public function get lane_tf():TextField
		{
			return _skin.getChildByName("lane_tf") as TextField;
		}

		public function get length_tf():TextField
		{
			return _skin.getChildByName("length_tf") as TextField;
		}

		public function get surfacewidth_tf():TextField
		{
			return _skin.getChildByName("surfacewidth_tf") as TextField;
		}

		public function get size_tf():TextField
		{
			return _skin.getChildByName("size_tf") as TextField;
		}
	}
}
