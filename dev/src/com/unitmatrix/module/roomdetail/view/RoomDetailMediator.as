package com.unitmatrix.module.roomdetail.view
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.module.roomdetail.view.component.GeometryComponent;
	import com.unitmatrix.module.roomdetail.view.component.LocationComponent;
	import com.unitmatrix.module.roomdetail.view.component.StructureComponent;
	import com.unitmatrix.module.roomdetail.view.component.VehicleComponent;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class RoomDetailMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:RoomDetailModule;


		// create ------------------------------------------------------------------

		private var current_page:int;
		private var total_page:int;
		private var row_per_page:int = 10;
		private var _bridgeDetailData:BridgeDetailData;


		override public function onRegister():void
		{

			module.create();
			module.createRoom(coreModel.RoomName);

			initMouseHandler();
		}


		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function hideMenu():void
		{
			if (!_bridgeDetailData.repairdetail || _bridgeDetailData.repairdetail.length < 1)
			{
				module.getBTN("5").visible = false;
				module.getBTN("5").mouseEnabled = false;
			}

		}


		// event ------------------------------------------------------------------
		private function onMouseUp(event:Event):void
		{


			if (event.target.name == "back_btn")
			{
				appModel.viewManager.viewID = Config.M30_RESERVEROOM_MODULE;
			}

		}


		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
