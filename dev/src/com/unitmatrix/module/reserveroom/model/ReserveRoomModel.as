package com.unitmatrix.module.reserveroom.model
{
	import com.unitmatrix.module.reserveroom.signals.RequestLoginSignal;

	import org.robotlegs.mvcs.Actor;

	public class ReserveRoomModel extends Actor
	{

		[Inject]
		public var requestLoginSignal:RequestLoginSignal;

		public function prepareLogin(username:String, password:String):void
		{
			requestLoginSignal.dispatch(username, password);
		}
	}
}
