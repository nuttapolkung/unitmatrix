package com.unitmatrix.module.reserveroom.controller
{
	import com.unitmatrix.module.reserveroom.remote.services.ReserveRoomService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class RequestReserveRoomCommand extends SignalCommand
	{
		[Inject]
		public var service:ReserveRoomService;
		
		[Inject(name='username')]
		public var username_str:String;
		
		[Inject(name='password')]
		public var password_str:String;
		
		public function RequestReserveRoomCommand()
		{
			super();
		}
		
		override public function execute():void
		{
			service.loginWithUsernameAndPassword(username_str,password_str);
		}
	}
}