package com.unitmatrix.module.reserveroom.remote.services
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.UserData;
	import com.unitmatrix.module.reserveroom.model.data.ReserveRoomData;
	import com.unitmatrix.module.reserveroom.signals.ReceivedLoginResult;
	import com.sleepydesign.utils.VectorUtil;

	public class ReserveRoomService
	{
		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var receiveLoginResultSignal:ReceivedLoginResult;

		public function loginWithUsernameAndPassword(username:String, password:String):void
		{

			var userData:UserData = VectorUtil.getItemBy(coreModel.userDatas, username, "username");

			if (!userData)
			{
				receiveLoginResultSignal.dispatch(ReserveRoomData.LOGIN_RESULT_ERROR, null);
				return;
			}

			if (userData.password)
			{
				coreModel.currentUserData = userData;
				receiveLoginResultSignal.dispatch(ReserveRoomData.LOGIN_RESULT_COMPLETE, null);
			}
			else
			{
				receiveLoginResultSignal.dispatch(ReserveRoomData.LOGIN_RESULT_ERROR, null);
			}
		}

	}
}
