package com.unitmatrix.module.reserveroom.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.UserData;
	import com.unitmatrix.module.reserveroom.model.ReserveRoomModel;
	import com.unitmatrix.module.reserveroom.model.data.ReserveRoomData;
	import com.unitmatrix.module.reserveroom.signals.ReceivedLoginResult;

	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	import net.pirsquare.m.MobileFileUtil;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class ReserveRoomMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:ReserveRoomModule;
		[Inject]
		public var model:ReserveRoomModel;

		[Inject]
		public var receiveLoginSignal:ReceivedLoginResult;


		// create ------------------------------------------------------------------
		private var _dialogComponent:DialogComponent;

		private var _dialogSignal:Signal;

		override public function onRegister():void
		{
			module.create();

			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			if (event.target.name == "back_btn")
				appModel.viewManager.viewID = Config.M20_HOME_MODULE;
			else if (event.target.name.indexOf("floor") != -1)
				module.createRoomSlot();
			else if (event.target.name.indexOf("room") != -1)
			{
				coreModel.RoomName = event.target.name;
				appModel.viewManager.viewID = Config.M60_ROOM_DETAIL_MODULE;
			}

		}

		private function prepareLogin():void
		{
			if (module.userName == "ชื่อผูใชงาน" || module.password == "รหัสผาน" || module.userName == "" || module.password == "")
			{
				dialog("แจ้งเตือน", "<font color='#ff0000'>*กรุณากรอก username และ password</font>", DialogComponent.CLOSE);
			}
			else
			{
				dialog("แจ้งเตือน", "<font color='#ff0000'>กำลังตรวจสอบข้อมูล</font>", DialogComponent.NONE);
				receiveLoginSignal.addOnce(onLoginResult);
				model.prepareLogin(module.userName, module.password);

			}
		}

		private function onLoginResult(result:String, userData:UserData):void
		{
			_dialogComponent.dispose();
			_dialogComponent = null;

			if (result == ReserveRoomData.LOGIN_RESULT_COMPLETE)
			{
				appModel.viewManager.viewID = Config.M10_QUESTIONNAIRE_MODULE;
				save();
			}
			else
				dialog("แจ้งเตือน", "<font color='#ff0000'>มีความผิดพลาดขณะตรวจสอบข้อมูล</font>", DialogComponent.CLOSE);

		}

		private function save():void
		{
			var file:File = MobileFileUtil.getFile("bmmt/isLogin.dat");
			var fileStream:FileStream = new FileStream();

			fileStream.openAsync(file, FileMode.WRITE);
			fileStream.writeUTFBytes("true");
			fileStream.close();
		}


		private function dialog(title:String, content:String, dialog_type:String):void
		{
			module.showText = false;
			_dialogSignal = new Signal(String);
			_dialogSignal.addOnce(onDialogClick);

			_dialogComponent = new DialogComponent(title, content, dialog_type, _dialogSignal);
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
		}


		private function onDialogClick(action:String):void
		{
			if (module)
				module.showText = true;

			if (_dialogComponent)
			{
				_dialogComponent.dispose();
				_dialogComponent = null;
			}

			if (_dialogSignal)
			{
				_dialogSignal.removeAll();
				_dialogSignal = null;
			}

		}

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
