package com.unitmatrix.module.home.view
{
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.unitmatrix.core.model.CoreModel;

	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;


	public class HomeMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:HomeModule;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			coreModel.changeViewSignal.add(onChangeView);

			module.create();
			initMouseHandler();

			insertAccount();
//			checkLogin();
		}

		private function insertAccount():void
		{
			var url:String = "http://sena.crmthai.net/WB_Service_AI/accounts/insert_accounts";
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();

			var data:Object = new Object();
			data.smownerid = "11034";
			data.account_no = "";
			data.email1 = "ekkachai1@aisyst.com";
			data.cf_955 = "Ekkachai";
			data.cf_768 = "Fungfueng";
			data.cf_1112 = "2007-01-04";
			data.cf_952 = "1800800039077";
			data.cf_1582 = "กลาง";
			data.cf_1336 = "อ่างทอง";
			data.cf_1214 = "นาย / Mr.";
			data.cf_1439 = "10";
			data.cf_1440 = "03";
			data.cf_1441 = "2528";

			requestVars["AI-API-KEY"] = "1234";
			requestVars.module = "Accounts";
			requestVars.crmid = "992736";
			requestVars.action = "add";
			requestVars.data = [data];


			requestVars.sessionTime = new Date().getTime();
			request.data = requestVars;
			request.method = URLRequestMethod.POST;

			var urlLoader:URLLoader = new URLLoader();
			urlLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, loaderCompleteHandler, false, 0, true);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);

			for (var prop:String in requestVars)
			{
				trace("Sent: " + prop + " is: " + requestVars[prop]);
			}
			try
			{
				urlLoader.load(request);
			}
			catch (e:Error)
			{
				trace(e);
			}
		}


		private function checkLogin():void
		{
			var url:String = "http://sena.crmthai.net/WB_Service_AI/accounts/check_member";
			var request:URLRequest = new URLRequest(url);
			var requestVars:URLVariables = new URLVariables();
			requestVars["AI-API-KEY"] = "1234";
			requestVars.module = "Accounts";
			requestVars.email1 = "ekkachai1@aisyst.com";
			requestVars.cf_955 = "Ekkachai";
			requestVars.cf_768 = "Fungfueng";
			requestVars.sessionTime = new Date().getTime();
			request.data = requestVars;
			request.method = URLRequestMethod.POST;

			var urlLoader:URLLoader = new URLLoader();
			urlLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			urlLoader.addEventListener(Event.COMPLETE, loaderCompleteHandler, false, 0, true);
			urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
			urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler, false, 0, true);
			urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler, false, 0, true);

			for (var prop:String in requestVars)
			{
				trace("Sent: " + prop + " is: " + requestVars[prop]);
			}
			try
			{
				urlLoader.load(request);
			}
			catch (e:Error)
			{
				trace(e);
			}
		}

		private function loaderCompleteHandler(e:Event):void
		{
			var json_string:String = e.target.data as String;
//			{"Type":"E","Message":"This is no longer a member","cache_time":"2014-11-20 22:04:47","total":0,"offset":"0","limit":"1","data":[]}
//			var responseVars = URLVariables(e.target.data);
			trace("responseVars: " + json_string);

		}

		private function httpStatusHandler(e:HTTPStatusEvent):void
		{
			trace("httpStatusHandler:" + e);
		}

		private function securityErrorHandler(e:SecurityErrorEvent):void
		{
			trace("securityErrorHandler:" + e);
		}

		function ioErrorHandler(e:IOErrorEvent):void
		{
			trace("ORNLoader:ioErrorHandler: " + e);
		}

		private function onChangeView(value:String):void
		{

		}

		// event ------------------------------------------------------------------

		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_DOWN, onMouseDown);
		}

		private function onMouseDown(event:MouseEvent):void
		{
			trace("target name " + event.target.name);
			if (event.target.name == "reserve_btn")
				appModel.viewManager.viewID = Config.M30_RESERVEROOM_MODULE;
			else if (event.target.name == "questionnaire_btn")
				appModel.viewManager.viewID = Config.M10_QUESTIONNAIRE_MODULE;
		}

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			appModel.viewManager.removeViewByID(module.ID);
			coreModel.changeViewSignal.remove(onChangeView);

			appModel = null;
			module = null;
		}
	}
}
