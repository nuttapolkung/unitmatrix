package com.unitmatrix.module.home.view
{
	import com.bit101.components.PushButton;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.unitmatrix.module.home.HomeContext;

	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;
	import flash.media.StageWebView;
	import flash.net.URLRequest;

	import org.robotlegs.core.IInjector;


	public class HomeModule extends ModuleBase
	{

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new HomeContext(this, value);
		}

		override public function create():void
		{
			var xpos:int = 100;
			var ypos:int = 200;
			var reserve_btn:PushButton = new PushButton(this, xpos, ypos, "reserve room");
			reserve_btn.name = "reserve_btn";
			var questionnaire_btn:PushButton = new PushButton(this, xpos, ypos + 50, "questionnaire");
			questionnaire_btn.name = "questionnaire_btn";

			var html:StageWebView = new StageWebView(true);
			html.stage = stage;
			html.viewPort = new Rectangle(stage.stageWidth - 100, 0, 100, 20);
			html.loadURL("http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fsenadevelopmentplc&width&layout=button&action=like&show_faces=true&share=true&height=80&appId=445785635563219");

		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

	}
}
