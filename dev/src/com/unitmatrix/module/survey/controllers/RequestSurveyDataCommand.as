package com.unitmatrix.module.survey.controllers
{
	import com.unitmatrix.module.survey.remote.services.SurveyService;
	
	import org.robotlegs.mvcs.SignalCommand;
	
	public class RequestSurveyDataCommand extends SignalCommand
	{
		[Inject]
		public var service:SurveyService;
		
		override public function execute():void
		{
			service.loadData();
		}
	}
}