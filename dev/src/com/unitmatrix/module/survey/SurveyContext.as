package com.unitmatrix.module.survey
{
	import com.unitmatrix.module.survey.controllers.RequestSurveyDataCommand;
	import com.unitmatrix.module.survey.model.SurveyModel;
	import com.unitmatrix.module.survey.remote.services.SurveyService;
	import com.unitmatrix.module.survey.signals.ReceivedSurveyDataSignal;
	import com.unitmatrix.module.survey.signals.RequestSurveyDataSignal;
	import com.unitmatrix.module.survey.view.SurveyMediator;
	import com.unitmatrix.module.survey.view.SurveyModule;
	import com.unitmatrix.module.survey.view.component.SearchMediator;
	import com.unitmatrix.module.survey.view.component.SearchView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class SurveyContext extends ModuleContext
	{
		public function SurveyContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(SurveyModel);
			injector.mapSingleton(SurveyService);

			injector.mapSingleton(ReceivedSurveyDataSignal);

			signalCommandMap.mapSignalClass(RequestSurveyDataSignal, RequestSurveyDataCommand);

			mediatorMap.mapView(SearchView, SearchMediator)
			mediatorMap.mapView(SurveyModule, SurveyMediator);



			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			super.dispose();
		}
	}
}
