package com.unitmatrix.module.survey.view
{
	import com.unitmatrix.module.survey.SurveyContext;
	import com.unitmatrix.module.survey.model.data.SurveyData;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.text.TextField;

	import org.osflash.signals.Signal;
	import org.robotlegs.core.IInjector;
	import com.unitmatrix.module.survey.view.component.SurveyItem;

	public class SurveyModule extends ModuleBase
	{
		public var selectedSingal:Signal = new Signal(String, String);
		public var surveyItems:Vector.<SurveyItem>;

		private var survey_table:Sprite;
		private var container:Sprite;

		private var search_btn:SimpleButton;
		private var paging_tf:TextField;
		private var next_btn:SimpleButton;
		private var privious_btn:SimpleButton;


		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new SurveyContext(this, value);
		}

		override public function create():void
		{
			survey_table = new survey_table_sp();
			addChild(survey_table);

			container = survey_table.getChildByName("survey_container_sp") as Sprite;

			var paging_sp:Sprite = survey_table.getChildByName("paging_index_sp") as Sprite;

			next_btn = paging_sp.getChildByName("next_btn") as SimpleButton;
			privious_btn = paging_sp.getChildByName("privious_btn") as SimpleButton;
			paging_tf = paging_sp.getChildByName("page_text") as TextField;

			paging_tf.text = "1/1";

			resize();
		}

		private function resize():void
		{
			survey_table.scaleX = survey_table.scaleY = Config.ratio;
			survey_table.x = Config.GUI_SIZE.width * 0.5 - survey_table.width * 0.5;
			survey_table.y = Config.GUI_SIZE.height * 0.5 - survey_table.height * 0.5;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{
			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			destroy();
			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

		public function createSurveyListWithData(surveys:Vector.<SurveyData>):void
		{
			DisplayObjectUtil.removeChildren(container, true, true);
			surveyItems = new Vector.<SurveyItem>;
			for (var i:int = 0; i < surveys.length; i++)
			{
				var survey_data:SurveyData = surveys[i];
				var surveyItem:SurveyItem = new SurveyItem(survey_data.id, survey_data.rank.toString(), survey_data.group, survey_data.highway_number, survey_data.bridge_name, survey_data.localtion, survey_data.direction, survey_data.want_to_save, selectedSingal);
				surveyItem.y = (i * SurveyItem.HEIGHT);
				container.addChild(surveyItem);
				surveyItems.push(surveyItem);
			}

			if (surveys.length == 0)
			{
				var errorText:Sprite = new error_txt_sp;
				errorText.x = 969 / 2 - errorText.width / 2;
				errorText.y = 480 / 2 - errorText.height / 2;

				container.addChild(errorText);
			}
		}

		public function getSurveyItemsByID(id:String):SurveyItem
		{
			return VectorUtil.getItemByID(surveyItems, id) as SurveyItem;
		}

		public function setPagingText(current_page:int, total_page:int):void
		{
			paging_tf.text = (current_page + 1) + "/" + total_page;

			if (current_page + 1 == total_page)
			{
				next_btn.visible = false;
				privious_btn.visible = true;
			}
			else if (current_page == 0)
			{
				privious_btn.visible = false;
				next_btn.visible = true;
			}
			else
			{
				next_btn.visible = true;
				privious_btn.visible = true;
			}

		}
	}
}
