package com.unitmatrix.module.survey.view.component
{
	import com.bit101.components.ComboBox;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.net.LoaderUtil;

	import flash.display.Sprite;
	import flash.text.TextField;

	import Dialog.search_sp;

	import SurveyInfo.block_image;

	import org.osflash.signals.Signal;

	public class SearchView extends SDSprite
	{
		public var completeSignal:Signal = new Signal;

		private var _container:Sprite;
		private var _skin:Sprite;
		private var _bpiscore_combo:ComboBox;
		private var _group_combo:ComboBox;
		private var _surface_combo:ComboBox;
		private var _highway_combo:ComboBox;
		private var _category_combo:ComboBox;

		public function dispose():void
		{
			completeSignal.dispatch();
			destroy();
		}

		public function create():void
		{

			_container = new Sprite();
			addChild(_container);
			_container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			_container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			_container.graphics.endFill();
			_container.alpha = 0.8;

			_skin = new Dialog.search_sp;
			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x = Config.GUI_SIZE.width * 0.5;
			_skin.y = Config.GUI_SIZE.height * 0.5;

			addChild(_skin);
			var sp:Sprite;
			if (!_group_combo)
			{
				sp = _skin.getChildByName("group_combo") as Sprite;
				_group_combo = new ComboBox();
				_group_combo.x = sp.x;
				_group_combo.y = sp.y;
				_group_combo.width = sp.width;
				_group_combo.height = sp.height;
				sp.parent.addChild(_group_combo);
				sp.parent.removeChild(sp);
			}
			if (!_surface_combo)
			{
				sp = _skin.getChildByName("surface_combo") as Sprite;
				_surface_combo = new ComboBox();
				_surface_combo.x = sp.x;
				_surface_combo.y = sp.y;
				_surface_combo.width = sp.width;
				_surface_combo.height = sp.height;
				sp.parent.addChild(_surface_combo);
				sp.parent.removeChild(sp);
			}
			if (!_highway_combo)
			{
				sp = _skin.getChildByName("highway_combo") as Sprite;
				_highway_combo = new ComboBox();
				_highway_combo.x = sp.x;
				_highway_combo.y = sp.y;
				_highway_combo.width = sp.width;
				_highway_combo.height = sp.height;
				sp.parent.addChild(_highway_combo);
				sp.parent.removeChild(sp);
			}
			if (!_category_combo)
			{
				sp = _skin.getChildByName("category_combo") as Sprite;
				_category_combo = new ComboBox();
				_category_combo.x = sp.x;
				_category_combo.y = sp.y;
				_category_combo.width = sp.width;
				_category_combo.height = sp.height;
				sp.parent.addChild(_category_combo);
				sp.parent.removeChild(sp);
			}
			if (!_bpiscore_combo)
			{
				sp = _skin.getChildByName("bpiscore_combo") as Sprite;
				_bpiscore_combo = new ComboBox();
				_bpiscore_combo.x = sp.x;
				_bpiscore_combo.y = sp.y;
				_bpiscore_combo.width = sp.width;
				_bpiscore_combo.height = sp.height;
				sp.parent.addChild(_bpiscore_combo);
				sp.parent.removeChild(sp);
			}
		}

		LoaderUtil

			public function get group_combo():ComboBox
		{
			return _group_combo;
		}

		public function get surface_combo():ComboBox
		{
			return _surface_combo;
		}

		public function get highway_combo():ComboBox
		{
			return _highway_combo;
		}

		public function get category_combo():ComboBox
		{
			return _category_combo;
		}

		public function get bpiscore_combo():ComboBox
		{
			return _bpiscore_combo;
		}

		public function get name_tf():TextField
		{
			return _skin.getChildByName("name_tf") as TextField;
		}

		public function get build_tf():TextField
		{
			return _skin.getChildByName("build_tf") as TextField;
		}

		public function get year_tf():TextField
		{
			return _skin.getChildByName("year_tf") as TextField;
		}

		public function get bpi_start_tf():TextField
		{
			return _skin.getChildByName("bpi_start_tf") as TextField;
		}

		public function get bpi_end_tf():TextField
		{
			return _skin.getChildByName("bpi_end_tf") as TextField;
		}

		public function get width_start_tf():TextField
		{
			return _skin.getChildByName("width_start_tf") as TextField;
		}

		public function get width_end_tf():TextField
		{
			return _skin.getChildByName("width_end_tf") as TextField;
		}

	}
}
