package com.unitmatrix.module.survey.view.component
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import org.osflash.signals.Signal;

	public class SurveyItem extends SDSprite
	{
		public static var HEIGHT:int = 48;
		public static var WIDTH:int = 967;
		public static var SURVEY:String = "survey";
		public static var SAVE:String = "save";
		public static var BRIDGE:String = "bridge";

		private var _skin:Sprite;
		private var _id:String;
		private var _isSave:Boolean;
		private var _selectedSingal:Signal;


		public function SurveyItem(id:String, rank:String, group:String, highway_number:String, bridge_name:String, localtion:String, direction:String, isSave:Boolean = false, selectedSingal:Signal = null)
		{
			_skin = new survey_detail_sp();
			addChild(_skin);

			_id = id || "ไม่มีข้อมูล";
			rank_tf.text = rank || "ไม่มีข้อมูล";
			group_tf.text = group || "ไม่มีข้อมูล";
			highway_number_tf.text = highway_number || "ไม่มีข้อมูล";
			bridge_name_tf.text = bridge_name || "ไม่มีข้อมูล";
			localtion_tf.text = localtion || "ไม่มีข้อมูล";
			direction_tf.text = direction || "ไม่มีข้อมูล";

			this.isSave = isSave;

			if (int(rank) % 2 == 0)
				bg_sp.alpha = 0;
			else
				bg_sp.alpha = .5;

			bridge_name_tf.mouseEnabled = false;

			scrollRect = new Rectangle(0, 0, WIDTH, HEIGHT);

			_selectedSingal = selectedSingal;
			if (_selectedSingal)
				addEventListener(MouseEvent.MOUSE_UP, onUp);
		}

		protected function onUp(event:MouseEvent):void
		{
			switch (event.target.name)
			{
				case "save_btn":
					_selectedSingal.dispatch(id, SAVE);
					break;
				case "survey_btn":
					_selectedSingal.dispatch(id, SURVEY);
					break;
				case "bridge_btn":
					_selectedSingal.dispatch(id, BRIDGE);
					break;

			}
		}

		public function get id():String
		{
			return _id;
		}

		public function get rank_tf():TextField
		{
			return _skin.getChildByName("rank_tf") as TextField;
		}

		public function get group_tf():TextField
		{
			return _skin.getChildByName("group_tf") as TextField;
		}

		public function get highway_number_tf():TextField
		{
			return _skin.getChildByName("highway_number_tf") as TextField;
		}

		public function get bridge_name_tf():TextField
		{
			return _skin.getChildByName("bridge_name_tf") as TextField;
		}

		public function get localtion_tf():TextField
		{
			return _skin.getChildByName("localtion_tf") as TextField;
		}

		public function get direction_tf():TextField
		{
			return _skin.getChildByName("direction_tf") as TextField;
		}

		public function get bg_sp():Sprite
		{
			return _skin.getChildByName("bg_sp") as Sprite;
		}

		public function get save_btn():SimpleButton
		{
			return _skin.getChildByName("save_btn") as SimpleButton;
		}

		public function get isSave():Boolean
		{
			return _isSave;
		}

		public function set isSave(value:Boolean):void
		{
			_isSave = value;

			if (!_isSave)
				toBW();
			else
				toColor();
		}

		private function toBW():void
		{
			var color:uint = 0xFF0000;
			var r:uint = (color >> 8) & 0xFF;
			var g:uint = (color >> 8) & 0xFF;
			var b:uint = (color >> 8) & 0xFF;

			var n:Number = 1 / 15;

			var matrix:Array = new Array();
			matrix = matrix.concat([n, n, n, 0, r]);
			matrix = matrix.concat([n, n, n, 0, g]);
			matrix = matrix.concat([n, n, n, 0, b]);
			matrix = matrix.concat([0, 0, 0, .9, 0]);

			var myRedMatrixFilter:ColorMatrixFilter = new ColorMatrixFilter(matrix);

			save_btn.filters = [myRedMatrixFilter];
			save_btn.mouseEnabled = false;
		}

		private function toColor():void
		{
			save_btn.filters = null;
			save_btn.mouseEnabled = true;
		}
	}
}
