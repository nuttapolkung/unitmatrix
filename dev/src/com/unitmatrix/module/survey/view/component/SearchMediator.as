package com.unitmatrix.module.survey.view.component
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.CategoryData;
	import com.unitmatrix.core.model.data.GroupData;
	import com.unitmatrix.core.model.data.HighwayData;
	import com.unitmatrix.core.model.data.SurfaceData;
	import com.unitmatrix.module.survey.model.SurveyModel;
	import com.unitmatrix.module.survey.model.data.SearchSelectData;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SearchMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		// internal ------------------------------------------------------------------

		[Inject]
		public var view:SearchView;

		[Inject]
		public var model:SurveyModel;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			view.create();
			initMouseHandler();

			view.group_combo.addItem({label: "ทั้งหมด", data: ""});
			for each (var gd:GroupData in coreModel.searchData.group)
				view.group_combo.addItem({label: gd.groupname, data: gd.groupid});

			view.surface_combo.addItem({label: "ทั้งหมด", data: ""});
			for each (var sd:SurfaceData in coreModel.searchData.surface)
				view.surface_combo.addItem({label: sd.surfacename, data: sd.surfaceid});

			view.highway_combo.addItem({label: "ทั้งหมด", data: ""});
			for each (var hd:HighwayData in coreModel.searchData.highway)
				view.highway_combo.addItem({label: hd.highwayname, data: hd.highwayid});

			view.category_combo.addItem({label: "ทั้งหมด", data: ""});
			for each (var cd:CategoryData in coreModel.searchData.category)
				view.category_combo.addItem({label: cd.categoryname, data: cd.categoryid});

			view.bpiscore_combo.addItem({label: "ทั้งหมด", data: ""});
			for each (var st:String in coreModel.searchData.bpiscore)
				view.bpiscore_combo.addItem({label: st, data: st});

			view.group_combo.selectedIndex = 0;
			view.surface_combo.selectedIndex = 0;
			view.highway_combo.selectedIndex = 0;
			view.category_combo.selectedIndex = 0;
			view.bpiscore_combo.selectedIndex = 0;
		}

		private function initMouseHandler():void
		{
			eventMap.mapListener(view, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			var targetName:String = event.target.name;
			var searchSelectData:SearchSelectData;
			switch (targetName)
			{
				case "close_btn":
					view.dispose();
					break;
				case "dialog_search_btn":
					searchSelectData = new SearchSelectData();
					searchSelectData.groupID = (view.group_combo.selectedItem) ? view.group_combo.selectedItem.data : "";
					searchSelectData.surfaceID = (view.surface_combo.selectedItem) ? view.surface_combo.selectedItem.data : "";
					searchSelectData.highwayID = (view.highway_combo.selectedItem) ? view.highway_combo.selectedItem.data : "";
					searchSelectData.categoryID = (view.category_combo.selectedItem) ? view.category_combo.selectedItem.data : "";
					searchSelectData.damagelevel = (view.bpiscore_combo.selectedItem) ? view.bpiscore_combo.selectedItem.data : "";
					searchSelectData.name = view.name_tf.text;
					searchSelectData.addYear = int(view.build_tf.text);
					searchSelectData.totalYear = int(view.year_tf.text);
					searchSelectData.bpiStart = Number(view.bpi_start_tf.text);
					searchSelectData.bpiEnd = Number(view.bpi_end_tf.text);
					searchSelectData.widthStart = Number(view.width_start_tf.text);
					searchSelectData.widthEnd = Number(view.width_end_tf.text);


					model.searchSelectData = searchSelectData;
					view.dispose();
					break;
				case "dialog_reset_btn":
					/*searchSelectData = new SearchSelectData();
					searchSelectData.groupID = "";
					searchSelectData.surfaceID = "";
					searchSelectData.highwayID = "";
					searchSelectData.categoryID = "";
					searchSelectData.damagelevel = "";
					searchSelectData.name = "";
					searchSelectData.addYear = 0;
					searchSelectData.totalYear = 0;
					searchSelectData.bpiStart = 0;
					searchSelectData.bpiEnd = 0;
					searchSelectData.widthStart = 0;
					searchSelectData.widthEnd = 0;


					model.searchSelectData = searchSelectData;
					view.dispose();*/

					view.group_combo.selectedIndex = 0;
					view.surface_combo.selectedIndex = 0;
					view.highway_combo.selectedIndex = 0;
					view.category_combo.selectedIndex = 0;
					view.bpiscore_combo.selectedIndex = 0;
					view.name_tf.text = "";
					view.build_tf.text = "";
					view.year_tf.text = "";
					view.bpi_start_tf.text = "";
					view.bpi_end_tf.text = "";
					view.width_start_tf.text = "";
					view.width_end_tf.text = "";
					break;
				default:
			}

		}


		override public function onRemove():void
		{
			eventMap.unmapListener(view, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			view = null;
		}

	}
}
