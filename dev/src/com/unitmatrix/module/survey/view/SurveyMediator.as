package com.unitmatrix.module.survey.view
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.component.UploadDialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.core.signals.ReceiveSaveDataSignal;
	import com.unitmatrix.core.signals.ReceiveSaveImageSignal;
	import com.unitmatrix.module.survey.model.SurveyModel;
	import com.unitmatrix.module.survey.model.data.SurveyData;
	import com.unitmatrix.module.survey.remote.services.SurveyService;
	import com.unitmatrix.module.survey.signals.ReceivedSurveyDataSignal;
	import com.unitmatrix.module.survey.signals.RequestSurveyDataSignal;
	import com.unitmatrix.module.survey.view.component.SearchView;
	import com.unitmatrix.module.survey.view.component.SurveyItem;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;
	import com.sleepydesign.utils.VectorUtil;

	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.ByteArray;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SurveyMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var receiveSaveDataSignal:ReceiveSaveDataSignal;

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var model:SurveyModel;

		[Inject]
		public var module:SurveyModule;

		[Inject]
		public var requestSurveyDataSignal:RequestSurveyDataSignal;

		[Inject]
		public var receiveSurveyDataSignal:ReceivedSurveyDataSignal;

		[Inject]
		public var receiveSaveImageSignal:ReceiveSaveImageSignal;

		// create ------------------------------------------------------------------

		private var current_page:int;
		private var total_page:int;
		private var row_per_page:int = 10;
		private var _dialogComponent:DialogComponent;
		private var _searchView:SearchView;
		private var _uploadDialogComponent:UploadDialogComponent;
		private var _images:Vector.<ByteArray>;
		private var _image_counter:uint;
		private var _routine_counter:int;
		private var _routine_detail:RoutineDetail;
		private var _treatment_counter:int;
		private var _is_treatment:Boolean;
		private var _memo_counter:uint;
		private var _printciple_detail:PrintcipleDetail;
		private var _printciple_counter:uint;
		private var _isExit:Boolean = false;


		override public function onRegister():void
		{
			module.create();
			module.selectedSingal.add(onSelected);
			initMouseHandler();
			requestData();
		}

		private function requestData():void
		{
			receiveSurveyDataSignal.addOnce(onReceiveSurveyData);
			requestSurveyDataSignal.dispatch();
		}

		private function onReceiveSurveyData(result:String):void
		{
			if (result == SurveyService.LOAD_SUEVEY_COMPLETE)
			{
				total_page = model.surveys.length / row_per_page;

				if (model.surveys.length % row_per_page > 0)
					total_page++;

				setPage(coreModel.active_page);
			}
		}

		private function setPage(page:int):void
		{
			current_page = page;
			var datas:Vector.<SurveyData> = model.surveys.slice(current_page * row_per_page, (current_page * row_per_page) + row_per_page);
			module.createSurveyListWithData(datas);
			module.setPagingText(current_page, total_page);
			model.searchSelectData = null;
		}

		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_DOWN, onMouseUp);
		}

		private function onMouseUp(event:Event):void
		{
			var item:SurveyItem;

			switch (event.target.name)
			{
				case "next_btn":
					nextPage();
					break;
				case "privious_btn":
					priviousPage();
					break;
				case "search_btn":
					_searchView = new SearchView();
					_searchView.completeSignal.addOnce(onSearch);
					module.addChild(_searchView);
					break;
				case "log_out_btn":

					if (coreModel.itemToSave)
					{
						var dialogSignal:Signal = new Signal(String);
						dialogSignal.addOnce(function(value:String):void
						{
							_dialogComponent.dispose();
							_dialogComponent = null;

							if (value == DialogComponent.DIALOGCOMPONENT_CLICK_YES)
							{
								_isExit = true;
								saving();

							}
							else
								logOut();
						});

						if (coreModel.isOnline)
							_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ต้องการบันทึกข้อมูลนี้ใช่รึไม่</font>", DialogComponent.YES_NO, dialogSignal);
						else
							_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ปัจจุบันระบบไม่ได้ติดต่อ internet\nต้องการบันทึกข้อมูลในระบบ local นี้ใช่รึไม่</font>", DialogComponent.YES_NO, dialogSignal);

						lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);

					}
					else
					{
						logOut();
					}

					break;
			}
		}

		private function logOut():void
		{
			coreModel.dataLocal = false;
			coreModel.deleteData(["isLogin"]);
			appModel.viewManager.viewID = Config.M00_INIT_MODULE;
		}

		private function onSearch():void
		{
			if (model.searchSelectData)
			{
				coreModel.active_page = current_page = 0;
				requestData();
			}
		}

		private function onSelected(id:String, type:String):void
		{
			var item:SurveyItem;
			switch (type)
			{
				case SurveyItem.SURVEY:

					item = module.getSurveyItemsByID(id);
					coreModel.select_survey_id = item.id;
					coreModel.currentSurveyData = VectorUtil.getItemByID(model.surveys, id) as SurveyData;
					coreModel.currentBridgeData = VectorUtil.getItemByID(coreModel.bridgeDatas, id) as BridgeData;
					appModel.viewManager.viewID = Config.M50_SURVEY_INFO_MODULE;
					break;

				case SurveyItem.BRIDGE:

					item = module.getSurveyItemsByID(id);
					coreModel.select_survey_id = item.id;
					appModel.viewManager.viewID = Config.M60_ROOM_DETAIL_MODULE;
					break;

				case SurveyItem.SAVE:

					item = module.getSurveyItemsByID(id);
					coreModel.select_survey_id = item.id;
					coreModel.currentBridgeData = VectorUtil.getItemByID(coreModel.bridgeDatas, id) as BridgeData;
					var dialogSignal:Signal = new Signal(String);
					dialogSignal.addOnce(onDialogClick);

					if (coreModel.isOnline)
						_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ต้องการบันทึกข้อมูลนี้ใช่รึไม่</font>", DialogComponent.YES_NO, dialogSignal);
					else
						_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ปัจจุบันระบบไม่ได้ติดต่อ internet\nต้องการบันทึกข้อมูลในระบบ local นี้ใช่รึไม่</font>", DialogComponent.YES_NO, dialogSignal);

					lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
					break;
			}

		}

		private function onDialogClick(action:String):void
		{

			_dialogComponent.dispose();
			_dialogComponent = null;

			if (action == DialogComponent.DIALOGCOMPONENT_CLICK_NO)
				return;

			saving();

		}

		private function saving():void
		{
			if (coreModel.isOnline)
			{
				receiveSaveDataSignal.addOnce(onSaveToServer);
				coreModel.saveToServer();

				_uploadDialogComponent = new UploadDialogComponent("แจ้งเตือน", "<font color='#000000'>กำลังบันทึกข้อมูล\nกรุณารอสักครู่</font>");
				lightBoxSignal.dispatch(_uploadDialogComponent, LightBoxModule.ALERT_LAYER);
			}
			else
			{
				coreModel.saveLocal();
				if (_isExit)
				{
					logOut();
					_isExit = false;
				}
			}


		}

		private function saveComplete():void
		{
			var dialogSignal:Signal = new Signal(String);
			dialogSignal.addOnce(function():void
			{
				_dialogComponent.dispose();

				var sd:SurveyData = VectorUtil.getItemByID(model.surveys, coreModel.select_survey_id) as SurveyData
				sd.want_to_save = false;
				var bd:BridgeData = VectorUtil.getItemByID(coreModel.bridgeDatas, coreModel.select_survey_id) as BridgeData
				bd.isSave = false;
				var item:SurveyItem = module.getSurveyItemsByID(coreModel.select_survey_id);
				item.isSave = false;

				if (_isExit)
				{
					logOut();
					_isExit = false;
				}
			});
			_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#000000'>ข้อมูลบันทึกเรียบร้อย</font>", DialogComponent.CLOSE, dialogSignal);

			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
		}

		private function onSaveToServer():void
		{
			_uploadDialogComponent.dispose();
			coreModel.saveLocal();

			_routine_counter = 0;
			saveRoutineImage();
			saveComplete();
		}

		private function saveRoutineImage():void
		{
			if (_routine_counter < coreModel.currentBridgeData.routine_detail.length)
			{
				_routine_detail = coreModel.currentBridgeData.routine_detail[_routine_counter];
				_treatment_counter = 0;
				saveTreatmentImage();
				_routine_counter++;
			}
			else
			{
				_routine_detail = null;
				savePrintcipleImage();
			}
		}

		private function savePrintcipleImage():void
		{
			if (_printciple_counter < coreModel.currentBridgeData.printciple_detail.length)
			{
				_printciple_detail = coreModel.currentBridgeData.printciple_detail[_printciple_counter];

				if (_printciple_detail)
				{
					_treatment_counter = 0;
					saveTreatmentImage();
					_printciple_counter++;
				}
			}
			else
			{
				saveComplete();
			}
		}

		private function saveTreatmentImage():void
		{
			if (_treatment_counter < _routine_detail.treatment_detail.length)
			{
				_is_treatment = true;

				var album_id:String;

				if (_routine_detail)
					album_id = _routine_detail.treatment_detail[_treatment_counter].path;
				else
					album_id = _printciple_detail.treatment_detail[_treatment_counter].path;

				_images = CoreModel.getImageByteAtID(album_id);

				_image_counter = 0;
				if (_images.length > 0)
					saveImage();

				//count ------ 
				_treatment_counter++;
			}
			else
			{
				_memo_counter = 0;
				saveMemoImage();
			}
		}

		private function saveMemoImage():void
		{
			if (_memo_counter < _routine_detail.memo_detail.length)
			{
				_is_treatment = false;
				var album_id:String;

				if (_routine_detail)
					album_id = _routine_detail.memo_detail[_treatment_counter].path;
				else
					album_id = _printciple_detail.memo_detail[_treatment_counter].path;

				_images = CoreModel.getImageByteAtID(album_id);

				_image_counter = 0;
				if (_images.length > 0)
					saveImage();

				//count ------ 
				_memo_counter++;
			}
			else
			{
				saveRoutineImage();
			}
		}

		private function saveImage():void
		{
			receiveSaveImageSignal.addOnce(onSaveComplete)
			coreModel.saveImage(_images[_image_counter]);
			_image_counter++;
		}

		private function onSaveComplete():void
		{
			if (_image_counter < _images.length)
			{
				saveImage();
			}
			else
			{
				if (_is_treatment)
					saveTreatmentImage();
				else
					saveMemoImage();
			}
		}

		private function priviousPage():void
		{
			if (current_page - 1 >= 0)
				setPage(current_page - 1);

			coreModel.active_page = current_page;
		}

		private function nextPage():void
		{
			if (current_page + 1 < total_page)
				setPage(current_page + 1);

			coreModel.active_page = current_page;
		}

		// event ------------------------------------------------------------------

		// destroy ------------------------------------------------------------------

		override public function onRemove():void
		{
			module.selectedSingal.remove(onSelected);
			eventMap.unmapListener(module, MouseEvent.MOUSE_DOWN, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
