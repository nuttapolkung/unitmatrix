package com.unitmatrix.module.survey.model.data
{

	public class SurveyData extends Object
	{
		public var id:String;
		public var rank:int;
		public var group:String;
		public var groupID:String;
		public var highway_number:String;
		public var bridge_name:String;
		public var localtion:String;
		public var direction:String;
		public var highwayID:String;
		public var surfaceID:String;
		public var categoryID:String;
		public var damagelevel:String;
		public var addYear:String;
		public var bpi:Number;
		public var width:Number;

		public var want_to_save:Boolean = false;

	}
}
