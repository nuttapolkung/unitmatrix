package com.unitmatrix.module.survey.model.data
{


	public class SearchSelectData extends Object
	{
		public var groupID:String;
		public var surfaceID:String;
		public var highwayID:String;
		public var categoryID:String;
		public var damagelevel:String;
		public var name:String;
		public var addYear:int;
		public var totalYear:int;
		public var bpiStart:Number;
		public var bpiEnd:Number;
		public var widthStart:Number;
		public var widthEnd:Number;
	}
}
