package com.unitmatrix.module.survey.model
{
	import com.unitmatrix.module.survey.model.data.SearchSelectData;
	import com.unitmatrix.module.survey.model.data.SurveyData;

	public class SurveyModel
	{
		public var surveys:Vector.<SurveyData>;
		public var searchSelectData:SearchSelectData;

		public function SurveyModel()
		{
		}
	}
}
