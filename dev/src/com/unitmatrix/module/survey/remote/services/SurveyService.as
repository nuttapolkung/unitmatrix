package com.unitmatrix.module.survey.remote.services
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.module.survey.model.SurveyModel;
	import com.unitmatrix.module.survey.model.data.SurveyData;
	import com.unitmatrix.module.survey.signals.ReceivedSurveyDataSignal;
	import com.sleepydesign.utils.VectorUtil;

	import flash.events.IOErrorEvent;

	public class SurveyService
	{

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var model:SurveyModel;

		[Inject]
		public var receiveSurveyDataSignal:ReceivedSurveyDataSignal;

		public static const LOAD_SUEVEY_COMPLETE:String = "LOAD_SUEVEY_COMPLETE";
		public static const LOAD_SUEVEY_FAILED:String = "LOAD_SUEVEY_FAILED";

		public function loadData():void
		{
			var count:int = 1;
			model.surveys = new Vector.<SurveyData>;
			for each (var b_data:BridgeData in coreModel.bridgeDatas)
			{
				var bd:BridgeDetailData = VectorUtil.getItemByID(coreModel.bridgeDetailDatas, b_data.id);
				var surveyData:SurveyData = new SurveyData();
				surveyData.id = b_data.id;
				surveyData.rank = count;
				surveyData.group = b_data.group_name;
				surveyData.highway_number = b_data.highway_name;
				surveyData.bridge_name = b_data.bridge_name;
				surveyData.localtion = b_data.position;
				surveyData.direction = b_data.direction_name || "-";

				// for search
				surveyData.groupID = b_data.group_id;
				surveyData.surfaceID = bd.surfaceid;
				surveyData.categoryID = bd.categoryid;
				surveyData.damagelevel = bd.damagelevel;
				surveyData.highwayID = b_data.highway_id;
				surveyData.addYear = bd.createdate;
				surveyData.bpi = int(bd.bpiscore);
				surveyData.width = int(bd.width);
				surveyData.want_to_save = b_data.isSave;

				if (b_data.isSave)
				{
					coreModel.currentBridgeData = b_data;
					coreModel.currentSurveyData = surveyData;
					coreModel.itemToSave = true;
				}

				model.surveys.push(surveyData);

				count++;
			}

			if (model.searchSelectData)
			{
				if (model.searchSelectData.groupID != null && model.searchSelectData.groupID != "")
					model.surveys = byGroup(model.surveys);
				if (model.searchSelectData.surfaceID != null && model.searchSelectData.surfaceID != "")
					model.surveys = bySurface(model.surveys);
				if (model.searchSelectData.highwayID != null && model.searchSelectData.highwayID != "")
					model.surveys = byHighway(model.surveys);
				if (model.searchSelectData.categoryID != null && model.searchSelectData.categoryID != "")
					model.surveys = byCategory(model.surveys);
				if (model.searchSelectData.damagelevel != null && model.searchSelectData.damagelevel != "")
					model.surveys = byDamagelevel(model.surveys);
				if (model.searchSelectData.name != null && model.searchSelectData.name != "")
					model.surveys = byName(model.surveys);
				if (model.searchSelectData.addYear != 0)
					model.surveys = byAddYear(model.surveys);
				if (model.searchSelectData.totalYear != 0)
					model.surveys = byTotalYear(model.surveys);
				if (model.searchSelectData.bpiStart != 0 && model.searchSelectData.bpiEnd != 0)
					model.surveys = byBPI(model.surveys);
				if (model.searchSelectData.widthStart != 0 && model.searchSelectData.widthEnd != 0)
					model.surveys = byWidth(model.surveys);
			}

			receiveSurveyDataSignal.dispatch(SurveyService.LOAD_SUEVEY_COMPLETE);
		}

		protected function onLoadError(event:IOErrorEvent):void
		{
			trace("! load Data error. " + event.toString());
		}

		protected function byGroup(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.groupID == model.searchSelectData.groupID)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function bySurface(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.surfaceID == model.searchSelectData.surfaceID)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byHighway(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.highwayID == model.searchSelectData.highwayID)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byCategory(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.categoryID == model.searchSelectData.categoryID)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byDamagelevel(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.damagelevel == model.searchSelectData.damagelevel)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byName(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.bridge_name.indexOf(model.searchSelectData.name) != -1)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byTotalYear(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{
			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{
				var current_year:int = new Date().getFullYear() + 543;
				var year:int = current_year - int(data.addYear);

				if (year == model.searchSelectData.totalYear)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byAddYear(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{

			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (int(data.addYear) == model.searchSelectData.addYear)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byBPI(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{

			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.bpi >= model.searchSelectData.bpiStart && data.bpi <= model.searchSelectData.bpiEnd)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}

		protected function byWidth(surveys:Vector.<SurveyData>):Vector.<SurveyData>
		{

			var new_data:Vector.<SurveyData> = new Vector.<SurveyData>;
			var count:int = 1;
			for each (var data:SurveyData in model.surveys)
			{

				if (data.width >= model.searchSelectData.widthStart && data.width <= model.searchSelectData.widthEnd)
				{
					data.rank = count;
					new_data.push(data);
					count++;
				}
			}

			return new_data;
		}
	}
}
