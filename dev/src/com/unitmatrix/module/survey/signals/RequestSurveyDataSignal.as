package com.unitmatrix.module.survey.signals
{
	import org.osflash.signals.Signal;
	
	public class RequestSurveyDataSignal extends Signal
	{
		public function RequestSurveyDataSignal()
		{
			super();
		}
	}
}