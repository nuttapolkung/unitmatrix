package com.unitmatrix.module.survey.signals
{
	import org.osflash.signals.Signal;
	
	public class ReceivedSurveyDataSignal extends Signal
	{
		public function ReceivedSurveyDataSignal()
		{
			super(String);
		}
	}
}