package com.unitmatrix.module.surveyinfo.model.data
{

	public class AlbumData
	{
		public var id:String;
		public var title:String;
		public var images:Vector.<ImageData>;

		public function AlbumData()
		{
		}
	}
}
