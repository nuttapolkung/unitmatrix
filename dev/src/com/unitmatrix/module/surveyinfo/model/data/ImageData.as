package com.unitmatrix.module.surveyinfo.model.data
{
	import flash.filesystem.File;

	public class ImageData
	{
		public var image_path:String;
		public var title:String;
		public var id:String;
		public var album_id:String;
		public var f:File;

		public function ImageData()
		{
		}
	}
}
