package com.unitmatrix.module.surveyinfo.model.data
{

	public class DamageData
	{
		public var rank:int;
		public var detail:String;
		public var quantity:int;
		public var unit:String;

		public function DamageData(data:Object)
		{
			this.rank=data.rank;
			this.detail=data.detail;
			this.quantity=data.quantity;
			this.unit=data.unit;
		}
	}
}
