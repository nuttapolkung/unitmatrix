package com.unitmatrix.module.surveyinfo
{
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.unitmatrix.module.surveyinfo.remote.services.SurveyInfoService;
	import com.unitmatrix.module.surveyinfo.view.SurveyInfoMediator;
	import com.unitmatrix.module.surveyinfo.view.SurveyInfoModule;
	import com.unitmatrix.module.surveyinfo.view.components.GalleryMediator;
	import com.unitmatrix.module.surveyinfo.view.components.GalleryView;
	import com.unitmatrix.module.surveyinfo.view.components.PhotoAlbumMediator;
	import com.unitmatrix.module.surveyinfo.view.components.PhotoAlbumView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddBasicMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddBasicView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddNormalMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddNormalView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddSpecialMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddSpecialView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditBasicMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditBasicView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditNormalMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditNormalView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditSpecialMediator;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditSpecialView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.DisplayObjectContainer;
	import flash.utils.getDefinitionByName;

	import org.robotlegs.core.IInjector;
	import org.robotlegs.utilities.modular.mvcs.ModuleContext;

	public class SurveyInfoContext extends ModuleContext
	{
		public function SurveyInfoContext(contextView:DisplayObjectContainer, injector:IInjector)
		{
			super(contextView, true, injector);
		}

		override public function startup():void
		{
			injector.mapSingleton(SurveyInfoService);
			injector.mapSingleton(SurveyInfoModel);

			mediatorMap.mapView(SurveyInfoModule, SurveyInfoMediator);

			mediatorMap.mapView(GalleryView, GalleryMediator);
			mediatorMap.mapView(PhotoAlbumView, PhotoAlbumMediator);

			mediatorMap.mapView(SurveyInfoAddBasicView, SurveyInfoAddBasicMediator);
			mediatorMap.mapView(SurveyInfoAddNormalView, SurveyInfoAddNormalMediator);
			mediatorMap.mapView(SurveyInfoAddSpecialView, SurveyInfoAddSpecialMediator);

			mediatorMap.mapView(SurveyInfoEditNormalView, SurveyInfoEditNormalMediator);
			mediatorMap.mapView(SurveyInfoEditBasicView, SurveyInfoEditBasicMediator);
			mediatorMap.mapView(SurveyInfoEditSpecialView, SurveyInfoEditSpecialMediator);

			super.startup();
		}

		override public function dispose():void
		{
			mediatorMap.unmapView(getDefinitionByName(ModuleBase(contextView).ID) as Class);
			mediatorMap.removeMediatorByView(contextView);

			mediatorMap.unmapView(SurveyInfoAddBasicView);
			mediatorMap.removeMediatorByView(SurveyInfoAddBasicView);

			mediatorMap.unmapView(SurveyInfoAddSpecialView);
			mediatorMap.removeMediatorByView(SurveyInfoAddSpecialView);

			mediatorMap.unmapView(SurveyInfoAddNormalView);
			mediatorMap.removeMediatorByView(SurveyInfoAddNormalView);

			mediatorMap.unmapView(SurveyInfoEditNormalView);
			mediatorMap.removeMediatorByView(SurveyInfoEditNormalView);

			mediatorMap.unmapView(SurveyInfoEditBasicView);
			mediatorMap.removeMediatorByView(SurveyInfoEditBasicView);

			mediatorMap.unmapView(SurveyInfoEditSpecialView);
			mediatorMap.removeMediatorByView(SurveyInfoEditSpecialView);

			mediatorMap.unmapView(GalleryView);
			mediatorMap.removeMediatorByView(GalleryView);

			mediatorMap.unmapView(PhotoAlbumView);
			mediatorMap.removeMediatorByView(PhotoAlbumView);

			super.dispose();
		}
	}
}
