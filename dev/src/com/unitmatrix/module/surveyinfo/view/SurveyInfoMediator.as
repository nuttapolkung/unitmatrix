package com.unitmatrix.module.surveyinfo.view
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.module.surveyinfo.view.components.GalleryView;
	import com.unitmatrix.module.surveyinfo.view.components.PhotoAlbumView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddBasicView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddNormalView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoAddSpecialView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditBasicView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditNormalView;
	import com.unitmatrix.module.surveyinfo.view.components.SurveyInfoEditSpecialView;
	import com.unitmatrix.module.surveyinfo.view.components.item.AddDamageRowItem;
	import com.unitmatrix.module.surveyinfo.view.components.item.DamageRowItem;
	import com.unitmatrix.module.surveyinfo.view.components.item.DamageRowItem2;
	import com.unitmatrix.module.surveyinfo.view.components.item.MemoEditItem;
	import com.unitmatrix.module.surveyinfo.view.components.item.PrintcipleSkin;
	import com.unitmatrix.module.surveyinfo.view.components.item.RoutineSkin;
	import com.unitmatrix.module.surveyinfo.view.components.item.SpecialSkin;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SurveyInfoMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:SurveyInfoModule;

		// create ------------------------------------------------------------------
		private var _dialogComponent:DialogComponent;
		private var _bridgeData:BridgeData;
		private var _bridgeDetailData:BridgeDetailData;

		override public function onRegister():void
		{
			_bridgeData = coreModel.getBridgeDataByID(coreModel.select_survey_id);
			_bridgeDetailData = coreModel.getBridgeDetailDatasByID(_bridgeData.id);

			module.bridgeData = _bridgeData;
			module.bridgeDetailData = _bridgeDetailData;
			module.treatmentDatas = coreModel.treatmentDatas;

			module.create();

			initMouseHandle();

			module.addBasicDamageDetail(_bridgeData.printciple_detail);
			module.addNormalDamageDetail(_bridgeData.routine_detail);
			module.addSpecialDamageDetail(_bridgeData.printciple_detail);
		}

		private function initMouseHandle():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			switch (event.target.name)
			{
				case "normal_survey_btn":
					module.activeNormalSurvey();
					break;
				case "special_survey_btn":
					module.activeSpecialSurvey();
					break;
				case "album_btn":
				case "founded_damage_detail_btn":
					coreModel.currentPrintciple = module.specialSkin.currentPrintciple;
					module.addChild(new PhotoAlbumView());
					break;
				case "basic_survey_btn":
					module.activeBasicSurvey();
					break;
				case "increase_btn":
					if (event.target.parent.parent is RoutineSkin)
					{
						var surveyInfoAddNormalView:SurveyInfoAddNormalView = new SurveyInfoAddNormalView();
						surveyInfoAddNormalView.updateRoutineSignal.addOnce(function():void
						{
							module.upDateNormalDamageDetail(_bridgeData.routine_detail);
						});

						module.addChild(surveyInfoAddNormalView);
					}
					else if (event.target.parent.parent is PrintcipleSkin)
					{
						var surveyInfoAddBasicView:SurveyInfoAddBasicView = new SurveyInfoAddBasicView();
						surveyInfoAddBasicView.updateRoutineSignal.addOnce(function():void
						{
							module.upDateBasicDamageDetail(_bridgeData.printciple_detail);
							module.upDateSpecialDamageDetail(_bridgeData.printciple_detail);
							module.activeBasicSurvey();
						});

						module.addChild(surveyInfoAddBasicView);
					}
					else if (event.target.parent.parent is SpecialSkin)
					{
						var surveyInfoAddSpecialView:SurveyInfoAddSpecialView = new SurveyInfoAddSpecialView();
						surveyInfoAddSpecialView.updateRoutineSignal.addOnce(function():void
						{
							module.upDateSpecialDamageDetail(_bridgeData.printciple_detail);
						});
						module.addChild(surveyInfoAddSpecialView);
					}
					break;
				case "edit_btn":
					if (event.target.parent.parent is RoutineSkin)
					{
						var routineSkin:RoutineSkin = event.target.parent.parent as RoutineSkin;

						if (!routineSkin.currentRoutineDetail)
							return;

						coreModel.currentRoutineDetail = routineSkin.currentRoutineDetail;

						var surveyInfoEditNormalView:SurveyInfoEditNormalView = new SurveyInfoEditNormalView();
						surveyInfoEditNormalView.completeSingal.addOnce(function():void
						{
							module.upDateNormalDamageDetail(_bridgeData.routine_detail);
						});
						module.addChild(surveyInfoEditNormalView);
					}
					else if (event.target.parent.parent is PrintcipleSkin)
					{

						var printcipleSkin:PrintcipleSkin = event.target.parent.parent as PrintcipleSkin;

						if (!printcipleSkin.currentPrintciple)
							return;

						coreModel.currentPrintciple = printcipleSkin.currentPrintciple;

						var surveyInfoEditBasicView:SurveyInfoEditBasicView = new SurveyInfoEditBasicView();
						surveyInfoEditBasicView.completeSingal.addOnce(function():void
						{
							module.upDateBasicDamageDetail(_bridgeData.printciple_detail);
							module.upDateSpecialDamageDetail(_bridgeData.printciple_detail);
							module.activeBasicSurvey();
						});
						module.addChild(surveyInfoEditBasicView);
					}
					else if (event.target.parent.parent is SpecialSkin)
					{
						var specialSkin:SpecialSkin = event.target.parent.parent as SpecialSkin;

						if (!specialSkin.currentPrintciple)
							return;

						coreModel.currentPrintciple = specialSkin.currentPrintciple;

						var surveyInfoEditSpecialView:SurveyInfoEditSpecialView = new SurveyInfoEditSpecialView();
						surveyInfoEditSpecialView.completeSingal.addOnce(function():void
						{
							module.upDateSpecialDamageDetail(_bridgeData.printciple_detail);
						});
						module.addChild(surveyInfoEditSpecialView);
					}
					break;
				case "decrease_btn":
					var searchIndex:int;
					var dialogSignal:Signal = new Signal(String);
					dialogSignal.addOnce(function(value:String):void
					{

						_dialogComponent.dispose();
						_dialogComponent = null;

						if (value != DialogComponent.DIALOGCOMPONENT_CLICK_YES)
							return;

						if (event.target.parent.parent is RoutineSkin)
						{
							var currentRoutineDetail:RoutineDetail = module.currentRoutineSkinRoutineDetail;

							searchIndex = _bridgeData.routine_detail.indexOf(currentRoutineDetail, 0);
							if (searchIndex != -1)
								_bridgeData.routine_detail.splice(searchIndex, 1);

							module.upDateNormalDamageDetail(_bridgeData.routine_detail);


						}
						else if (event.target.parent.parent is PrintcipleSkin)
						{
							var currentPrintcipleDetail:PrintcipleDetail = module.currentPrintcipleSkinPrintcipleDetail;

							searchIndex = _bridgeData.printciple_detail.indexOf(currentPrintcipleDetail, 0);
							if (searchIndex != -1)
								_bridgeData.printciple_detail.splice(searchIndex, 1);

							module.upDateBasicDamageDetail(_bridgeData.printciple_detail);
							module.upDateSpecialDamageDetail(_bridgeData.printciple_detail);
							module.activeBasicSurvey();
						}

						//set to save
						coreModel.itemToSave = true;
					});
					_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#ff0000'>ต้องการลบข้อมูลนี้ใช่รึไม่</font>", DialogComponent.YES_NO, dialogSignal);

					lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
					break;
				case "back_btn":
					appModel.viewManager.viewID = Config.M40_SURVEY_MODULE;
					break;

			}

			var printcipleDetail:PrintcipleDetail;
			var id:String;

			if (event.target.name.indexOf("camera") != -1)
			{
				var galleryView:GalleryView = new GalleryView();

				var item:DamageRowItem = event.target.parent.parent as DamageRowItem;
				if (item)
					galleryView.gallery_ID = item.path as String;

				var item2:DamageRowItem2 = event.target.parent.parent as DamageRowItem2;
				if (item2)
					galleryView.gallery_ID = item2.id as String;

				var add_item:AddDamageRowItem = event.target.parent.parent as AddDamageRowItem;
				if (add_item)
					galleryView.gallery_ID = add_item.path as String;

				var memo_item:MemoEditItem = event.target.parent.parent as MemoEditItem;
				if (memo_item)
					galleryView.gallery_ID = memo_item.id as String;


				module.addChild(galleryView);
				return;
			}
			else if (event.target.name.indexOf("RoutineSkinTab_") != -1)
			{
				id = String(event.target.name).split("_")[1];
				for each (var routineDetail:RoutineDetail in _bridgeData.routine_detail)
				{
					if (id == routineDetail.id)
					{
						module.activeRoutineSkin(routineDetail);

						break;
					}
				}
			}
			else if (event.target.name.indexOf("PrintcipleSkin_") != -1)
			{
				id = String(event.target.name).split("_")[1];
				for each (printcipleDetail in _bridgeData.printciple_detail)
				{
					if (id == printcipleDetail.id)
					{
						module.activePrintcipleSkin(printcipleDetail);
						break;
					}
				}
			}
			else if (event.target.name.indexOf("SpecialSkin_") != -1)
			{
				id = String(event.target.name).split("_")[1];
				for each (printcipleDetail in _bridgeData.printciple_detail)
				{
					if (id == printcipleDetail.id)
					{
						module.activeSpecialSkin(printcipleDetail);
						break;
					}
				}
			}
		}

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel.viewManager.removeViewByID(module.ID);
			appModel = null;
			module = null;
		}
	}
}
