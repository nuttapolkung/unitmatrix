package com.unitmatrix.module.surveyinfo.view
{
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.BridgeDetailData;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.module.surveyinfo.SurveyInfoContext;
	import com.unitmatrix.module.surveyinfo.view.components.item.HeaderInfo;
	import com.unitmatrix.module.surveyinfo.view.components.item.PrintcipleSkin;
	import com.unitmatrix.module.surveyinfo.view.components.item.RoutineSkin;
	import com.unitmatrix.module.surveyinfo.view.components.item.SpecialSkin;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.robotlegs.apps.view.IView;
	import com.sleepydesign.robotlegs.modules.ModuleBase;

	import flash.display.Sprite;

	import org.robotlegs.core.IInjector;

	public class SurveyInfoModule extends ModuleBase
	{

		public static const SURVEY_MODE_NORMAL:String = "normal";
		public static const SURVEY_MODE_BASIC:String = "basic";
		public static const SURVEY_MODE_SPECIAL:String = "special";


		private var contentScroll:ScrollController;
		private var contentScrollNormal:ScrollController;

		private var detail_sp:Sprite;
		public var treatmentDatas:Vector.<TreatmentData>;

		private var printcipleSkin:PrintcipleSkin;
		private var routineSkin:RoutineSkin;
		public var specialSkin:SpecialSkin;

		private var _header:HeaderInfo;
		public var bridgeData:BridgeData;
		public var bridgeDetailData:BridgeDetailData;
		private var _container:Sprite;

		[Inject]
		public function set parentInjector(value:IInjector):void
		{
			_context = new SurveyInfoContext(this, value);
		}

		// -----------------------------------------------------------------------------------------------------------------------

		override public function create():void
		{

			_container = new Sprite;
			addChild(_container);

			_header = new HeaderInfo(bridgeDetailData.bridgename, bridgeDetailData.groupname, bridgeDetailData.highwayname, bridgeDetailData.position, bridgeDetailData.width, bridgeDetailData.directionname, bridgeDetailData.typename, bridgeDetailData.typeabbrev, bridgeDetailData.damagelevel, bridgeDetailData.bpiscore, bridgeDetailData.createdate, bridgeDetailData.span);
			_container.addChild(_header);

			routineSkin = new RoutineSkin();
			routineSkin.y = _header.height + 10;
			_container.addChild(routineSkin);

			printcipleSkin = new PrintcipleSkin();
			printcipleSkin.y = _header.height + 10;
			_container.addChild(printcipleSkin);

			specialSkin = new SpecialSkin();
			specialSkin.y = _header.height + 10;
			_container.addChild(specialSkin);

			resize();

			activeNormalSurvey();
		}

		private function resize():void
		{
			_container.scaleX = _container.scaleY = Config.ratio;

			_container.x = Config.GUI_SIZE.width * 0.5 - _container.width * 0.5;
			_container.y = Config.GUI_SIZE.height * 0.5 - _container.height * 0.5;
		}

		// basic ------------------
		public function addBasicDamageDetail(printciple_detail:Vector.<PrintcipleDetail>):void
		{
			printcipleSkin.addBasicDamageDetail(printciple_detail, treatmentDatas);
			setUseSpecialBTN((printcipleSkin.currentPrintciple.addstt == "1") ? true : false);
		}

		public function upDateBasicDamageDetail(printciple_detail:Vector.<PrintcipleDetail>):void
		{
			printcipleSkin.dispose();

			printcipleSkin = new PrintcipleSkin();
			printcipleSkin.y = _header.height + 10;
			_container.addChild(printcipleSkin);

			printcipleSkin.addBasicDamageDetail(printciple_detail, treatmentDatas);

			setUseSpecialBTN((printcipleSkin.currentPrintciple.addstt == "1") ? true : false);
		}

		public function setUseSpecialBTN(value:Boolean):void
		{
			_header.useSpecialBTN = value;
		}

		// special ------------------
		public function addSpecialDamageDetail(printciple_detail:Vector.<PrintcipleDetail>):void
		{
			specialSkin.addBasicDamageDetail(printciple_detail, treatmentDatas);
		}

		public function upDateSpecialDamageDetail(printciple_detail:Vector.<PrintcipleDetail>):void
		{
			specialSkin.dispose();

			specialSkin = new SpecialSkin();
			specialSkin.y = _header.height + 10;
			_container.addChild(specialSkin);

			specialSkin.addBasicDamageDetail(printciple_detail, treatmentDatas);
		}

		public function upDateSpecialDamageDetailData(printciple_detail:Vector.<PrintcipleDetail>):void
		{
			specialSkin.dispose();

			specialSkin = new SpecialSkin();
			specialSkin.y =_header.height + 10;
			_container.addChild(specialSkin);

			specialSkin.addBasicDamageDetail(printciple_detail, treatmentDatas);
		}

		// normal ------------------
		public function addNormalDamageDetail(routine_detail:Vector.<RoutineDetail>):void
		{
			routineSkin.addNormalDamageDetail(routine_detail, treatmentDatas);
		}

		public function upDateNormalDamageDetail(routine_detail:Vector.<RoutineDetail>):void
		{
			routineSkin.dispose();

			routineSkin = new RoutineSkin();
			routineSkin.y = _header.height + 10;
			_container.addChild(routineSkin);

			routineSkin.addNormalDamageDetail(routine_detail, treatmentDatas);
		}

		public function get currentRoutineSkinRoutineDetail():RoutineDetail
		{
			return routineSkin.currentRoutineDetail;
		}

		public function get currentPrintcipleSkinPrintcipleDetail():PrintcipleDetail
		{
			return printcipleSkin.currentPrintcipleDetail;
		}

		public function activeRoutineSkin(routineDetail:RoutineDetail):void
		{
			routineSkin.activeTapWithID(routineDetail, treatmentDatas);
		}

		public function activePrintcipleSkin(printcipleDetail:PrintcipleDetail):void
		{
			printcipleSkin.activeTapWithID(printcipleDetail, treatmentDatas);
			setUseSpecialBTN((printcipleSkin.currentPrintciple.addstt == "1") ? true : false);
		}

		public function activeSpecialSkin(printcipleDetail:PrintcipleDetail):void
		{
			specialSkin.activeTapWithID(printcipleDetail, treatmentDatas);
		}

		public function selectedCK(choice:String, index:String):void
		{
			printcipleSkin.selectedCK(choice, index);
		}

		public function setSurveyMode(mode:String):void
		{
			_header.mode = mode;
		}

		override public function activate(onActivated:Function, prevView:IView = null):void
		{

			super.activate(onActivated, prevView);
		}

		override public function deactivate(onDeactivated:Function, nextView:IView = null):void
		{
			dispose();

			super.deactivate(onDeactivated, nextView);
		}

		override public function dispose():void
		{
			destroy();
			super.dispose();
		}

		public function activeNormalSurvey():void
		{
			setSurveyMode(SURVEY_MODE_NORMAL);
			printcipleSkin.hide();
			routineSkin.show();
			specialSkin.hide()
		}

		public function activeBasicSurvey():void
		{
			setSurveyMode(SURVEY_MODE_BASIC);
			printcipleSkin.show();
			routineSkin.hide();
			specialSkin.hide();
		}

		public function activeSpecialSurvey():void
		{
			setSurveyMode(SURVEY_MODE_SPECIAL);
			printcipleSkin.hide();
			routineSkin.hide();
			specialSkin.show()
		}

		public function get header():HeaderInfo
		{
			return _header;
		}

	}
}


