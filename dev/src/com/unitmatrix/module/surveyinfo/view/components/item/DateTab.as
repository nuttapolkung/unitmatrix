package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.text.TextField;

	import surveyinfo.date_tab_sp;

	public class DateTab extends SDSprite
	{
		private var _skin:Sprite;

		public function DateTab(index:int, date:String)
		{
			_skin = new surveyinfo.date_tab_sp();
			addChild(_skin);
			tap_bg_sp.alpha = (index == 0) ? 1 : 0;
			date_tf.text = date;
			mouseChildren = false;
			buttonMode = true;
		}

		public function get date_tf():TextField
		{
			return _skin.getChildByName("date_tf") as TextField;
		}

		public function get tap_bg_sp():Sprite
		{
			return _skin.getChildByName("tap_bg_sp") as Sprite;
		}
	}
}
