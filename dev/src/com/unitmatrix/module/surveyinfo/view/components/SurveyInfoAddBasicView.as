package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.model.data.MemoDetail;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.unitmatrix.module.surveyinfo.view.components.item.AddDamageRowItem;
	import com.unitmatrix.module.surveyinfo.view.components.item.MemoEditItem;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sickworks.components.DateChooser;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.globalization.DateTimeFormatter;
	import flash.text.TextField;

	import SurveyInfo.block_image;
	import SurveyInfo.detail_basic_sp;
	import SurveyInfo.survey_basic_add_sp;

	import org.osflash.signals.Signal;

	public class SurveyInfoAddBasicView extends SDSprite
	{
		public var updateRoutineSignal:Signal = new Signal();
		private var _skin:Sprite;
		private var checkIcon_sp:Sprite;
		private var dc:DateChooser;
		private var detail_sp:Sprite;
		private var damage_founded_content:Sprite;
		private var damage_items:Vector.<AddDamageRowItem>;
		private var _total_damage_founded:int = 1;
		private var _damage_founded_content:Sprite;
		private var _ypos:int;
		private var _contentScroll:ScrollController;
		private var _dfm:DateTimeFormatter;
		private var damage_content_header:Sprite;
		private var _memoEditItems:Vector.<MemoEditItem>;


		public function dispose():void
		{
			destroy();
		}

		public function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new SurveyInfo.survey_basic_add_sp();

			addChild(_skin);

			checkIcon_sp = _skin.getChildByName("special_checked_sp") as Sprite;


			_dfm = new DateTimeFormatter("en-US");
			_dfm.setDateTimePattern("dd-MM-yyyy");
			date_tf.text = _dfm.format(new Date());

			today_date_tf.text = _dfm.format(new Date());
			next_date_tf.text = "00-00-0000";
			pass_date_tf.text = "00-00-0000";

			resize();
		}

		private function resize():void
		{

			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x = Config.GUI_SIZE.width * 0.5 - _skin.width * 0.5;
			_skin.y = Config.GUI_SIZE.height * 0.5 - _skin.height * 0.5;

		}



		public function addDetail(treatmentDatas:Vector.<TreatmentData>):void
		{
			var row_bg:Sprite;
			detail_sp = new Sprite();

			damage_founded_content = new SurveyInfo.detail_basic_sp();

			detail_sp.addChild(damage_founded_content);

			for (var j:int = 0; j < 5; j++)
			{
				selectedCK("3", j.toString());
			}


			damage_items = new Vector.<AddDamageRowItem>();
			var count:int = 0;
			for each (var td:TreatmentData in treatmentDatas)
			{
				if (td.principle_flag == "1")
				{
					var row:AddDamageRowItem = new AddDamageRowItem(td.id, null, count, td.name, td.unit);
					row.y = _ypos;

					damage_items.push(row);

					_ypos += row.height;

					count++;
				}
			}

			/*

						var count:int = 0;
						var damage_content:Sprite = new Sprite();
						damage_content_header = new SurveyInfo.detail_normal_sp();

						damage_content.addChild(damage_content_header);

						_ypos = damage_content_header.height;

						damage_items = new Vector.<AddDamageRowItem>();

						for each (var td:TreatmentData in treatmentDatas)
						{
							if (td.principle_flag == "1")
							{
								var row:AddDamageRowItem = new AddDamageRowItem(td.id, null, count, td.name, td.unit);
								row.y = _ypos;

								damage_items.push(row);

								_ypos += row.height;
								damage_content.addChild(row);

								count++;
							}
						}

						damage_content.y = damage_founded_content.height + 20;
						detail_sp.addChild(damage_content);

						//			create Footer
						_damage_founded_content = new Sprite();
						var damage_founded_content_header:Sprite = new SurveyInfo.detail_normal_destroy_sp();

						_damage_founded_content.addChild(damage_founded_content_header);

						_ypos = damage_content_header.height;

						_memoEditItems = new Vector.<MemoEditItem>;
						var memo:MemoEditItem;
						memo = new MemoEditItem(null, _total_damage_founded.toString(), "");
						memo.y = _ypos;
						_damage_founded_content.addChild(memo);
						_total_damage_founded++;
						_memoEditItems.push(memo);

						_damage_founded_content.y = detail_sp.height;
						detail_sp.addChild(_damage_founded_content);*/
			content_sp.addChild(detail_sp);

			//			add Scroller

			setScrollbar(711, 592);
		}


		public function get treatmentDtail():Vector.<TreatmentDetailData>
		{
			var return_treatmentDtail:Vector.<TreatmentDetailData> = new Vector.<TreatmentDetailData>();

			for (var i:int = 0; i < damage_items.length; i++)
			{

				var items:AddDamageRowItem = damage_items[i];
				//if (items.quantity != "0")
				return_treatmentDtail.push(new TreatmentDetailData(items.id, "0", items.path));
			}

			return return_treatmentDtail;
		}

		//		get memoDetail
		public function get memoDetail():Vector.<MemoDetail>
		{
			if (!_memoEditItems)
				return null;

			var return_memos:Vector.<MemoDetail> = new Vector.<MemoDetail>;
			var count:int = 0;
			for each (var memo:MemoEditItem in _memoEditItems)
			{
				if (memo.title != "")
				{
					count++;
					var md:MemoDetail = new MemoDetail(count.toString(), memo.title, memo.id);
					return_memos.push(md);
				}

			}

			return return_memos;
		}

		public function getSelect(choice:String):String
		{
			var index:String = "0";
			if (damage_founded_content)
				var row:Sprite = damage_founded_content.getChildByName("choice_" + choice + "_sp") as Sprite;
			if (row)
				for (var j:int = 0; j < row.numChildren; j++)
				{
					var symbol:Sprite = row.getChildAt(j) as Sprite;

					if (symbol.alpha == 1)
					{
						var v_name:String = symbol.name.split("ck")[1]
						index = v_name.split("_")[0];
						break;
					}
				}

			return index;
		}

		public function selectedCK(choice:String, index:String):void
		{
			var choiceName:String = "ck" + choice + "_sp";
			if (damage_founded_content)
				var row:Sprite = damage_founded_content.getChildByName("choice_" + index + "_sp") as Sprite;
			if (row)
				for (var j:int = 0; j < row.numChildren; j++)
				{
					var symbol:Sprite = row.getChildAt(j) as Sprite;

					if (symbol.name == choiceName)
						symbol.alpha = 1;
					else
						symbol.alpha = 0;
				}


		}


		public function get content_sp():Sprite
		{
			return _skin.getChildByName("container_sp") as Sprite;
		}

		public function get date_tf():TextField
		{
			return _skin.getChildByName("date_tf") as TextField;
		}

		public function get today_date_tf():TextField
		{
			return _skin.getChildByName("today_date_tf") as TextField;
		}

		public function get next_date_tf():TextField
		{
			return _skin.getChildByName("next_date_tf") as TextField;
		}

		public function get pass_date_tf():TextField
		{
			return _skin.getChildByName("pass_date_tf") as TextField;
		}

		public function swichCheckIcon():void
		{
			if (checkIcon_sp.alpha == 1)
				checkIcon_sp.alpha = 0;
			else
				checkIcon_sp.alpha = 1;

		}

		public function get principle_special_flag():String
		{
			if (checkIcon_sp.alpha == 1)
				return "true";
			else
				return "false";
		}

		public function addDatePicker(pos:Point, onDateChangeSignal:Signal):void
		{
			//			check old date picker
			if (dc)
			{
				removeChild(dc);
				dc = null;
			}

			//			create date picker
			dc = new DateChooser(false);
			dc.setCellSize(40 * Config.ratio, 40 * Config.ratio);
			dc.headerHeight = 60 * Config.ratio;
			dc.setBackgroundColors([14870251, 16777215]);
			dc.gradientOffset = 65 * Config.ratio;
			dc.cellColor = 16777215;
			dc.highlightColor = 10079487;
			dc.selectedColor = 10066329;
			dc.todayColor = 14870251;
			dc.setBorderStyle(1, 13421772);

			dc.x = pos.x;
			dc.y = pos.y;

			dc.addEventListener(DateChooser.SELECTION_CHANGED, function(event:Event):void
			{
				onDateChangeSignal.dispatch(dc.selectedDates[0]);
				removeChild(dc);
				dc = null;
			});

			addChild(dc);
			dc.setCellSize(50 * Config.ratio, 50 * Config.ratio);
		}

		public function checkHitDC(event:MouseEvent):void
		{
			if (dc)
			{
				var dc_rect:Rectangle = new Rectangle(dc.x, dc.y, dc.width, dc.height);

				if (!dc_rect.containsPoint(new Point(event.stageX, event.stageY)))
				{
					removeChild(dc);
					dc = null;
				}
			}
		}


		public function addNewData():void
		{
			var i:int;
			var row:MemoEditItem = new MemoEditItem(null, _total_damage_founded.toString(), "");
			row.y = _ypos + row.height * (_total_damage_founded - 1);
			_damage_founded_content.addChild(row);
			_total_damage_founded++;

			_memoEditItems.push(row);

			setScrollbar(711, 592);
		}

		public function removeRowDataWithTarget(target:Sprite):void
		{
			if (_memoEditItems.length <= 1)
				return;

			var row:MemoEditItem;
			//remove view
			var counter:int = 0;
			for each (row in _memoEditItems)
			{
				_damage_founded_content.removeChild(row);
			}

			_ypos = _damage_founded_content.height;
			row = target.parent as MemoEditItem;
			if (row)
			{
				VectorUtil.removeItem(_memoEditItems, row);
				_total_damage_founded = 1;
			}

			var datas:Vector.<MemoEditItem> = new Vector.<MemoEditItem>;
			for each (var memo:MemoEditItem in _memoEditItems)
			{
				row = new MemoEditItem(memo.id, _total_damage_founded.toString(), memo.title);
				row.y = _ypos + row.height * (_total_damage_founded - 1);
				_damage_founded_content.addChild(row);
				_total_damage_founded++;

				datas.push(row);
			}

			_memoEditItems = datas;
			setScrollbar(711, 592);
		}

		private function setScrollbar(w:int, h:int):void
		{
			var containerViewport:Rectangle = new Rectangle(0, 0, w, h);

			if (!_contentScroll)
			{
				_contentScroll = new ScrollController();
				_contentScroll.horizontalScrollingEnabled = false;
			}
			else
			{
				_contentScroll.removeScrollControll();
			}

			if (detail_sp.height > containerViewport.height)
			{
				_contentScroll.addScrollControll(detail_sp, content_sp, containerViewport);
			}
		}

	}
}
