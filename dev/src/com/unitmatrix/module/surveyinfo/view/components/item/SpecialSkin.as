package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.MemoDetail;
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import SurveyInfo.detail_basic_2_sp;
	import SurveyInfo.detail_normal_destroy_sp;
	import SurveyInfo.survey_special_sp;

	public class SpecialSkin extends SDSprite
	{
		private var _skin:Sprite;

		private var _contentScroll:ScrollController;
		private var _contentScrollTab:ScrollController;

		private var _currentPrintciple:PrintcipleDetail;

		private var date_tabs:Vector.<DateTab>;

		private var container:Sprite;

		private var detail_2_sp:Sprite;
		private var detail_basic_destroy_sp:Sprite;


		public function SpecialSkin()
		{
			_skin = new SurveyInfo.survey_special_sp();
			name = "survey_special_sp";

			addChild(_skin);

			_contentScroll = new ScrollController();
			_contentScrollTab = new ScrollController();

		}

		public function get currentPrintcipleDetail():PrintcipleDetail
		{
			return _currentPrintciple;
		}


		public function activeTapWithID(printciple_detail:PrintcipleDetail, treatmentDatas:Vector.<TreatmentData>):void
		{
			for each (var dateTab:DateTab in date_tabs)
			{
				if (dateTab.name == "SpecialSkin_" + printciple_detail.id)
					dateTab.tap_bg_sp.alpha = 1;
				else
					dateTab.tap_bg_sp.alpha = 0;
			}

			_currentPrintciple = printciple_detail;

			if (!_currentPrintciple)
				return;

			if (detail_2_sp)
			{
				DisplayObjectUtil.removeChildren(detail_2_sp, true);
				if (detail_2_sp.parent)
					detail_2_sp.parent.removeChild(detail_2_sp);
			}
			if (detail_basic_destroy_sp)
			{
				DisplayObjectUtil.removeChildren(detail_basic_destroy_sp, true);
				if (detail_basic_destroy_sp.parent)
					detail_basic_destroy_sp.parent.removeChild(detail_basic_destroy_sp);
			}

			var counter:int = 0;

			if (_currentPrintciple.treatment_detail.length > 0)
			{
				detail_2_sp = new SurveyInfo.detail_basic_2_sp;
				container.addChild(detail_2_sp);

				counter = 0;


				for each (var treatmentDetailData:TreatmentDetailData in _currentPrintciple.treatment_detail)
				{
					var td:TreatmentData = VectorUtil.getItemByID(treatmentDatas, treatmentDetailData.id) as TreatmentData;
					var row:DamageRowItem = new DamageRowItem(treatmentDetailData.id, treatmentDetailData.path, counter, td.name, treatmentDetailData.quantity, td.unit);
					row.y = 40 + (counter * (row.height));
					detail_2_sp.addChild(row);

					if (!CoreModel.checkHaveImageAtID(treatmentDetailData.path))
					{
						row.cametaBTN.mouseEnabled = false;
						row.cametaBTN.enabled = false;
						row.cametaBTN.alpha = 0.2;
					}

					counter++
				}

			}

			if (_currentPrintciple.memo_detail.length > 0)
			{
				detail_basic_destroy_sp = new SurveyInfo.detail_normal_destroy_sp();
				detail_basic_destroy_sp.x = 0;
				detail_basic_destroy_sp.y = detail_2_sp.height + detail_2_sp.y;
				container.addChild(detail_basic_destroy_sp);

				var add_btn:SimpleButton = detail_basic_destroy_sp.getChildByName("add_btn") as SimpleButton;
				add_btn.visible = false;

				counter = 0;

				for each (var memoDetail:MemoDetail in _currentPrintciple.memo_detail)
				{
					var item2:DamageRowItem2 = new DamageRowItem2(memoDetail.path, counter, memoDetail.memo);
					item2.y = 40 + (counter * (item2.height));

					if (!CoreModel.checkHaveImageAtID(memoDetail.path))
					{
						item2.cametaBTN.mouseEnabled = false;
						item2.cametaBTN.enabled = false;
						item2.cametaBTN.alpha = 0.2;
					}

					detail_basic_destroy_sp.addChild(item2);
					counter++
				}
			}
			checked_mc.gotoAndStop(2);

			if (_currentPrintciple)
			{
				today_date_tf.text = _currentPrintciple.principle_dt || "0000-00-00";
				next_date_tf.text = _currentPrintciple.principle_next_dt || "0000-00-00";
				pass_date_tf.text = _currentPrintciple.principle_last_dt || "0000-00-00";
				checked_mc.gotoAndStop((_currentPrintciple.principle_special_flag == "true") ? 1 : 2);
			}
		}

		public function get checked_mc():MovieClip
		{
			return _skin.getChildByName("checked_mc") as MovieClip;
		}

		public function addBasicDamageDetail(printciple_detail:Vector.<PrintcipleDetail>, treatmentDatas:Vector.<TreatmentData>):void
		{
			container = new Sprite;
			container_sp.addChild(container);

			//add date menu tab
			var tabContainer:Sprite = new Sprite();
			tabContainer.x = 24;
			tabContainer.y = 22;

			var tabContent:Sprite = new Sprite();
			var counter:int = 0;

			date_tabs = new Vector.<DateTab>();

			for each (var printcipleDetail:PrintcipleDetail in printciple_detail)
			{
				if (!_currentPrintciple)
					_currentPrintciple = printcipleDetail;

				if (printcipleDetail.addstt == "1")
				{
					var tab_sp:DateTab = new DateTab(counter, printcipleDetail.principle_dt);
					tab_sp.name = "SpecialSkin_" + printcipleDetail.id;
					tab_sp.x = counter * tab_sp.width;
					tabContent.addChild(tab_sp);

					date_tabs.push(tab_sp)
					counter++;
				}
			}

			tabContainer.addChild(tabContent);
			addChild(tabContainer);

			if (_currentPrintciple.treatment_detail.length > 0)
			{
				detail_2_sp = new SurveyInfo.detail_basic_2_sp;
				container.addChild(detail_2_sp);

				counter = 0;

				for each (var treatmentDetailData:TreatmentDetailData in _currentPrintciple.treatment_detail)
				{
					var td:TreatmentData = VectorUtil.getItemByID(treatmentDatas, treatmentDetailData.id) as TreatmentData;
					var row:DamageRowItem = new DamageRowItem(treatmentDetailData.id, treatmentDetailData.path, counter, td.name, treatmentDetailData.quantity, td.unit);
					row.y = 40 + (counter * (row.height));

					if (!CoreModel.checkHaveImageAtID(treatmentDetailData.path))
					{
						row.cametaBTN.mouseEnabled = false;
						row.cametaBTN.enabled = false;
						row.cametaBTN.alpha = 0.2;
					}

					detail_2_sp.addChild(row);
					counter++
				}
			}

			if (_currentPrintciple.memo_detail.length > 0)
			{
				detail_basic_destroy_sp = new SurveyInfo.detail_normal_destroy_sp();
				detail_basic_destroy_sp.x = 0;
				detail_basic_destroy_sp.y = detail_2_sp.height + detail_2_sp.y;
				container.addChild(detail_basic_destroy_sp);

				var add_btn:SimpleButton = detail_basic_destroy_sp.getChildByName("add_btn") as SimpleButton;
				add_btn.visible = false;
				counter = 0;
				for each (var memoDetail:MemoDetail in _currentPrintciple.memo_detail)
				{
					var item2:DamageRowItem2 = new DamageRowItem2(memoDetail.path, counter, memoDetail.memo);
					item2.y = 40 + (counter * (item2.height));

					if (!CoreModel.checkHaveImageAtID(memoDetail.path))
					{
						item2.cametaBTN.mouseEnabled = false;
						item2.cametaBTN.enabled = false;
						item2.cametaBTN.alpha = 0.2;
					}

					detail_basic_destroy_sp.addChild(item2);
					counter++
				}

			}

			var containerViewport:Rectangle = new Rectangle(0, 0, 710, 315);
			if (container.height > containerViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.addScrollControll(container, container_sp, containerViewport);
			}

			var containerTabViewport:Rectangle = new Rectangle(0, 0, 850, 43);
			if (tabContent.height > containerTabViewport.width)
			{
				_contentScrollTab.verticalScrollingEnabled = false;
				_contentScrollTab.addScrollControll(tabContent, tabContainer, containerTabViewport);
			}

			checked_mc.gotoAndStop(2);

			if (_currentPrintciple)
			{
				today_date_tf.text = _currentPrintciple.principle_dt || "0000-00-00";
				next_date_tf.text = _currentPrintciple.principle_next_dt || "0000-00-00";
				pass_date_tf.text = _currentPrintciple.principle_last_dt || "0000-00-00";
				checked_mc.gotoAndStop((_currentPrintciple.principle_special_flag == "true") ? 1 : 2);
			}

		}

		public function get container_sp():Sprite
		{
			return _skin.getChildByName("container_sp") as Sprite;
		}

		public function get today_date_tf():TextField
		{
			return _skin.getChildByName("today_date_tf") as TextField;
		}

		public function get next_date_tf():TextField
		{
			return _skin.getChildByName("next_date_tf") as TextField;
		}

		public function get pass_date_tf():TextField
		{
			return _skin.getChildByName("pass_date_tf") as TextField;
		}

		public function show():void
		{
			visible = true;
			mouseChildren = true;
			mouseEnabled = true;
		}

		public function hide():void
		{
			visible = false;
			mouseChildren = false;
			mouseEnabled = false;
		}

		public function dispose():void
		{
			_contentScroll.removeScrollControll();
			_contentScrollTab.removeScrollControll();
			super.destroy();
		}

		public function get currentPrintciple():PrintcipleDetail
		{
			return _currentPrintciple;
		}

	}
}
