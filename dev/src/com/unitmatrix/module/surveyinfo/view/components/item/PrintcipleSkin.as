package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.core.model.data.PrintcipleDetail;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.display.SDSprite;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import SurveyInfo.detail_basic_sp;
	import SurveyInfo.survey_basic_sp;

	public class PrintcipleSkin extends SDSprite
	{
		private var _skin:Sprite;
		private var _detail_sp:Sprite;

		private var _contentScroll:ScrollController;
		private var _contentScrollTab:ScrollController;

		private var _currentPrintciple:PrintcipleDetail;

		private var date_tabs:Vector.<DateTab>;

		private var container:Sprite;


		public function PrintcipleSkin()
		{
			_skin = new SurveyInfo.survey_basic_sp();
			name = "survey_basic_sp";

			addChild(_skin);

			alert_mc.visible = false;
			alert_mc.stop();

			_contentScroll = new ScrollController();
			_contentScrollTab = new ScrollController();

		}

		public function get currentPrintcipleDetail():PrintcipleDetail
		{
			return _currentPrintciple;
		}


		public function activeTapWithID(printciple_detail:PrintcipleDetail, treatmentDatas:Vector.<TreatmentData>):void
		{
			for each (var dateTab:DateTab in date_tabs)
			{
				if (dateTab.name == "PrintcipleSkin_" + printciple_detail.id)
					dateTab.tap_bg_sp.alpha = 1;
				else
					dateTab.tap_bg_sp.alpha = 0;
			}

			_currentPrintciple = printciple_detail;
			if (!_currentPrintciple)
				return;
//			
			DisplayObjectUtil.removeChildren(_detail_sp, true);
			container.removeChild(_detail_sp);


			var counter:int = 0;

			_detail_sp = new SurveyInfo.detail_basic_sp;
			container.addChild(_detail_sp);

			selectedCK(_currentPrintciple.superstruct_rate == null ? "n" : _currentPrintciple.superstruct_rate, "0");
			selectedCK(_currentPrintciple.substruct_rate == null ? "n" : _currentPrintciple.substruct_rate, "1");
			selectedCK(_currentPrintciple.bridgeneck_rate == null ? "n" : _currentPrintciple.bridgeneck_rate, "2");
			selectedCK(_currentPrintciple.slopeprotection_rate == null ? "n" : _currentPrintciple.slopeprotection_rate, "3");
			selectedCK(_currentPrintciple.surface_point == null ? "n" : _currentPrintciple.surface_point, "4");

			checked_mc.gotoAndStop(2);

			if (_currentPrintciple)
			{
				today_date_tf.text = _currentPrintciple.principle_dt || "0000-00-00";
				next_date_tf.text = _currentPrintciple.principle_next_dt || "0000-00-00";
				pass_date_tf.text = _currentPrintciple.principle_last_dt || "0000-00-00";
				checked_mc.gotoAndStop((_currentPrintciple.principle_special_flag == "true") ? 1 : 2);
			}

			//if (int(_currentPrintciple.superstruct_rate) < 3 || int(_currentPrintciple.substruct_rate) < 3 || int(_currentPrintciple.bridgeneck_rate) < 3 || int(_currentPrintciple.slopeprotection_rate) < 3 || int(_currentPrintciple.surface_point) < 3)
			if (_currentPrintciple.addstt == "1")
			{
				alert_mc.visible = true;
				alert_mc.play();
			}
			else
			{
				alert_mc.visible = false;
				alert_mc.stop();
			}
		}

		public function get checked_mc():MovieClip
		{
			return _skin.getChildByName("checked_mc") as MovieClip;
		}

		public function get alert_mc():MovieClip
		{
			return _skin.getChildByName("alert_mc") as MovieClip;
		}

		public function addBasicDamageDetail(printciple_detail:Vector.<PrintcipleDetail>, treatmentDatas:Vector.<TreatmentData>):void
		{
			container = new Sprite;
			container_sp.addChild(container);

			//add date menu tab
			var tabContainer:Sprite = new Sprite();
			tabContainer.x = 24;
			tabContainer.y = 22;

			var tabContent:Sprite = new Sprite();
			var counter:int = 0;

			date_tabs = new Vector.<DateTab>();

			for each (var printcipleDetail:PrintcipleDetail in printciple_detail)
			{
				if (!_currentPrintciple)
					_currentPrintciple = printcipleDetail;

				var tab_sp:DateTab = new DateTab(counter, printcipleDetail.principle_dt);
				tab_sp.name = "PrintcipleSkin_" + printcipleDetail.id;
				tab_sp.x = counter * tab_sp.width;
				tabContent.addChild(tab_sp);

				date_tabs.push(tab_sp)
				counter++;
			}

			tabContainer.addChild(tabContent);
			addChild(tabContainer);

			_detail_sp = new SurveyInfo.detail_basic_sp;
			container.addChild(_detail_sp);

			var containerViewport:Rectangle = new Rectangle(0, 0, 710, 315);
			if (container.height > containerViewport.height)
			{
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.addScrollControll(container, container_sp, containerViewport);
			}

			var containerTabViewport:Rectangle = new Rectangle(0, 0, 850, 43);
			if (tabContent.height > containerTabViewport.width)
			{
				_contentScrollTab.verticalScrollingEnabled = false;
				_contentScrollTab.addScrollControll(tabContent, tabContainer, containerTabViewport);
			}


			selectedCK(_currentPrintciple.superstruct_rate == null ? "n" : _currentPrintciple.superstruct_rate, "0");
			selectedCK(_currentPrintciple.substruct_rate == null ? "n" : _currentPrintciple.substruct_rate, "1");
			selectedCK(_currentPrintciple.bridgeneck_rate == null ? "n" : _currentPrintciple.bridgeneck_rate, "2");
			selectedCK(_currentPrintciple.slopeprotection_rate == null ? "n" : _currentPrintciple.slopeprotection_rate, "3");
			selectedCK(_currentPrintciple.surface_point == null ? "n" : _currentPrintciple.surface_point, "4");

			checked_mc.gotoAndStop(2);

			if (_currentPrintciple)
			{
				today_date_tf.text = _currentPrintciple.principle_dt || "0000-00-00";
				next_date_tf.text = _currentPrintciple.principle_next_dt || "0000-00-00";
				pass_date_tf.text = _currentPrintciple.principle_last_dt || "0000-00-00";
				checked_mc.gotoAndStop((_currentPrintciple.principle_special_flag == "true") ? 1 : 2);
			}

			if (_currentPrintciple.addstt == "1")
			{
				alert_mc.visible = true;
				alert_mc.play();
			}
			else
			{
				alert_mc.visible = false;
				alert_mc.stop();
			}

		}

		public function selectedCK(choice:String, index:String):void
		{
			var choiceName:String = "ck" + choice + "_sp";
			if (_detail_sp)
				var row:Sprite = _detail_sp.getChildByName("choice_" + index + "_sp") as Sprite;
			if (row)
				for (var j:int = 0; j < row.numChildren; j++)
				{
					var symbol:Sprite = row.getChildAt(j) as Sprite;

					if (symbol.name == choiceName)
						symbol.alpha = 1;
					else
						symbol.alpha = 0;
				}


		}

		private function reset():void
		{
			if (!_detail_sp)
				return;

			var total:int = 5;
			for (var i:int = 0; i < total; i++)
			{
				var row:Sprite = _detail_sp.getChildByName("choice_" + i + "_sp") as Sprite;
				for (var j:int = 0; j < row.numChildren; j++)
				{
					var symbol:Sprite = row.getChildAt(j) as Sprite;
					symbol.buttonMode = true;

					if (symbol.name.indexOf("0") == -1)
						symbol.alpha = 0.0;
				}

			}

		}

		public function get container_sp():Sprite
		{
			return _skin.getChildByName("container_sp") as Sprite;
		}

		public function get today_date_tf():TextField
		{
			return _skin.getChildByName("today_date_tf") as TextField;
		}

		public function get next_date_tf():TextField
		{
			return _skin.getChildByName("next_date_tf") as TextField;
		}

		public function get pass_date_tf():TextField
		{
			return _skin.getChildByName("pass_date_tf") as TextField;
		}

		public function show():void
		{
			visible = true;
			mouseChildren = true;
			mouseEnabled = true;
		}

		public function hide():void
		{
			visible = false;
			mouseChildren = false;
			mouseEnabled = false;
		}

		public function dispose():void
		{
			_contentScroll.removeScrollControll();
			_contentScrollTab.removeScrollControll();
			super.destroy();
		}

		public function get currentPrintciple():PrintcipleDetail
		{
			return _currentPrintciple;
		}

	}
}
