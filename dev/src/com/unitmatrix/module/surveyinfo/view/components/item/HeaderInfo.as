package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.module.surveyinfo.view.SurveyInfoModule;
	import com.sleepydesign.display.SDSprite;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;

	import SurveyInfo.header_sp;

	public class HeaderInfo extends SDSprite
	{
		private var _skin:Sprite;
		private var _mode:String;

		public function HeaderInfo(birdge_name:String, group_name:String, highway_number:String, localtion:String, width:String, direction:String, type:String, kide:String, damage:String, bpi:String, year:String, span:String)
		{
			_skin = new SurveyInfo.header_sp();
			addChild(_skin);

			if (highway_number != "")
				highway_number = "หมายเลขทางหลวง " + highway_number;

			birdge_name_tf.text = birdge_name || "ไม่มีข้อมูล";
			group_name_tf.text = group_name || "ไม่มีข้อมูล";
			highway_number_tf.text = highway_number || "ไม่มีข้อมูล";
			width_tf.text = width || "ไม่มีข้อมูล";
			direction_tf.text = direction || "ไม่มีข้อมูล";
			type_tf.text = type || "ไม่มีข้อมูล";
			kide_tf.text = kide || "ไม่มีข้อมูล";
			damage_tf.text = damage || "ไม่มีข้อมูล";
			bpi_tf.text = bpi || "ไม่มีข้อมูล";
			year_tf.text = year || "ไม่มีข้อมูล";
			localtion_tf.text = localtion || "ไม่มีข้อมูล";
			span_tf.text = span || "ไม่มีข้อมูล";
		}

		public function get survey_special_btn():MovieClip
		{
			return _skin.getChildByName("special_survey_btn") as MovieClip;
		}

		public function get survey_basic_btn():MovieClip
		{
			return _skin.getChildByName("basic_survey_btn") as MovieClip;
		}

		public function get survey_normal_btn():MovieClip
		{
			return _skin.getChildByName("normal_survey_btn") as MovieClip;
		}

		public function get birdge_name_tf():TextField
		{
			return _skin.getChildByName("birdge_name_tf") as TextField;
		}

		public function get group_name_tf():TextField
		{
			return _skin.getChildByName("group_name_tf") as TextField;
		}

		public function get highway_number_tf():TextField
		{
			return _skin.getChildByName("highway_number_tf") as TextField;
		}

		public function get width_tf():TextField
		{
			return _skin.getChildByName("width_tf") as TextField;
		}

		public function get direction_tf():TextField
		{
			return _skin.getChildByName("direction_tf") as TextField;
		}

		public function get type_tf():TextField
		{
			return _skin.getChildByName("type_tf") as TextField;
		}

		public function get kide_tf():TextField
		{
			return _skin.getChildByName("kide_tf") as TextField;
		}

		public function get damage_tf():TextField
		{
			return _skin.getChildByName("damage_tf") as TextField;
		}

		public function get bpi_tf():TextField
		{
			return _skin.getChildByName("bpi_tf") as TextField;
		}

		public function get year_tf():TextField
		{
			return _skin.getChildByName("year_tf") as TextField;
		}

		public function get span_tf():TextField
		{
			return _skin.getChildByName("span_tf") as TextField;
		}

		public function get localtion_tf():TextField
		{
			return _skin.getChildByName("localtion_tf") as TextField;
		}

		public function set mode(value:String):void
		{
			if (value == _mode)
				return;

			if (value == SurveyInfoModule.SURVEY_MODE_BASIC)
			{
				survey_basic_btn.gotoAndStop(1);
				survey_normal_btn.gotoAndStop(2);
				survey_special_btn.gotoAndStop(2);
			}
			else if (value == SurveyInfoModule.SURVEY_MODE_NORMAL)
			{
				survey_basic_btn.gotoAndStop(2);
				survey_normal_btn.gotoAndStop(1);
				survey_special_btn.gotoAndStop(2);
			}
			else if (value == SurveyInfoModule.SURVEY_MODE_SPECIAL)
			{
				survey_basic_btn.gotoAndStop(2);
				survey_normal_btn.gotoAndStop(2);
				survey_special_btn.gotoAndStop(1);
			}
		}

		public function set useSpecialBTN(value:Boolean):void
		{
			survey_special_btn.mouseEnabled = value;
		}

		public function get mode():String
		{
			return _mode;
		}

	}
}
