package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.module.surveyinfo.model.data.ImageData;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.skins.MacLoadingClip;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	import Gallery.image_sp;

	import SurveyInfo.block_image;

	import org.osflash.signals.Signal;

	public class ImageView extends SDSprite
	{
		public var deletSignal:Signal = new Signal();
		public var loader:ImageLoader;
		private var _skin:Sprite;
		private var _imageData:ImageData;

		public function ImageView()
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new Gallery.image_sp();
			addChild(_skin);
			container_sp.scrollRect = new Rectangle(0, 0, container_sp.width, container_sp.height);

			resize();
			initHandler();
		}

		private function initHandler():void
		{
			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		protected function onMouseUp(event:MouseEvent):void
		{
			switch (event.target.name)
			{
				case "image_close_btn":
					dispose();
					break;
				case "image_delete_btn":
					_imageData.f.deleteFile(); 
					deletSignal.dispatch();
					break;
			}
		}

		private function resize():void
		{
			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x = Config.GUI_SIZE.width / 2 - _skin.width / 2;
			_skin.y = Config.GUI_SIZE.height / 2 - _skin.height / 2;
		}

		public function get container_sp():Sprite
		{
			return _skin.getChildByName("container_sp") as Sprite;
		}

		public function initWithImageData(imageData:ImageData):void
		{
			_imageData = imageData;

			var preload:MacLoadingClip = new MacLoadingClip();
			preload.x = container_sp.width * 0.5 - preload.width * 0.5;
			preload.y = container_sp.height * 0.5 - preload.height * 0.5;

			container_sp.addChild(preload);

			loader = new ImageLoader(_imageData.f.url, {name: "image", container: container_sp, width: container_sp.width, height: container_sp.height, scaleMode: "proportionalInside", onComplete: function(event:LoaderEvent):void
			{
				container_sp.removeChild(preload);
				preload.destroy();
			}});
		}

		public function dispose():void
		{
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			deletSignal.removeAll();
			destroy();
		}

	}
}

