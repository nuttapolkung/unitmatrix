package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.module.surveyinfo.model.data.ImageData;
	import com.unitmatrix.module.surveyinfo.view.components.item.GalleryThumbnail;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.greensock.loading.LoaderMax;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.display.SDSprite;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import Gallery.gallery_sp;

	import SurveyInfo.block_image;

	import org.osflash.signals.Signal;

	public class GalleryView extends SDSprite
	{
		private var gallery_sp:Sprite;
		private var container_sp:Sprite;
		private var content_sp:Sprite;
		private var contentScroll:ScrollController;
		private var thumbs:Vector.<GalleryThumbnail>

		private var _gallery_ID:String = "_";

		private var capture_btn:MovieClip;

		public var dispose_signal:Signal = new Signal();

		public function get gallery_ID():String
		{
			return _gallery_ID;
		}

		public function set gallery_ID(value:String):void
		{
			_gallery_ID = value;
		}

		public function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			gallery_sp = new Gallery.gallery_sp;
			gallery_sp.name = "gallery_sp";
			gallery_sp.x = 17;
			gallery_sp.y = 30;
			addChild(gallery_sp);

			container_sp = gallery_sp.getChildByName("container_sp") as Sprite;

			resize();
		}

		private function resize():void
		{
			gallery_sp.scaleX = gallery_sp.scaleY = Config.ratio;
			gallery_sp.x = Config.GUI_SIZE.width * 0.5 - gallery_sp.width * 0.5;
			gallery_sp.y = Config.GUI_SIZE.height * 0.5 - gallery_sp.height * 0.5;
		}

		public function createGallery(images:Vector.<ImageData>):void
		{
			if(!content_sp){
				content_sp = new Sprite();
			}else{
				DisplayObjectUtil.removeChildren(content_sp);
			}

			capture_btn = container_sp.getChildByName("capture_btn") as MovieClip;
			content_sp.addChild(capture_btn);


			var total:int = images.length;

			var col:int = 3;
			var row:int = total / col;
			if (total % col != 0)
				row++;

			thumbs = new Vector.<GalleryThumbnail>;

			var loaderMaax:LoaderMax = new LoaderMax();
			loaderMaax.maxConnections = 1;

			var counter:int = 1;
			for (var i:int = 0; i < total; i++)
			{
				var thumb_nail:GalleryThumbnail = new GalleryThumbnail();
				thumb_nail.name =  new Date().time.toString();


				thumb_nail.initWithImageData(images[i]);
				loaderMaax.append(thumb_nail.loader);

				thumb_nail.x = (counter % col) * (thumb_nail.width + 10);


				thumb_nail.y = int(counter / col) * (thumb_nail.height + 15);

				content_sp.addChild(thumb_nail);

				thumb_nail.mouseChildren = false;
				thumb_nail.buttonMode = true;

				thumbs.push(thumb_nail);
				counter++;
			}

			loaderMaax.load();

			content_sp.x = 35;
			content_sp.y = 0;

			container_sp.addChild(content_sp);

			var containerViewport:Rectangle = new Rectangle(0, 0, 940, 604);

			contentScroll = new ScrollController();
			contentScroll.horizontalScrollingEnabled = false;
			contentScroll.addScrollControll(content_sp, container_sp, containerViewport);

		}

		private function relayout():void
		{
			trace("Relayout");
			var total:int = thumbs.length;

			var col:int = 3;
			var row:int = total / col;
			if (total % col != 0)
				row++;

			for (var i:int = 1; i < total; i++)
			{
				var thumb_nail:GalleryThumbnail = thumbs[i - 1];

				thumb_nail.x = (i % col) * (thumb_nail.width + 10);
				thumb_nail.y = int(i / col) * (thumb_nail.height + 15);
			}

		}

		public function addImage(image:ImageData):void
		{
			var thumb_nail:GalleryThumbnail = new GalleryThumbnail();
			thumb_nail.name = new Date().time.toString();

			thumb_nail.initWithImageData(image);

			thumb_nail.x = 0 * (thumb_nail.width + 10);
			thumb_nail.y = 0 * (thumb_nail.height + 15);

			content_sp.addChild(thumb_nail);

			thumb_nail.mouseChildren = false;
			thumb_nail.buttonMode = true;

			thumbs.push(thumb_nail);

			var i:int = thumbs.length;
			var col:int = 3;

			thumb_nail.loader.load();

			thumb_nail.x = (i % col) * (thumb_nail.width + 10);
			thumb_nail.y = int(i / col) * (thumb_nail.height + 15);

			content_sp.addChild(capture_btn);
			contentScroll.setContentRect(new Rectangle(0, 0, content_sp.width, thumb_nail.y + thumb_nail.height + 20));

		}

		override public function destroy():void
		{
			dispose_signal.dispatch();
			dispose_signal = null;
			super.destroy();
		}
	}
}

