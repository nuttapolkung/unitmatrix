package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.unitmatrix.module.surveyinfo.model.data.AlbumData;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;

	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class PhotoAlbumMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:PhotoAlbumView;

		[Inject]
		public var model:SurveyInfoModel;

		// create ------------------------------------------------------------------

		override public function onRegister():void
		{
			module.create();
			initMouseHandle();
			module.albumID = coreModel.currentPrintciple.path;
			module.createAlbum(new Vector.<AlbumData>);
		}

		private function initMouseHandle():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			var target_name:String = event.target.name;

			switch (target_name)
			{
				case "close_btn":
				{
					if (event.target.parent)
						if (event.target.parent.name == "album_sp")
							module.destroy();
					break;
				}

				default:
				{
					break;
				}
			}

			if (target_name.indexOf("thumb_nail") != -1)
			{
				var album_id:String = target_name.split("_")[2];
				model.select_album_id = album_id;
				var gallery:GalleryView = new GalleryView();
				gallery.dispose_signal.addOnce(updateAlbum);
				gallery.gallery_ID = (event.target).albumID;
				module.addChild(gallery);
			}
		}

		private function updateAlbum():void
		{
			module.refreshView();

		}

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			module = null;
		}
	}
}
