package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.globalization.DateTimeFormatter;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SurveyInfoEditSpecialMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var view:SurveyInfoEditSpecialView;

		[Inject]
		public var model:SurveyInfoModel;

		// create ------------------------------------------------------------------

		private var _dialogComponent:DialogComponent


		override public function onRegister():void
		{
			trace("! " + this + "register.");

			view.create();
			view.addDetail(coreModel.treatmentDatas, coreModel.currentPrintciple);
			initMouseHandler();
		}

		private function initMouseHandler():void
		{
			eventMap.mapListener(view, MouseEvent.MOUSE_UP, onMouseUp);

		}

		private function saveData():void
		{
			var routine_dt:String = view.today_date_tf.text;
			var routine_last_dt:String = view.pass_date_tf.text;
			var routine_next_dt:String = view.next_date_tf.text;

			var principle_special_flag:String = view.principle_special_flag;

			coreModel.currentPrintciple.principle_dt = routine_dt;
			coreModel.currentPrintciple.principle_last_dt = routine_last_dt;
			coreModel.currentPrintciple.principle_next_dt = routine_next_dt;
			coreModel.currentPrintciple.principle_special_flag = principle_special_flag;
			coreModel.currentPrintciple.memo_detail = view.memoDetail;
			coreModel.currentPrintciple.treatment_detail = view.treatmentDtail;

			//set to save
			coreModel.itemToSave = true;
		}

		private function onMouseUp(event:MouseEvent):void
		{
			var onDateChangeSignal:Signal = new Signal(Date);
			var dfm:DateTimeFormatter = new DateTimeFormatter("en-US");
			dfm.setDateTimePattern("dd-MM-yyyy");
			var targetName:String = event.target.name;

			switch (targetName)
			{
				case "save_btn":
				case "close_btn":
				{
					var dialogSignal:Signal = new Signal(String);
					dialogSignal.addOnce(function(value:String):void
					{
						if (value == DialogComponent.DIALOGCOMPONENT_CLICK_YES)
							saveData();

						_dialogComponent.dispose();
						_dialogComponent = null;

						//if(targetName == "close_btn")
						view.dispose();
					});
					_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#000000'>ต้องการบันทึกข้อมูลรึไม่</font>", DialogComponent.YES_NO, dialogSignal);

					lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);

					break;
				}
				case "special_checked_sp":
				{
					view.swichCheckIcon();
					return;
					break;
				}
				case "founded_damage_detail_btn":
				{
					var photoAlbum:PhotoAlbumView = new PhotoAlbumView();
					photoAlbum.albumID = coreModel.currentPrintciple.path;
					view.addChild(photoAlbum);
					break;
				}
				case "today_date_btn":
				{

					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.today_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 40) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "next_date_btn":
				{
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.next_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 40) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "pass_date_btn":
				{
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.pass_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 40) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "add_btn":
				{
					view.addNewData();
					break;
				}
				case "delete_btn":
				{
					view.removeRowDataWithTarget((event.target).parent);
					break;
				}
				default:
				{
					view.checkHitDC(event);
					break;
				}
			}


		/*if (targetName.indexOf("ck") != -1)
		{
			var index:int = String(event.target.parent.name).split("_")[1];
			var select:SelectDamageLevel = new SelectDamageLevel(index + 1);
			select.completeSignal.addOnce(function():void
			{
				view.selectedCK(select.selected, index.toString());
			});
			lightBoxSignal.dispatch(select, LightBoxModule.ALERT_LAYER);
			return;
		}
*/
		}


		override public function onRemove():void
		{
			eventMap.unmapListener(view, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			view = null;
		}


	}
}
