package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.module.surveyinfo.model.data.ImageData;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.sleepydesign.skins.MacLoadingClip;

	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import Gallery.image_view_sp;

	public class GalleryThumbnail extends Sprite
	{
		private var _imageData:ImageData;

		public var loader:ImageLoader;

		public function GalleryThumbnail()
		{
			var thumb_nail:Sprite = new Gallery.image_view_sp();
			addChild(thumb_nail);
			this.scrollRect = new Rectangle(0, 0, thumb_nail.width, thumb_nail.height);
		}

		public function get imageData():ImageData
		{
			return _imageData;
		}

		public function initWithImageData(imageData:ImageData):void
		{
			_imageData = imageData;

			var preload:MacLoadingClip = new MacLoadingClip();
			preload.x = width * 0.5 - preload.width * 0.5;
			preload.y = height * 0.5 - preload.height * 0.5;

			addChild(preload);

			loader = new ImageLoader(_imageData.f.url, {name: "image", container: this, width: width, height: height, scaleMode: "proportionalInside", onComplete: function(event:LoaderEvent):void
			{
				removeChild(preload);
				preload.destroy();
				trace("Load Image complete.");
			}});


		}

	}
}

