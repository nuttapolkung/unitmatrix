package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.globalization.DateTimeFormatter;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SurveyInfoAddNormalMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:SurveyInfoAddNormalView;

		[Inject]
		public var model:SurveyInfoModel;

		// create ------------------------------------------------------------------
		private var _dialogComponent:DialogComponent;


		override public function onRegister():void
		{
			trace("! " + this + "register.");
			module.create();
			module.addDetail(coreModel.treatmentDatas);
			initMouseHandler();

		}

		private function initMouseHandler():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function saveData():void
		{
			var routine_dt:String = module.today_date_tf.text;
			var routine_last_dt:String = module.pass_date_tf.text;
			var routine_next_dt:String = module.next_date_tf.text;
			var routine_special_flag:String = module.routine_special_flag;

			if (routineDetail == null)
			{
				//TODO
				routineDetail = new RoutineDetail(null, routine_dt, routine_last_dt, routine_next_dt, routine_special_flag, module.memoDetail, module.treatmentDtail, "0");
				var _bridgeData:BridgeData = coreModel.getBridgeDataByID(coreModel.select_survey_id);
				_bridgeData.routine_detail.push(routineDetail);
			}
			else
			{
				routineDetail.routine_dt = routine_dt;
				routineDetail.routine_last_dt = routine_last_dt;
				routineDetail.routine_next_dt = routine_next_dt;
				routineDetail.routine_special_flag = routine_special_flag;
				routineDetail.memo_detail = module.memoDetail;
				routineDetail.treatment_detail = module.treatmentDtail;
			}

			//set to save
			coreModel.itemToSave = true;
		}


		private var pressSaveBTN_Notyet:Boolean = false;

		private var routineDetail:RoutineDetail;

		private function onMouseUp(event:MouseEvent):void
		{
			var onDateChangeSignal:Signal = new Signal(Date);
			var dfm:DateTimeFormatter = new DateTimeFormatter("en-US");
			dfm.setDateTimePattern("dd-MM-yyyy");
			var targetName:String = event.target.name;


			var dialogSignal:Signal;

			switch (targetName)
			{
				case "save_btn":
				/*pressSaveBTN_Notyet = true;
				dialogSignal = new Signal(String);
				dialogSignal.addOnce(function():void
				{
					_dialogComponent.dispose();
					_dialogComponent = null;

				});
				_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#000000'>บันทึกข้อมูลเรียบร้อย</font>", DialogComponent.CLOSE, dialogSignal);

				lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
				saveData();
				break;*/

				case "close_btn":
				{
					/*if (pressSaveBTN_Notyet)
					{
						module.updateRoutineSignal.dispatch();
						module.destroy();
					}
					else
					{*/
					dialogSignal = new Signal(String);
					dialogSignal.addOnce(function(value:String):void
					{
						if (value == DialogComponent.DIALOGCOMPONENT_CLICK_YES)
							saveData();

						_dialogComponent.dispose();
						_dialogComponent = null;

						/*if (targetName == "close_btn")
						{*/
						module.updateRoutineSignal.dispatch();
						module.dispose();
						//}
					});
					_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#000000'>ต้องการบันทึกข้อมูลรึไม่</font>", DialogComponent.YES_NO, dialogSignal);

					lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);
					//}
					break;
				}
				case "special_checked_sp":
				{
					module.swichCheckIcon();
					break;
				}
				case "today_date_btn":
				{

					onDateChangeSignal.addOnce(function(date:Date):void
					{
						module.today_date_tf.text = dfm.format(date);
					});
					module.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "next_date_btn":
				{
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						module.next_date_tf.text = dfm.format(date);
					});
					module.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "pass_date_btn":
				{
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						module.pass_date_tf.text = dfm.format(date);
					});
					module.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				}
				case "add_btn":
				{
					module.addNewData();
					break;
				}
				case "delete_btn":
				{
					module.removeRowDataWithTarget((event.target).parent);
					break;
				}
				default:
				{
					module.checkHitDC(event);
					break;
				}
			}

		}


		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			module = null;
		}

	}
}
