package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.text.TextField;

	import SurveyInfo.damage_row_detail_sp;

	public class DamageRowItem extends SDSprite
	{
		private var _id:String;
		private var _skin:Sprite;
		private var _path:String;

		public function DamageRowItem(id:String, path:String, index:int, title:String, quantity:String, unit:String)
		{
			_skin = new SurveyInfo.damage_row_detail_sp();
			addChild(_skin);

			tab_bg_sp.alpha = (index % 2 == 0) ? 0 : .3;
			_path = path;
			_id = id;
			rank_tf.text = (index + 1).toString();
			title_tf.text = title;
			quantity_tf.text = Number(quantity).toString();
			unit_tf.text = unit;

		}

		public function get rank_tf():TextField
		{
			return _skin.getChildByName("rank_tf") as TextField;
		}

		public function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}

		public function get quantity_tf():TextField
		{
			return _skin.getChildByName("quantity_tf") as TextField;
		}

		public function get unit_tf():TextField
		{
			return _skin.getChildByName("unit_tf") as TextField;
		}

		public function get tab_bg_sp():Sprite
		{
			return _skin.getChildByName("tab_bg_sp") as Sprite;
		}

		public function get cametaBTN():SimpleButton
		{
			return _skin.getChildByName("camera_icon_btn") as SimpleButton;
		}

		public function get id():String
		{
			return _id;
		}

		public function get path():String
		{
			return _path;
		}


	}
}
