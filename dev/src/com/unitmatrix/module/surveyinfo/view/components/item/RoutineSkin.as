package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.model.data.MemoDetail;
	import com.unitmatrix.core.model.data.RoutineDetail;
	import com.unitmatrix.core.model.data.TreatmentData;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.DisplayObjectUtil;
	import com.sleepydesign.display.SDSprite;
	import com.sleepydesign.utils.VectorUtil;

	import flash.display.MovieClip;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	import flash.text.TextField;

	import SurveyInfo.detail_normal_destroy_sp;
	import SurveyInfo.detail_normal_sp;
	import SurveyInfo.survey_normal_sp;

	public class RoutineSkin extends SDSprite
	{
		private var _skin:Sprite;
		private var _contentScroll:ScrollController;
		private var _contentScrollTab:ScrollController;
		private var _currentRoutineDetail:RoutineDetail;
		private var date_tabs:Vector.<DateTab>
		private var detail_normal_sp:Sprite;
		private var container:Sprite;
		private var tabContent:Sprite;
		private var tabContainer:Sprite;
		private var detail_normal_destroy_sp:Sprite;

		public function RoutineSkin()
		{
			_skin = new SurveyInfo.survey_normal_sp();
			addChild(_skin);

			name = "survey_normal_sp";

			_contentScroll = new ScrollController();
			_contentScrollTab = new ScrollController();
		}

		public function activeTapWithID(routineDetail:RoutineDetail, treatmentDatas:Vector.<TreatmentData>):void
		{
			for each (var dateTab:DateTab in date_tabs)
			{
				if (dateTab.name == "RoutineSkinTab_" + routineDetail.id)
					dateTab.tap_bg_sp.alpha = 1;
				else
					dateTab.tap_bg_sp.alpha = 0;
			}

			_currentRoutineDetail = routineDetail;

			if (!_currentRoutineDetail)
				return;

//			remove old
			if (detail_normal_sp)
			{
				DisplayObjectUtil.removeChildren(detail_normal_sp, true);
				container.removeChild(detail_normal_sp);
			}

			if (detail_normal_destroy_sp)
			{
				DisplayObjectUtil.removeChildren(detail_normal_destroy_sp, true);
				container.removeChild(detail_normal_destroy_sp);
			}
//			create new 
			if (_currentRoutineDetail.treatment_detail.length > 0)
			{
				detail_normal_sp = new SurveyInfo.detail_normal_sp();
				detail_normal_sp.x = 0;
				detail_normal_sp.y = 0;

				container.addChild(detail_normal_sp);

				var counter:int = 0;


				for each (var treatmentDetailData:TreatmentDetailData in _currentRoutineDetail.treatment_detail)
				{
					var td:TreatmentData = VectorUtil.getItemByID(treatmentDatas, treatmentDetailData.id) as TreatmentData;
					var row:DamageRowItem = new DamageRowItem(treatmentDetailData.id, treatmentDetailData.path, counter, td.name, treatmentDetailData.quantity, td.unit);
					row.y = 40 + (counter * (row.height));

					if (!CoreModel.checkHaveImageAtID(treatmentDetailData.path))
					{
						row.cametaBTN.mouseEnabled = false;
						row.cametaBTN.enabled = false;
						row.cametaBTN.alpha = 0.2;
					}

					detail_normal_sp.addChild(row);
					counter++
				}
			}

			if (_currentRoutineDetail.memo_detail.length > 0)
			{
				detail_normal_destroy_sp = new SurveyInfo.detail_normal_destroy_sp();
				detail_normal_destroy_sp.x = 0;
				detail_normal_destroy_sp.y = detail_normal_sp.height;
				container.addChild(detail_normal_destroy_sp);

				var add_btn:SimpleButton = detail_normal_destroy_sp.getChildByName("add_btn") as SimpleButton;
				add_btn.visible = false;

				counter = 0;

				for each (var memoDetail:MemoDetail in _currentRoutineDetail.memo_detail)
				{
					var item2:DamageRowItem2 = new DamageRowItem2(memoDetail.path, counter, memoDetail.memo);
					item2.y = 40 + (counter * (item2.height));

					if (!CoreModel.checkHaveImageAtID(memoDetail.path))
					{
						item2.cametaBTN.mouseEnabled = false;
						item2.cametaBTN.enabled = false;
						item2.cametaBTN.alpha = 0.2;
					}

					detail_normal_destroy_sp.addChild(item2);
					counter++
				}

			}

			checked_mc.gotoAndStop(2);

			if (_currentRoutineDetail)
			{
				today_date_tf.text = _currentRoutineDetail.routine_dt || "0000-00-00";
				next_date_tf.text = _currentRoutineDetail.routine_next_dt || "0000-00-00";
				pass_date_tf.text = _currentRoutineDetail.routine_last_dt || "0000-00-00";

				checked_mc.gotoAndStop((_currentRoutineDetail.routine_special_flag == "true") ? 1 : 2);
			}

		}

		public function addNormalDamageDetail(routine_detail:Vector.<RoutineDetail>, treatmentDatas:Vector.<TreatmentData>):void
		{
			container = new Sprite;
			container_sp.addChild(container);

			//add date menu tab
			tabContainer = new Sprite();
			tabContainer.x = 24;
			tabContainer.y = 22;

			date_tabs = new Vector.<DateTab>();

			tabContent = new Sprite();
			var counter:int = 0;

			var result:Array = VectorUtil.sortOn(routine_detail, "routine_date");

			for each (var routineDetail:RoutineDetail in result)
			{
				if (!_currentRoutineDetail)
					_currentRoutineDetail = routineDetail;

				var tab_sp:DateTab = new DateTab(counter, routineDetail.routine_dt);
				tab_sp.name = "RoutineSkinTab_" + routineDetail.id;
				tab_sp.x = counter * tab_sp.width;
				tabContent.addChild(tab_sp);
				counter++;

				date_tabs.push(tab_sp);
			}

			tabContainer.addChild(tabContent);
			addChild(tabContainer);


			checked_mc.gotoAndStop(2);


			if (!_currentRoutineDetail)
				return;

			today_date_tf.text = _currentRoutineDetail.routine_dt || "0000-00-00";
			next_date_tf.text = _currentRoutineDetail.routine_next_dt || "0000-00-00";
			pass_date_tf.text = _currentRoutineDetail.routine_last_dt || "0000-00-00";

			checked_mc.gotoAndStop((_currentRoutineDetail.routine_special_flag == "true") ? 1 : 2);


			if (_currentRoutineDetail.treatment_detail.length > 0)
			{
				detail_normal_sp = new SurveyInfo.detail_normal_sp();
				detail_normal_sp.x = 0;
				detail_normal_sp.y = 0;

				container.addChild(detail_normal_sp);

				counter = 0;

				if (_currentRoutineDetail)
					for each (var treatmentDetailData:TreatmentDetailData in _currentRoutineDetail.treatment_detail)
					{
						var td:TreatmentData = VectorUtil.getItemByID(treatmentDatas, treatmentDetailData.id) as TreatmentData;
						var row:DamageRowItem = new DamageRowItem(treatmentDetailData.id, treatmentDetailData.path, counter, td.name, treatmentDetailData.quantity, td.unit);
						row.y = 40 + (counter * (row.height));

						if (!CoreModel.checkHaveImageAtID(treatmentDetailData.path))
						{
							//gallery_495886019535
							row.cametaBTN.mouseEnabled = false;
							row.cametaBTN.enabled = false;
							row.cametaBTN.alpha = 0.2;
						}

						detail_normal_sp.addChild(row);
						counter++
					}
			}

			if (_currentRoutineDetail.memo_detail.length > 0)
			{
				detail_normal_destroy_sp = new SurveyInfo.detail_normal_destroy_sp();
				detail_normal_destroy_sp.x = 0;
				detail_normal_destroy_sp.y = (detail_normal_sp) ? detail_normal_sp.height : 0;
				container.addChild(detail_normal_destroy_sp);

				var add_btn:SimpleButton = detail_normal_destroy_sp.getChildByName("add_btn") as SimpleButton;
				add_btn.visible = false;

				counter = 0;

				for each (var memoDetail:MemoDetail in _currentRoutineDetail.memo_detail)
				{
					var item2:DamageRowItem2 = new DamageRowItem2(memoDetail.path, counter, memoDetail.memo);
					item2.y = 40 + (counter * (item2.height));

					if (!CoreModel.checkHaveImageAtID(memoDetail.path))
					{
						item2.cametaBTN.mouseEnabled = false;
						item2.cametaBTN.enabled = false;
						item2.cametaBTN.alpha = 0.2;
					}

					detail_normal_destroy_sp.addChild(item2);
					counter++
				}

			}

			var containerNormalViewport:Rectangle = new Rectangle(0, 0, 710, 315);
			if (container.height > containerNormalViewport.height)
			{
				_contentScroll = new ScrollController();
				_contentScroll.horizontalScrollingEnabled = false;
				_contentScroll.addScrollControll(container, container_sp, containerNormalViewport);
			}

			var containerTabViewport:Rectangle = new Rectangle(0, 0, 850, 43);
			if (tabContent.height > containerTabViewport.width)
			{
				_contentScrollTab.verticalScrollingEnabled = false;
				_contentScrollTab.addScrollControll(tabContent, tabContainer, containerTabViewport);
			}

		}

		public function get checked_mc():MovieClip
		{
			return _skin.getChildByName("checked_mc") as MovieClip;
		}

		public function get container_sp():Sprite
		{
			return _skin.getChildByName("container_sp") as Sprite;
		}

		public function get today_date_tf():TextField
		{
			return _skin.getChildByName("today_date_tf") as TextField;
		}

		public function get next_date_tf():TextField
		{
			return _skin.getChildByName("next_date_tf") as TextField;
		}

		public function get pass_date_tf():TextField
		{
			return _skin.getChildByName("pass_date_tf") as TextField;
		}

		public function show():void
		{
			visible = true;
			mouseChildren = true;
			mouseEnabled = true;
		}

		public function hide():void
		{
			visible = false;
			mouseChildren = false;
			mouseEnabled = false;
		}

		public function get currentRoutineDetail():RoutineDetail
		{
			return _currentRoutineDetail;
		}

		public function dispose():void
		{
			_contentScroll.removeScrollControll();
			_contentScrollTab.removeScrollControll();
			super.destroy();
		}

		public function set currentRoutineDetail(value:RoutineDetail):void
		{
			_currentRoutineDetail = value;
		}

	}
}
