package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.data.BridgeData;
	import com.unitmatrix.core.model.data.MemoDetail;
	import com.unitmatrix.core.model.data.TreatmentDetailData;
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.unitmatrix.module.surveyinfo.view.components.item.AddDamageRowItem;
	import com.unitmatrix.module.surveyinfo.view.components.item.MemoEditItem;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.globalization.DateTimeFormatter;

	import __AS3__.vec.Vector;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	public class SurveyInfoEditNormalMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var view:SurveyInfoEditNormalView;

		[Inject]
		public var model:SurveyInfoModel;

		// create ------------------------------------------------------------------
		private var _dialogComponent:DialogComponent;
		private var _bridgeDatas:BridgeData;

		override public function onRegister():void
		{
			_bridgeDatas = coreModel.getBridgeDataByID(coreModel.select_survey_id);

			view.create();
			view.addDetail(coreModel.treatmentDatas, coreModel.currentRoutineDetail);
			initMouseHandler();
		}

		private function initMouseHandler():void
		{
			eventMap.mapListener(view, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			var onDateChangeSignal:Signal = new Signal(Date);
			var dfm:DateTimeFormatter = new DateTimeFormatter("en-US");
			dfm.setDateTimePattern("dd-MM-yyyy");
			var targetName:String = event.target.name;

			switch (targetName)
			{
				case "save_btn":
				case "close_btn":
					var dialogSignal:Signal = new Signal(String);
					dialogSignal.addOnce(function(value:String):void
					{
						if (value == DialogComponent.DIALOGCOMPONENT_CLICK_YES && targetName == "save_btn")
							save();

						_dialogComponent.dispose();
						_dialogComponent = null;

						//if (targetName == "close_btn")
						view.dispose();
					});
					_dialogComponent = new DialogComponent("แจ้งเตือน", "<font color='#000000'>ต้องการบันทึกข้อมูลรึไม่</font>", DialogComponent.YES_NO, dialogSignal);

					lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);

					break;
				case "special_checked_sp":
					view.swichCheckIcon();
					break;
				case "today_date_btn":
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.today_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				case "next_date_btn":
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.next_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				case "pass_date_btn":
					onDateChangeSignal.addOnce(function(date:Date):void
					{
						view.pass_date_tf.text = dfm.format(date);
					});
					view.addDatePicker(new Point((event.target.x + 220) * Config.ratio, (event.target.y + 150) * Config.ratio), onDateChangeSignal);
					break;
				case "add_btn":
					view.addNewData();
					break;
				case "delete_btn":
					view.removeRowDataWithTarget((event.target).parent);
					break;
				default:
					view.checkHitDC(event);
					break;
			}

		}

		private function save():void
		{
			var treatment_detail:Vector.<TreatmentDetailData> = new Vector.<TreatmentDetailData>;
			for each (var item:AddDamageRowItem in view.addDamageRowItems)
			{
				if (item.quantity != "0")
				{
					var tdd:TreatmentDetailData = new TreatmentDetailData(item.id, item.quantity, item.path);
					treatment_detail.push(tdd);
				}

			}

			var memo_detail:Vector.<MemoDetail> = new Vector.<MemoDetail>;
			var count:int = 0;
			for each (var memo:MemoEditItem in view.memoEditItems)
			{
				if (memo.title != "")
				{
					count++;
					var md:MemoDetail = new MemoDetail(count.toString(), memo.title, memo.id);
					memo_detail.push(md);
				}

			}

			coreModel.currentRoutineDetail.memo_detail = memo_detail;
			coreModel.currentRoutineDetail.treatment_detail = treatment_detail;
			coreModel.currentRoutineDetail.routine_special_flag = view.routine_special_flag;
			coreModel.currentRoutineDetail.routine_dt = view.today_date_tf.text;
			coreModel.currentRoutineDetail.routine_next_dt = view.next_date_tf.text;
			coreModel.currentRoutineDetail.routine_last_dt = view.pass_date_tf.text;

			//set to save
			coreModel.itemToSave = true;
		}

		override public function onRemove():void
		{
			eventMap.unmapListener(view, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			view = null;
		}

	}
}


