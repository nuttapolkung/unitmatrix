package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.idealizm.util.StageTextUtil;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.SoftKeyboardType;
	import flash.text.StageText;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;

	import SurveyInfo.damage_add_row_detail_sp;

	public class AddDamageRowItem extends SDSprite
	{

		private var _skin:Sprite;
		private var _id:String;
		private var _path:String;

		private var _st_tf:StageText;

		public function AddDamageRowItem(id:String, path:String, index:int, title:String, unit:String, quantity:String = "0")
		{
			_skin = new SurveyInfo.damage_add_row_detail_sp();
			addChild(_skin);

			tab_bg_sp.alpha = (index % 2 == 0) ? 0 : .3;

			rank_tf.text = (index + 1).toString();
			title_tf.text = title;
			unit_tf.text = unit;
			quantity_tf.text = Number(quantity).toString();

			if (!path || path == "")
				_path = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_path = path;

			_id = id;

			quantity_tf_sp.addEventListener(MouseEvent.MOUSE_DOWN, onDown);

		}

		protected function onDown(event:MouseEvent):void
		{
			var point:Point = this.localToGlobal(new Point(quantity_tf_sp.x, quantity_tf_sp.y));
			var viewPort:Rectangle = new Rectangle(point.x, point.y, quantity_tf_sp.width * Config.ratio, quantity_tf_sp.height * Config.ratio);

			var title:String = (Number(quantity_tf.text) == 0) ? "" : quantity_tf.text;
			var stageTextUtil:StageTextUtil = new StageTextUtil();
			stageTextUtil.textFieldToStageText(quantity_tf_sp, this.stage, viewPort, title, 18 * Config.ratio, 0x000000, "0-9\.", TextFormatAlign.CENTER, (Config.IS_ANDROID) ? SoftKeyboardType.DEFAULT : SoftKeyboardType.NUMBER);
			stageTextUtil.completeSignal.add(function(value:String):void
			{
				stageTextUtil = null;
				quantity_tf.text = Number(value).toString();
			});
		}

		public function get rank_tf():TextField
		{
			return _skin.getChildByName("rank_tf") as TextField;
		}

		public function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}

		public function get quantity_tf():TextField
		{
			return quantity_tf_sp.getChildByName("quantity_tf") as TextField;
		}

		public function get quantity_tf_sp():Sprite
		{
			return _skin.getChildByName("quantity_tf_sp") as Sprite;
		}

		public function get unit_tf():TextField
		{
			return _skin.getChildByName("unit_tf") as TextField;
		}

		public function get tab_bg_sp():Sprite
		{
			return _skin.getChildByName("tab_bg_sp") as Sprite;
		}

		public function get id():String
		{
			return _id;
		}

		public function get quantity():String
		{
			return quantity_tf.text;
		}

		public function get path():String
		{
			return _path;
		}


	}
}
