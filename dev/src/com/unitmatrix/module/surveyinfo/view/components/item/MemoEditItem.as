package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.idealizm.util.StageTextUtil;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.ReturnKeyLabel;
	import flash.text.SoftKeyboardType;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;

	import SurveyInfo.damage_add_row_detail_2_sp;

	public class MemoEditItem extends SDSprite
	{
		private var _skin:Sprite;
		private var _id:String;

		public function MemoEditItem(id:String, rank:String, title:String)
		{
			_skin = new SurveyInfo.damage_add_row_detail_2_sp();
			addChild(_skin);

			rank_tf.text = rank;
			title_tf.text = title;

			if (int(rank) % 2 == 0)
				tab_bg_sp.alpha = 0;
			else
				tab_bg_sp.alpha = .5;


			if (!id || id == "")
				_id = String(new Date().time / 1000000000).split(".")[1] + "" + Math.round(Math.random() * 999);
			else
				_id = id;
			title_tf.addEventListener(MouseEvent.MOUSE_DOWN, onDown);

		}

		protected function onDown(event:MouseEvent):void
		{
			var point:Point = this.localToGlobal(new Point(title_tf_sp.x, title_tf_sp.y));
			var viewPort:Rectangle = new Rectangle(point.x, point.y, title_tf_sp.width * Config.ratio, title_tf_sp.height * Config.ratio);
			var stageTextUtil:StageTextUtil = new StageTextUtil();
			stageTextUtil.textFieldToStageText(title_tf_sp, this.stage, viewPort, title_tf.text, 18 * Config.ratio, 0x000000, null, TextFormatAlign.LEFT, SoftKeyboardType.DEFAULT, ReturnKeyLabel.DEFAULT);
			stageTextUtil.completeSignal.addOnce(function(value:String):void
			{
				stageTextUtil = null;
				title_tf.text = value;
			});
		}


		public function get title():String
		{
			return title_tf.text;
		}

		public function get title_tf():TextField
		{
			return title_tf_sp.getChildByName("title_tf") as TextField;
		}

		public function get title_tf_sp():Sprite
		{
			return _skin.getChildByName("title_tf_sp") as Sprite;
		}

		public function get rank_tf():TextField
		{
			return _skin.getChildByName("rank_tf") as TextField;
		}

		public function get tab_bg_sp():Sprite
		{
			return _skin.getChildByName("tab_bg_sp") as Sprite;
		}

		public function get id():String
		{
			return _id;
		}

	}
}
