package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	import Dialog.check_sp;

	import SurveyInfo.block_image;

	import org.osflash.signals.Signal;

	public class SelectDamageLevel extends SDSprite
	{
		private var _skin:Sprite;
		public var selected:String = "6";
		public var completeSignal:Signal = new Signal;

		public function SelectDamageLevel(value:int, select:String)
		{
			create();
			setContent(value);

			selectedCK(select);
			selected = select;

			addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function setContent(value:int):void
		{
			var txt:String;
			var title_txt:String;

			switch (value)
			{
				case 1:
					title_txt = "โครงสร้างส่วนบน";
					txt = "5 สภาพดีสภาพเหมือนใหม่\n4 สภาพพอใช้พบการแตกร้าวที่ผิวเพียงเล็กน้อย สังเกตได้ยาก\n3 สภาพพอใช้พบการแตกร้าวที่ผิวแต่ไม่พบคราบสนิมตามแนวที่เกิดการแตกร้าว\n2 สภาพชํารุด พบการแตกร้าวที่ผิวและมีคราบสนิมตามแนวที่เกิดการแตกร้าว สามารถสังเกตเห็นเหล็กเสริม\n1 สภาพชํารุดเสียหายมาก พบการแตกร้าวที่ ก่ อให้เกิดการวิบัติต้องทําการซ่ อมบํารุงแบบเร่งด่วน\n0 สภาพชํารุดมาก ไม่สามารถทํางานได้ตามที่ออกแบบไว้ต้องทําการเปลี่ยนสะพานใหม่";
					break;
				case 2:
					title_txt = "โครงสร้างส่วนล่าง";
					txt = "5 สภาพดีสภาพเหมือนใหม่\n4 สภาพพอใช้พบการแตกร้าวที่ผิวเพียงเล็กน้อย สังเกตได้ยาก\n3 สภาพพอใช้พบการแตกร้าวที่ผิวแต่ไม่พบคราบสนิมตามแนวที่เกิดการแตกร้าว\n2 สภาพชํารุด พบการแตกร้าวที่ผิวและมีคราบสนิมตามแนวที่เกิดการแตกร้าว สามารถสังเกตเห็นเหล็กเสริม\n1 สภาพชํารุดเสียหายมาก พบการแตกร้าวที่ ก่ อให้เกิดการวิบัติต้องทําการซ่ อมบํารุงแบบเร่งด่วน\n0 สภาพชํารุดมาก ไม่สามารถทํางานได้ตามที่ออกแบบไว้ต้องทําการเปลี่ยนสะพานใหม่";
					break;
				case 3:
					title_txt = "คอสะพาน";
					txt = "5 สภาพดีสภาพเหมือนใหม่ ยานพาหนะสามารถใช้ความเร็วได้เต็มที่\n4 สภาพพอใช้ยานพาหนะสามารถใช้ความเร็วได้ตามปกติ\n3 สภาพพอใช้มีการทรุดตัวเล็กน้อย ยานพาหนะสามารถใช้ความเร็วได้ตามปกติมีการชะลอเล็กน้อย\n2 สภาพชํารุด มีการทรุดตัวของคอสะพานอย่างเห็นได้ชัด ยานพาหนะต้องชะลอลดความเร็วลงพอสมควรก่อนเข้า สะพาน\n1 สภาพชํารุดเสียหายมาก มีการทรุดตัวของคอสะพานอย่างมาก ยานพาหนะต้องชะลอลดความเร็วลงอย่างมาก ก่อนเข้าสะพาน\n0 สภาพชํารุดมาก มีการทรุดตัวของคอสะพานอย่างมาก อาจจะทําให้เกิดความเสียหายต่อยานพาหนะที่วิ่งผ่าน สะพานได้";
					break;
				case 4:
					title_txt = "Slope protection";
					txt = "5 สภาพดีสภาพเหมือนใหม่\n4 สภาพพอใช้สังเกตความเสียหายได้ยาก ไม่พบการทรุดตัวหรือเสียหาย\n3 สภาพพอใช้มีการทรุดตัวหรือเสียหายเล็กน้อย โครงสร้างสามารถกันดินได้ปกติ\n2 สภาพชํารุด มีการทรุดตัวหรือเสียหายอย่างเห็นได้ชัด โครงสร้างเริ่มสามารถกันดินได้\n1 สภาพชํารุดเสียหายมาก มีการทรุดตัวหรือเสียหายอย่างมาก โครงสร้างไม่สามารถกันดินได้\n0 สภาพชํารุดมาก มีการทรุดตัวหรือเสียหายอย่างมาก โครงสร้างกันดินถล่ม ไม่สามารถใช้งานได";
					break;
				case 5:
					title_txt = "ผิวจราจร";
					txt = "5 สภาพดีสภาพเหมือนใหม่\n4 สภาพพอใช้สังเกตการแตกร้าวที่ผิวได้ยาก ไม่พบการหลุดร่อนของวัสดุ\n3 สภาพพอใช้มีการเสียรูปเล็กน้อย มีการแตกร้าวเพียงเล็กน้อย ไม่พบการหลุดร่อนของผิวจราจร\n2 สภาพชํารุด มีการเสียรูปและแตกร้าวรุนแรงจนน้ําสามารถซึมผ่านลงไปได้ในบางตําแหน่ง มีการหลุดร่อนของผิว จราจรเล็กน้อย\n1 สภาพชํารุดเสียหายมาก มีการเสียรูปมากและมีการแตกร้าวรุนแรงจนน้ําสามารถซึมผ่านลงไปได้ มีการหลุดร่อน ของผิวจราจรเป็นบริเวณกว้าง\n0 สภาพชํารุดมาก ไม่สามารถทํางานได้ตามที่ออกแบบไว้ต้องทําการเปลี่ยนผิวจราจร";
					break;
			}
			content_tf.text = txt;
			title_tf.text = title_txt;
		}

		private function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			_skin = new Dialog.check_sp;
			_skin.scaleX = _skin.scaleY = Config.ratio;
			_skin.x = Config.GUI_SIZE.width / 2 - _skin.width / 2;
			_skin.y = Config.GUI_SIZE.height / 2 - _skin.height / 2;
			addChild(_skin);

			selectedCK(selected);

		}


		protected function onMouseUp(event:MouseEvent):void
		{
			if (event.target.name.indexOf("ck") != -1)
			{
				var choice:String = String(String(event.target.name).split("_")[0]).charAt(2);

				selectedCK(choice);
				selected = choice;
				dispose();
			}
			else if (event.target.name == "close_btn")
			{
				dispose();
			}
		}

		private function dispose():void
		{
			completeSignal.dispatch();
			destroy();
			removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function selectedCK(choice:String):void
		{
			var choiceName:String = "ck" + choice + "_sp";
			var row:Sprite = _skin.getChildByName("choice_0_sp") as Sprite;
			if (row)
				for (var j:int = 0; j < row.numChildren; j++)
				{
					var symbol:Sprite = row.getChildAt(j) as Sprite;

					if (symbol.name == choiceName)
						symbol.alpha = 1;
					else
						symbol.alpha = 0;
				}

		}

		private function get content_tf():TextField
		{
			return _skin.getChildByName("content_tf") as TextField;
		}
		private function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}
	}
}
