package com.unitmatrix.module.surveyinfo.view.components
{
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.sleepydesign.display.SDSprite;

	import flash.display.Sprite;
	import flash.geom.Rectangle;

	import Album.album_sp;

	import SurveyInfo.block_image;

	public class PhotoAlbumView extends SDSprite
	{

		private var album_sp:Sprite;

		private var container_sp:Sprite;

		private var contentScroll:ScrollController;

		private var content_sp:Sprite;

		public var albumID:String = "_";

		public function PhotoAlbumView()
		{
			super();
		}

		private var thumbs:Vector.<ImageThumbnail> = new Vector.<ImageThumbnail>;

		public function create():void
		{
			var container:Sprite = new Sprite();
			addChild(container);
			container.graphics.beginBitmapFill(new SurveyInfo.block_image, null, true, false);
			container.graphics.drawRect(0, 0, Config.GUI_SIZE.width, Config.GUI_SIZE.height);
			container.graphics.endFill();
			container.alpha = 0.8;

			album_sp = new Album.album_sp();
			album_sp.name = "album_sp";

			album_sp.x = 17;
			album_sp.y = 30;
			addChild(album_sp);

			container_sp = album_sp.getChildByName("container_sp") as Sprite;

			resize();
		}

		private function resize():void
		{
			album_sp.scaleX = album_sp.scaleY = Config.ratio;
			album_sp.x = Config.GUI_SIZE.width * 0.5 - album_sp.width * 0.5;
			album_sp.y = Config.GUI_SIZE.height * 0.5 - album_sp.height * 0.5;
		}

		public function createAlbum(param0:Object):void
		{
			content_sp = new Sprite();

			var total:int = 4;

			var col:int = 2;
			var row:int = total / col;
			if (total % col != 0)
				row++;

			thumbs = new Vector.<ImageThumbnail>();
			var titles:Array = ["อัลบั้มภาพรวม", "อัลบั้มภาพโครงสร้างส่วนบน", "อัลบั้มภาพโครงสร้างส่วนล่าง", "อัลบั้มภาพความเสียหาย"]
			for (var i:int = 0; i < total; i++)
			{
				var thumb_nail:ImageThumbnail = new ImageThumbnail(titles[i])
				thumb_nail.name = "thumb_nail_01";
				thumb_nail.albumID = this.albumID + i + "_";

				thumb_nail.initData();

				thumb_nail.x = (i % col) * (thumb_nail.width + 62);
				thumb_nail.y = int(i / col) * (thumb_nail.height + 47);

				content_sp.addChild(thumb_nail);

				thumb_nail.mouseChildren = false;
				thumb_nail.buttonMode = true;

				thumbs.push(thumb_nail);
			}

			content_sp.x = 60;
			content_sp.y = 0;

			container_sp.addChild(content_sp);

			var containerViewport:Rectangle = new Rectangle(0, 0, 940, 604);

			contentScroll = new ScrollController();
			contentScroll.horizontalScrollingEnabled = false;
			contentScroll.addScrollControll(content_sp, container_sp, containerViewport);
		}

		public function refreshView():void
		{
			for each (var thumb:ImageThumbnail in thumbs)
			{
				thumb.reCheckCover();
			}
		}
	}
}

// INTERNAL CLASS

import com.greensock.events.LoaderEvent;
import com.greensock.loading.ImageLoader;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.filesystem.File;
import flash.text.TextField;

import Album.thumbnail_sp;


internal class ImageThumbnail extends Sprite
{

	[Embed(source = "/album/noimage_avalible.jpg")]
	public var noImageCLAZZ:Class;

	public var albumID:String;
	private var album_file:File;

	private var _skin:Sprite;
	private var isHadCover:Boolean = false;

	public function ImageThumbnail(title:String)
	{
		_skin = new Album.thumbnail_sp();
		addChild(_skin);
		title_tf.text = title;
		this.mouseChildren = false;
	}

	public function get title_tf():TextField
	{
		return _skin.getChildByName("title_tf") as TextField;
	}

	public function initData():void
	{
		var bitmap:Bitmap;
		album_file = File.applicationStorageDirectory.resolvePath(albumID);
		if (album_file.exists)
		{
			var images:Array = album_file.getDirectoryListing();
			if (images.length == 0)
			{
				bitmap = new noImageCLAZZ();
				image_container_sp.addChild(bitmap);
			}
			else
			{
				isHadCover = true;
				var imageLoader:ImageLoader = new ImageLoader(images[0].url, {container: image_container_sp, width: image_container_sp.width, height: image_container_sp.height, scaleMode: "proportionalOutside", crop: true})
				imageLoader.load();
			}
		}
		else
		{
			bitmap = new noImageCLAZZ();
			image_container_sp.addChild(bitmap);
		}
	}

	public function reCheckCover():void
	{
		if (isHadCover)
			return;

		var bitmap:Bitmap;
		album_file = File.applicationStorageDirectory.resolvePath(albumID);
		if (album_file.exists)
		{
			var images:Array = album_file.getDirectoryListing();
			if (images.length != 0)
			{
				isHadCover = true;
				var imageLoader:ImageLoader = new ImageLoader(images[0].url, {container: image_container_sp, width: image_container_sp.width, height: image_container_sp.height, scaleMode: "proportionalOutside", crop: true})
				imageLoader.load();
			}
		}
	}

	private function completeHandler(event:LoaderEvent):void
	{
		trace(event.target + " is complete!");
		var imageLoader:ImageLoader = event.currentTarget as ImageLoader;
		image_container_sp.addChild(imageLoader.rawContent);
	}

	private function get image_container_sp():Sprite
	{
		return _skin.getChildByName("image_container_sp") as Sprite;
	}
}
