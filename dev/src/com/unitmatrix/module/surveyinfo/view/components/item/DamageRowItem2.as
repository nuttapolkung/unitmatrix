package com.unitmatrix.module.surveyinfo.view.components.item
{
	import com.sleepydesign.display.SDSprite;

	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.text.TextField;

	import SurveyInfo.damage_row_detail_2_sp;

	public class DamageRowItem2 extends SDSprite
	{
		private var _id:String;
		private var _skin:Sprite;

		public function DamageRowItem2(id:String, index:int, title:String)
		{
			_id = id;
			_skin = new SurveyInfo.damage_row_detail_2_sp();
			addChild(_skin);

			tab_bg_sp.alpha = (index % 2 == 0) ? 0 : .3;

			//_id = id;
			rank_tf.text = (index + 1).toString();
			title_tf.text = title;

		}

		public function get rank_tf():TextField
		{
			return _skin.getChildByName("rank_tf") as TextField;
		}

		public function get title_tf():TextField
		{
			return _skin.getChildByName("title_tf") as TextField;
		}

		public function get tab_bg_sp():Sprite
		{
			return _skin.getChildByName("tab_bg_sp") as Sprite;
		}

		public function get cametaBTN():SimpleButton
		{
			return _skin.getChildByName("camera_icon_btn") as SimpleButton;
		}

		public function get id():String
		{
			return _id;
		}

	}
}
