package com.unitmatrix.module.surveyinfo.view.components
{
	import com.unitmatrix.core.component.DialogCameraComponent;
	import com.unitmatrix.core.component.DialogComponent;
	import com.unitmatrix.core.model.CoreModel;
	import com.unitmatrix.module.surveyinfo.model.SurveyInfoModel;
	import com.unitmatrix.module.surveyinfo.model.data.AlbumData;
	import com.unitmatrix.module.surveyinfo.model.data.ImageData;
	import com.unitmatrix.module.surveyinfo.view.components.item.GalleryThumbnail;
	import com.unitmatrix.module.surveyinfo.view.components.item.ImageView;
	import com.sleepydesign.lightboxs.signals.LightBoxSignal;
	import com.sleepydesign.lightboxs.view.LightBoxModule;
	import com.sleepydesign.robotlegs.apps.model.AppModel;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.MediaEvent;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Rectangle;
	import flash.media.CameraRoll;
	import flash.media.CameraUI;
	import flash.media.MediaPromise;
	import flash.media.MediaType;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;
	import flash.utils.IDataInput;

	import mx.utils.UIDUtil;

	import org.osflash.signals.Signal;
	import org.robotlegs.utilities.modular.mvcs.ModuleMediator;

	import starling.utils.RectangleUtil;

	public class GalleryMediator extends ModuleMediator
	{
		// external ------------------------------------------------------------------

		[Inject]
		public var coreModel:CoreModel;

		[Inject]
		public var appModel:AppModel;

		[Inject]
		public var lightBoxSignal:LightBoxSignal;

		// internal ------------------------------------------------------------------

		[Inject]
		public var module:GalleryView;

		[Inject]
		public var model:SurveyInfoModel;

		private var _deviceCameraApp:CameraUI;
		private var _mediaSource:CameraRoll;
		private var _imageLoader:Loader;
		private var _gallery_path:String;
		private var _gallery_file:File;
		private var _files:Array;
		private var _albumData:AlbumData;
		private var _dialogComponent:DialogComponent;
		private var _dataSource:IDataInput;

		override public function onRegister():void
		{
			getAllimage();
		}

		private function getAllimage():void{
			var status:Boolean = false
			_gallery_file = File.applicationStorageDirectory.resolvePath(module.gallery_ID);
			_gallery_path = _gallery_file.nativePath;

			if (_gallery_file.exists)
				status = true;
			else
				_gallery_file.createDirectory();

			module.create();
			initMouseHandle();

			_albumData = new AlbumData();
			_albumData.images = new Vector.<ImageData>;

			if (status)
				loadGallery();
			else
				module.createGallery(_albumData.images);

		}
		private function loadGallery():void
		{
			_files = _gallery_file.getDirectoryListing();

			for (var i:int = 0; i < _files.length; i++)
			{
				var imageData:ImageData = new ImageData();
				imageData.f = _files[i];

				_albumData.images.push(imageData);
			}

			module.createGallery(_albumData.images);
		}

		private function initMouseHandle():void
		{
			eventMap.mapListener(module, MouseEvent.MOUSE_UP, onMouseUp);
		}

		private function onMouseUp(event:MouseEvent):void
		{
			if (event.target is GalleryThumbnail)
			{
				var gt:GalleryThumbnail = event.target as GalleryThumbnail

				var thumb_nail:ImageView = new ImageView();
				thumb_nail.deletSignal.addOnce(function():void
				{
					getAllimage();
				});
				thumb_nail.initWithImageData(gt.imageData);
				module.addChild(thumb_nail);
				thumb_nail.loader.load();

				return;
			}

			var target_name:String = event.target.name;
			switch (target_name)
			{
				case "close_btn":
				{
					if (event.target.parent.name == "gallery_sp")
						module.destroy();
					break;
				}
				case "capture_btn":
				{
					var dialogSignal:Signal = new Signal(String);
					dialogSignal.addOnce(selectTypeCamera);
					var dialogCameraComponent:DialogCameraComponent = new DialogCameraComponent(dialogSignal);
					lightBoxSignal.dispatch(dialogCameraComponent, LightBoxModule.ALERT_LAYER);

					break;
				}

				default:
				{
					break;
				}
			}

			if (target_name.indexOf("thumb_nail") != -1)
			{
				var album_id:String = target_name.split("_")[2];
				model.select_album_id = album_id;
				module.addChild(new GalleryView());
			}
		}

		private function selectTypeCamera(value:String):void
		{
			switch (value)
			{
				case DialogCameraComponent.CAMERA_UI:
					if (CameraUI.isSupported)
					{
						_deviceCameraApp = new CameraUI();
						_deviceCameraApp.launch(MediaType.IMAGE);
						_deviceCameraApp.addEventListener(MediaEvent.COMPLETE, imageCaptured);
						_deviceCameraApp.addEventListener(Event.CANCEL, captureCanceled);
						_deviceCameraApp.addEventListener(ErrorEvent.ERROR, cameraError);
					}
					else
					{
						trace("Camera interface is not supported.");
					}
					break;
				case DialogCameraComponent.CAMERA_ROLL:
					if (CameraRoll.supportsBrowseForImage)
					{
						_mediaSource = new CameraRoll();
						_mediaSource.addEventListener(MediaEvent.SELECT, imageCaptured);
						_mediaSource.addEventListener(Event.CANCEL, captureCanceled);
						_mediaSource.addEventListener(ErrorEvent.ERROR, cameraError);
						_mediaSource.browseForImage();
					}
					else
					{
						trace("Browsing in camera roll is not supported.");
					}
					break;
			}

		}

		//		Camera function 
		private function imageCaptured(event:MediaEvent):void
		{
			_dialogComponent = new DialogComponent("แจ้งเตือน", "กำลังบันทึกรูปภาพ", DialogComponent.NONE, new Signal());
			lightBoxSignal.dispatch(_dialogComponent, LightBoxModule.ALERT_LAYER);

			var imagePromise:MediaPromise = event.data;

			if (imagePromise.isAsync)
			{
				_dataSource = imagePromise.open();
				var eventSource:IEventDispatcher = _dataSource as IEventDispatcher;
				eventSource.addEventListener(Event.COMPLETE, onMediaLoaded);
			}
			else
			{
				trace("Synchronous media promise.");
			}
		}

		private function onMediaLoaded(event:Event):void
		{
			var imageBytes:ByteArray = new ByteArray();
			_dataSource.readBytes(imageBytes);

			var randInt:int = Math.random() * (99999 - 1001) + 1001;
			var randStr:String = randInt.toString();
			//		var filename:String = "image_" + new Date().toLocaleString() + ".jpg";
			var filename:String = "image_" + UIDUtil.createUID() + ".jpg";

			var f:File = _gallery_file.resolvePath(filename);
			var fs:FileStream = new FileStream();
			fs.openAsync(f, FileMode.WRITE);
			fs.writeBytes(imageBytes, 0, imageBytes.bytesAvailable);
			fs.close();



			trace("save file url " + f.nativePath);

			var imageData:ImageData = new ImageData();
			imageData.f = f;

			_albumData.images.push(imageData);

			fs.addEventListener(Event.CLOSE, function(event:Event):void
			{
				var mLoader:Loader = new Loader();
				var mRequest:URLRequest = new URLRequest(f.url);
				mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(event:Event):void
				{
					module.addImage(imageData);
					_dialogComponent.dispose();
				});

				mLoader.load(mRequest);
			});

		}

		private function captureCanceled(event:Event):void
		{
			trace("Media capture canceled.");
		}

		private function asyncImageLoaded(event:Event):void
		{
			trace("Media loaded in memory.");
			showMedia(_imageLoader);
		}

		private function showMedia(loader:Loader):void
		{

			var image:Bitmap = loader.content as Bitmap;

			var targetRect:Rectangle = RectangleUtil.fit(image.bitmapData.rect, new Rectangle(0, 0, 800, 600));

			image.width = targetRect.width;
			image.height = targetRect.height;

			var newBMD:BitmapData = new BitmapData(targetRect.width, targetRect.height, true, 0);
			newBMD.draw(image);

			_dialogComponent.dispose();
		}

		private function cameraError(error:ErrorEvent):void
		{
			trace("Error:" + error.text);
		}

		//		end camera function

		override public function onRemove():void
		{
			eventMap.unmapListener(module, MouseEvent.MOUSE_UP, onMouseUp);

			appModel = null;
			module = null;
		}
	}
}


