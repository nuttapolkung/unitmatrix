package com.idealizm.webcam
{
	import flash.media.Camera;
	import flash.media.Video;

	public class WebcamUtil
	{
		public function WebcamUtil()
		{
		}

		public static function setupWebcam():Video
		{
			var camera:Camera = Camera.getCamera();
			camera.setMode(320, 240, 30);

			var video:Video = new Video(320, 240);
			video.attachCamera(camera);

			return video;
		}
	}
}
