package com.idealizm.manager
{
	import com.sleepydesign.net.LoaderUtil;
	import com.sleepydesign.system.DebugUtil;

	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	import org.osflash.signals.Signal;

	public class LoadManager
	{
		private static var _raw:String;


		public static function get raw():String
		{
			return _raw;
		}

		public static function loadJson(group_name:String, urlVariables:URLVariables, type:String = URLRequestMethod.GET):Signal
		{
			var completeSignal:Signal = new Signal(String);

			var date:Date = new Date();
			var dateString:String = date.fullYear.toString() + "-" + ((date.month <= 9) ? ('0' + date.month.toString()) : date.month.toString()) + '-' + ((date.date <= 9) ? ('0' + date.date.toString()) : date.date.toString()) + ' ' + ((date.hours <= 9) ? ('0' + date.hours.toString()) : date.hours.toString()) + ':' + ((date.minutes <= 9) ? ('0' + date.minutes.toString()) : date.minutes.toString()) + ':' + ((date.seconds <= 9) ? ('0' + date.seconds.toString()) : date.seconds.toString());

			urlVariables.client_time = dateString;
			// set URI
			var uri:String = group_name;

			// load!
			LoaderUtil.request(uri, urlVariables, function(event:Event):void
			{
				if (event.type == IOErrorEvent.IO_ERROR)
				{
					DebugUtil.trace("Error : " + event);
				}

				if (event.type == Event.COMPLETE)
				{
					//
					var raw:String = event.target.data as String;
					_raw = raw;

					//-tell any one listener
					completeSignal.dispatch(_raw);
					//-clear signal
					completeSignal.removeAll();
					completeSignal = null;
						//event.target.data = event.target.data;

						//checkErrorData(event.target.data, callBack, event);
				}
			}, URLLoaderDataFormat.TEXT, type);

			return completeSignal;
		}

	}
}
