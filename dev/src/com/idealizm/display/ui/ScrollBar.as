package com.idealizm.display.ui
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	public class ScrollBar extends Sprite
	{
		public static const VERTICAL:String = "vertical";
		public static const HORIZONATAL:String = "horizonatal";

		private var _scrollBar:Sprite;
		private var _bounds:Rectangle;
		private var _container:Rectangle;

		private var _barMaxDistance:Number;
		private var _scrollBarHeight:Number;
		private var _scrollBarWidth:Number;
		private var _type:String;

		public function ScrollBar(container:Rectangle, bounds:Rectangle, type:String = VERTICAL)
		{
			_container = container;
			_bounds = bounds;
			_type = type;
			createScrollBar();
		}

		private function createScrollBar():void
		{
			if (!_scrollBar)
			{
				_scrollBar = new Sprite();
				addChild(_scrollBar);
			}

			_scrollBar.graphics.clear();
			if (_type == VERTICAL)
			{
				_scrollBar.x = (_container.x + _bounds.width) - 5;
				if (_bounds.height < _container.height)
				{
					_scrollBarHeight = _bounds.height / _container.height * _bounds.height;
					draw(_scrollBarHeight);
				}
			}
			else
			{
				_scrollBar.y = (_container.y + _bounds.height) - 5;
				if (_bounds.width < _container.width)
				{
					_scrollBarWidth = _bounds.width / _container.width * _bounds.width;
					draw(_scrollBarWidth);
				}
			}


			_scrollBar.alpha = 1;
			_barMaxDistance = (_type == VERTICAL) ? _bounds.height - _scrollBar.height : _bounds.width - _scrollBar.width;
		}

		public function updateBar(container:Rectangle):void
		{
			_container = container;

			if (_type == VERTICAL)
			{
				var listEndY:Number = _bounds.height - _container.height;
				_scrollBar.y = ((_container.y) / listEndY) * _barMaxDistance;

				if (_scrollBar.y > _barMaxDistance)
				{
					refreshScrollBar(_scrollBarHeight - ((_scrollBar.y + _scrollBarHeight) - _bounds.height));
				}

				//pulling down
				if (_scrollBar.y < 1)
				{
					_scrollBar.y = 0;
					refreshScrollBar(_scrollBarHeight - (_container.y / 2));
				}
			}
			else
			{
				var listEndX:Number = _bounds.width - _container.width;
				_scrollBar.x = ((_container.x) / listEndX) * _barMaxDistance;

				if (_scrollBar.x > _barMaxDistance)
				{
					refreshScrollBar(_scrollBarWidth - ((_scrollBar.x + _scrollBarWidth) - _bounds.width));
				}

				//pulling down
				if (_scrollBar.x < 1)
				{
					_scrollBar.x = 0;
					refreshScrollBar(_scrollBarWidth - (_container.x / 2));
				}
			}

		}

		private function refreshScrollBar(newSize:Number):void
		{
			//set to a minimum value of 4 to match iOS
			if (newSize > 4)
			{
				_scrollBar.graphics.clear();
				draw(newSize);
			}
		}

		private function draw(newSize:Number):void
		{
			//set to a minimum value of 4 to match iOS
			_scrollBar.graphics.beginFill(0x505050, .8);
			if (_type == VERTICAL)
				_scrollBar.graphics.drawRoundRect(0, _bounds.y, 4, (newSize), 6, 6);
			else
				_scrollBar.graphics.drawRoundRect(_bounds.x, 0, (newSize), 4, 6, 6);
			_scrollBar.graphics.endFill();
		}
	}
}
