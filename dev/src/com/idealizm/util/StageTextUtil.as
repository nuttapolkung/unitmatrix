package com.idealizm.util
{
	import com.greensock.TweenLite;

	import flash.display.DisplayObjectContainer;
	import flash.display.Stage;
	import flash.events.FocusEvent;
	import flash.events.SoftKeyboardEvent;
	import flash.geom.Rectangle;
	import flash.text.ReturnKeyLabel;
	import flash.text.SoftKeyboardType;
	import flash.text.StageText;

	import flashx.textLayout.formats.TextAlign;

	import org.osflash.signals.Signal;

	public class StageTextUtil
	{
		public var completeSignal:Signal = new Signal(String);
		private var _st_tf:StageText;
		private var _tf:DisplayObjectContainer;

		public function textFieldToStageText(tf:DisplayObjectContainer, stage:Stage, viewPort:Rectangle, title:String, fontSize:int, color:uint, restrict:String = "", textAlign:String = TextAlign.LEFT, softKeyboardType:String = SoftKeyboardType.DEFAULT, returnKeyLabel:String = ReturnKeyLabel.DEFAULT):void
		{
			_tf = tf;
			_tf.visible = false;
			_tf.mouseEnabled = false;

			_st_tf = new StageText();
			_st_tf.softKeyboardType = softKeyboardType;
			_st_tf.returnKeyLabel = returnKeyLabel;
			_st_tf.fontSize = fontSize;
			_st_tf.color = color;
			_st_tf.textAlign = textAlign;
			_st_tf.restrict = restrict;
			_st_tf.text = "";
			_st_tf.stage = stage;
			_st_tf.viewPort = viewPort;
			TweenLite.to(_st_tf, .5, {onComplete: function():void
			{
				_st_tf.assignFocus();
				initHandler();
			}});
		}

		protected function initHandler():void
		{
			_st_tf.addEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE, onSoftKeyboardDeactive);
			_st_tf.addEventListener(FocusEvent.FOCUS_OUT, onOut);
		}

		protected function onOut(event:FocusEvent):void
		{
			dispose();
		}

		protected function onSoftKeyboardDeactive(event:SoftKeyboardEvent):void
		{
			dispose();
		}

		public function dispose():void
		{
			if (!_st_tf)
				return;

			var str:String = _st_tf.text;
			completeSignal.dispatch(str);
			_st_tf.removeEventListener(SoftKeyboardEvent.SOFT_KEYBOARD_DEACTIVATE, onSoftKeyboardDeactive);
			_st_tf.removeEventListener(FocusEvent.FOCUS_OUT, onOut);
			TweenLite.to(_tf, 0.3, {onComplete: function():void
			{
				_tf.visible = true;
				_tf.mouseEnabled = true;
				_st_tf.stage = null;
				_st_tf = null;
			}});

		}
	}
}
